URL: `https://marchantandco.herokuapp.com/`

# Table of Contents

1. [Getting Started](#getting-started)
2. [The Project](#the-project)
3. [Navigating the app](#navigating-the-app)
4. [HTTP requests](#requests)
5. [Future Functionality](#future-functionality)

## Getting Started
1. Install Apache, PHP and MYSQL | [Windows](https://docs.moodle.org/34/en/Manual_install_on_Windows_7_with_Apache_and_MySQL), [OS X](https://jason.pureconcepts.net/2016/09/install-apache-php-mysql-mac-os-x-sierra/)
2. Clone the repository and point the apache web server to the application (this is in the process of being changed)
3. Install/update NPM
4. run `npm install` 
5. Create and populate the database (seed file is in: `/tests/_data/seed.sql`)

## Testing
The project uses codeception for testing. 

To run tests, run in console: `codecept run` 

#### The different suites and what they do:
* _Acceptance_ tests for user actions in the views such as filling and submission of forms, page redirects and feedback messages.
* _Functional_ tests for AJAX requests and validates JSON responses
* _Unit_ tests functions, models and their methods

**`NB:` The acceptance suite does not pass at this time**

Screenshots of browser and device functionality can be found in `/docs/browser_device_screens`

## The Project

This app is designed to be an _"office-buddy"_ of sorts for a small nursery business. 

Invoices can be submitted and their contents can be used to track inventory, manage products and calculate profit and sales data.

**Login Details**

_As users will be required to login to the site:_

```
username: user
password: password
```

## Navigating the app
**The app's directory structure is as follows**

```
app
├── assets
│   ├── components
│   │   ├── form_components
│   │   ├── inventory
│   │   ├── invoices
│   │   ├── layout
│   │   └── products
│   ├── images
│   │   └── inventory
│   ├── stylesheets
│   │   └── scss
│   │       └── components
│   └── vendor
├── config
├── controllers
│   ├── category
│   ├── form_components
│   ├── inventory_item
│   ├── invoice
│   ├── invoice_item
│   ├── product
│   └── supplier
├── lib
├── models
└── views
    ├── form_components
    ├── inventory_items
    ├── invoices
    └── layout

```

## Requests

Our api looks as follows:
```
eg.
`.com/invoices.php`
`GET` without parameters returns all invoices. 
`GET` with an id parameter returns invoice with that id. 
`POST` with params creates a new invoice, then redirects user to created invoice
`PATCH` with params updates an invoice
`DELETE` with an id parameter deletes said invoice
```

Suppliers, Invoices and category can be found the same way. 

The only difference is:

`.com/invoice_items.php` 

`GET` **must** be sent with an id parameter, which returns all invoice items for the **INVOICE WITH PASSED ID**

### Future Functionality
* **[BUG]** - Tailor Landscape views
* **[Feature]** - Ability to upload photograph or scan of original invoice for storage on the cloud and a database reference to the file url
* **[Security]** - Proper form validation. Emails are emails etc.
* **[Feature]** - Calculate Invoice Total (view)
* **[Security]** - Adjust client-side authorisation check to compare values not existence.
* **[Feature]** - Sortable table columns
* **[Feature]** - Search functionality (search for invoice, inventory item etc)
* **[Feature]** - Dashboard pages to Add/remove categories & suppliers
* **[Feature]** - Ability to create, delete and monitor Sales records (profit etc)
* **[Feature]** - Sales Metrics and Data
* **[Feature]** - Static care instructions for plants
* **[Feature]** - Mail integration to create and email invoices
