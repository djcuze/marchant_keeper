let image_upload = require('../form_components/image_upload');
const default_images_to_change = {
    name: 'default_images_to_change',
    template: '#dashboard_template',
    components: {
        table_row: {
            name: 'inventory_item',
            template: '#dashboard_table_row_template',
            props: ['id', 'prop_image_path', 'prop_description', 'quantity', 'category'],
            components: {
                image_upload
            },
            data() {
                return {
                    data_description: null,
                    data_image_path: null
                }
            },
            methods: {
                sendChanges() {
                    let item_to_pass = {
                        id: this.id,
                        description: this.data_description,
                        image_path: this.data_image_path,
                        quantity: this.quantity,
                        category: this.category
                    };
                    this.$parent.items_to_change.push(item_to_pass);
                }
            },
            computed: {
                image_path: {
                    get: function () {
                        return this.prop_image_path;
                    },
                    set: function (new_value) {
                        this.data_image_path = new_value;
                        this.sendChanges();
                    }
                },
                description: {
                    get: function () {
                        return this.prop_description;
                    },
                    set: function (new_value) {
                        this.data_description = new_value;
                        this.sendChanges();
                    }
                }
            },
            mounted() {
                this.data_image_path = this.image_path;
                this.data_description = this.description;
            },
        }
    },
    data() {
        return {
            items_to_change: []
        }
    },
    methods: {
        findCategory(category) {
            let all_categories = this.$root.categories;

            for (let i = 0; i < all_categories.length; i++) {
                if (all_categories[i].id === category) {
                    return all_categories[i].name;
                } else if (all_categories[i].name === category) {
                    return all_categories[i].id;
                }
            }
        },
        submitChanges() {
            for (let i = 0; i < this.items_to_change.length; i++) {
                axios.patch('/app/controllers/inventory.php', {
                    id: this.items_to_change[i].id,
                    description: this.items_to_change[i].description,
                    quantity: this.items_to_change[i].quantity,
                    image_path: this.items_to_change[i].image_path,
                    category_id: this.findCategory(this.items_to_change[i].category)
                })
            }
        }
    }
};
module.exports = default_images_to_change;