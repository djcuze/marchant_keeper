const STATUS_INITIAL = 0, STATUS_SAVING = 1, STATUS_SUCCESS = 2, STATUS_FAILED = 3;

function upload(formData) {
    for (let [key, value] of formData.entries()) {
        return axios.post('/app/controllers/form_components/image_upload.php',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        );
    }
}

const image_upload = {
    name: 'image_upload',
    template: '#image_upload_template',
    data() {
        return {
            uploadedFile: [],
            uploadError: null,
            currentStatus: null
        };
    },
    computed: {
        isInitial() {
            return this.currentStatus === STATUS_INITIAL;
        },
        isSaving() {
            return this.currentStatus === STATUS_SAVING;
        },
        isSuccess() {
            return this.currentStatus === STATUS_SUCCESS;
        },
        isFailed() {
            return this.currentStatus === STATUS_FAILED;
        }
    },
    methods: {
        reset() {
            // reset form to initial state
            this.uploadedFile = [];
            this.currentStatus = STATUS_INITIAL;
            this.uploadError = null;
        },
        save(formData) {
            // upload data to the server
            this.currentStatus = STATUS_SAVING;

            upload(formData)
                .then(response => {
                    this.uploadedFile = response.data;
                    this.uploadedFile.url = this.$parent.image_path = '/public/images/uploads/' + response.data.name;
                    this.currentStatus = STATUS_SUCCESS;
                })
                .catch(err => {
                    this.uploadError = err.response;
                    this.currentStatus = STATUS_FAILED;
                });
        },
        filesChange(fieldName, fileList) {
            // handle file changes
            const formData = new FormData();

            if (!fileList.length) return;

            // append the file to FormData
            formData.append(fieldName, fileList[0], fileList[0].name);
            // save it
            this.save(formData);
        }
    },
    mounted() {
        this.reset();
    },
};
module.exports = image_upload;