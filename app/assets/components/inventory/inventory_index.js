const inventory_index = {
    name: 'inventory_index',
    template: '#inventory_index_template',
    data() {
        return {
            inventory: [
                // An array of objects collected on creation with fetchData()
            ],
            results_per_page: 15,
            page_number: 1,
            settings_enabled: null
        };
    },
    mounted() {
        this.fetchData();
    },
    components: {
        table_row: {
            template: '#inventory_table_row_template',
            name: 'inventory_item',
            props: [
                'id',
                'description',
                'quantity',
                'category',
                'image_path'
            ],
            data() {
                return {
                    delete_confirmation: null
                };
            },
            methods: {
                deleteInventoryItem(id) {  // <=== There is a comment referring to this in _variables.scss
                    let inventory = this.$parent.inventory;
                    for (let i = 0; i < inventory.length; i++) {
                        if (inventory[i].id === this.id) {
                            this.$parent.inventory.splice(i, 1);
                            try {
                                axios.delete('/app/controllers/inventory.php', {
                                    data: {
                                        id: id
                                    }
                                }).then(
                                    this.delete_confirmation = null,
                                    this.$root.notice.message = 'Inventory Item deleted successfully',
                                    this.$root.notice.state = 'success'
                                );
                            } catch (error) {
                                console.log(error);
                                this.$root.notice.message = 'There was an error deleting the inventory item';
                                this.$root.notice.state = 'error';
                            }
                        }
                    }
                },
                deleteConfirmation() {
                    this.delete_confirmation = true;
                }
            }
        }
    },
    methods: {
        changePage(direction) {
            direction = direction.toLowerCase();
            if (direction === 'previous' && this.page_number > 1) {
                this.page_number -= 1;
            } else if (direction === 'next' && this.computedArray.length >= this.results_per_page) {
                this.page_number += 1;
            }
        },
        fetchData() {
            let self = this;
            self.$root.loading = true;
            axios.get('/app/controllers/inventory.php').then((response) => {
                this.inventory = response.data;
                self.$root.loading = false;
            }).catch(error => {
                console.log(error);
            });
        },
        updateResultsPerPageBy(count) {
            let buttons = $('.settings__control > div > button');
            buttons.removeClass('active');
            let result = $(".settings__control > div > button:contains(" + count + ")");
            result.toggleClass('active');
            this.results_per_page = count;
        },
        // Toggles visibility of "Options" div that contains buttons to an Edit form and a Destroy action accordingly
        toggleSettings() {
            let e = this.settings_enabled;
            this.settings_enabled = (function () {
                if (e === true) {
                    return null;
                } else {
                    return true;
                }
            })();
        }
    },
    computed: {
        computedArray() {
            let first = (this.results_per_page * (this.page_number - 1));
            let last = this.results_per_page * this.page_number;
            return this.inventory.slice(first, last);
        }
    },
    beforeRouteUpdate(to, from, next) {
        next();
    }
};
module.exports = inventory_index;