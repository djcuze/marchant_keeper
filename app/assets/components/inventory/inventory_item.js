let image_upload = require('../form_components/image_upload');
let single_inventory_item = {
    name: 'inventory_item',
    template: '#inventory_item_template',
    data() {
        return {
            description: null,
            quantity: null,
            image_path: null,
            category_id: null,
            editable: this.$route.params.editable
        };
    },
    components: {
        image_upload
    },
    mounted() {
        this.fetchData(this.$route.params.id);
    },
    computed: {
        categories() {
            return this.$root.categories
        },
        category() {
            return this.findCategory(this.category_id);
        }
    },
    methods: {
        fetchData(id) {
            let self = this;
            self.$root.loading = true;
            axios.get("/app/controllers/inventory.php?id=" + id)
                .then(response => {
                    this.description = response.data.attributes.description;
                    this.quantity = response.data.attributes.quantity;
                    this.image_path = response.data.attributes.image_path;
                    this.category_id = response.data.attributes.category_id;
                    self.$root.loading = false;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#category_id').selectmenu({
                select: function (event, ui) {
                    self.category = ui.item.value;
                }
            });
            $('#category_id').selectmenu('open');

        },
        findCategory(category) {
            for (let i = 0; i < this.categories.length; i++) {
                if (this.categories[i].id === category) {
                    return this.categories[i].name;
                } else if (this.categories[i].name === category) {
                    return this.categories[i].id;
                }
            }
        },
        updateInventoryItem() {
            axios.patch("/app/controllers/inventory.php", {
                id: this.$route.params.id,
                description: this.description,
                category_id: this.category_id,
                image_path: this.image_path,
                quantity: this.quantity,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((response) => {
                this.editable = false;
                this.$root.notice.message = 'Inventory Item updated successfully';
                this.$root.notice.state = 'success';
            })
        }
    },
    beforeRouteUpdate(to, from, next) {
        this.$router.push(to);
        next();
    }
};
module.exports = single_inventory_item;