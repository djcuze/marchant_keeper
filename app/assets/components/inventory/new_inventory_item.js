let image_upload = require('../form_components/image_upload');
const inventory_item_new = {
    name: 'new_inventory_item',
    data() {
        return {
            description: null,
            quantity: null,
            category: null,
            image_path: null
        };
    },
    components: {
        image_upload
    },
    methods: {
        validate() {
            if (!this.description || !this.category || !this.quantity) {
                return 'Error: Please fill out all fields';
            } else {
                return true;
            }
        },
        submit() {
            if (this.validate() === true) {
                try {
                    axios.post('/app/controllers/inventory.php', {
                        description: this.description,
                        quantity: this.quantity,
                        category_id: this.category,
                        image_path: this.image_path,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then((response) => {
                        this.$router.push('/inventory/' + response.data.id);
                        this.$root.notice.message = 'Inventory Item created successfully';
                        this.$root.notice.state = 'success';
                    });
                } catch (error) {
                    console.log(error);
                }
            } else {
                this.$root.notice.message = this.validate();
                this.$root.notice.state = 'error';
            }
        },
        getData() {
            $('#fileToUpload').addClass('half__visible');
            let file_name = document.getElementById('fileToUpload').value;
            $('#file_name').append(file_name);
        },
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#category_id').selectmenu({
                select: function (event, ui) {
                    self.category = ui.item.value;
                }
            });
            $('#category_id').selectmenu('open');
        }
    },
    template: '#new_inventory_item_template'
};
module.exports = inventory_item_new;