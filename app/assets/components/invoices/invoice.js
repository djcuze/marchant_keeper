let invoice_items_for_invoice = require('./invoice_items_for_invoice');
let invoice_items_for_invoice_form = require('./invoice_items_for_invoice_form');
let single_invoice = {
    name: 'Invoice',
    template: '#invoice_template',
    data() {
        return {
            number: null,
            date: null,
            supplier: null,
            invoice_items: [{}],
            total: null,
            editable: this.$route.params.editable,
            invoice_items_to_delete: []
        };
    },
    components: {
        // Components that render a template
        invoice_items_for_invoice,
        invoice_items_for_invoice_form
    },
    mounted() {
        this.fetchData(this.$route.params.id);
        this.fetchInvoiceItems(this.$route.params.id);
        this.listenForEscape();
    },
    methods: {
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#supplier_id').selectmenu({
                select: function (event, ui) {
                    self.supplier = ui.item.value;
                }
            });
            $('#supplier_id').selectmenu('open');

        },
        fetchData(id) {
            let self = this;
            self.$root.loading = true;
            axios.get("/app/controllers/invoices.php?id=" + id)
                .then(response => {
                    this.number = response.data.attributes.number;
                    this.date = response.data.attributes.date;
                    this.supplier = response.data.attributes.supplier;
                    this.total = response.data.attributes.total;
                    self.$root.loading = false;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchInvoiceItems(id) {
            axios.get('/app/controllers/invoice_items.php?id=' + id)
                .then(response => {
                    this.invoice_items = response.data;
                });
        },
        validateForm() {
            // validateForm();
            let number = $("#number").val();
            let supplier = $('#supplier');
            let date = $('#date');

            // Ensures input
            if (number.length <= 0) {
                let validation = false;
                let message = 'Please enter an invoice number';
                let problematic_input = 'number';
                return {validation, message, problematic_input};
            }
            else {
                let validation = true;
                let problematic_input, message = null;
                return {validation, message, problematic_input};
            }
        },
        updateInvoice() {
            let items_to_delete = this.invoice_items_to_delete;
            // If there are items that require removing from the Invoice:
            if (items_to_delete && items_to_delete.length !== 0) {
                for (let i = 0; i < items_to_delete.length; i++) {
                    axios.patch('/app/controllers/invoice_items.php', {
                        quantity: items_to_delete[i].quantity,
                        description: items_to_delete[i].description,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                }
            } else {
                let form = this.validateForm();
                if (form.validation === true) {
                    // Creates the invoice
                    axios.patch('', "/app/controllers/invoices.php", {
                        id: this.$route.params.id,
                        number: this.number,
                        date: this.date,
                        supplier: this.findSupplier(),
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(() => {
                        try { // Updating the invoice Items

                            // First, checks whether the invoice_item in question is a new entry, or an update
                            let invoice_items = this.invoice_items;

                            for (let n = 0; n < invoice_items.length; n++) {
                                let self = invoice_items[n];
                                if (Object.keys(self).length === 0 && self.constructor === Object) {
                                    break;
                                } else {
                                    axios.patch('../app/controllers/invoice_items.php', {
                                        id: invoice_items[n].id,
                                        description: invoice_items[n].description,
                                        category: this.findCategory(invoice_items[n].category),
                                        quantity: invoice_items[n].quantity,
                                        unit_price: invoice_items[n].unit_price,
                                        invoice_id: this.$route.params.id
                                    });
                                }
                            }
                        } catch (error) {
                            console.log(error);
                        }
                    }).then(
                        this.editable = null,
                        this.$root.notice.message = 'Invoice Updated Successfully',
                        this.$root.notice.state = 'success'
                    ).catch(error => {
                        console.log(error);
                        this.$root.notice.message = 'There was a problem updating the invoice';
                        this.$root.notice.state = 'error';
                    });
                } else {
                    $('#' + form.problematic_input).after('<p class="error">' + form.message + '</p>');
                }
            }
            this.invoice_items_to_delete = [];
        },
        calculateTotal() {
            if (this.editable && this.invoice_items) {
                let total = 0;
                for (let n = 0; n < this.invoice_items.length; n++) {
                    total += (this.invoice_items[n].quantity * this.invoice_items[n].unit_price);
                }
                return total.toFixed(2);
            } else {
                return this.total;
            }
        },
        initializeDatepicker() {
            let d = $("#datepicker");
            let self = this;
            d.datepicker({
                onSelect: function (selectedDate, datePicker) {
                    self.date = selectedDate;
                },
                dateFormat: 'yy-mm-dd'
            });
            // d.datepicker();
            d.datepicker("show");
        },
        findCategory(category) {
            let all_categories = this.$root.categories;

            for (let i = 0; i < all_categories.length; i++) {
                if (all_categories[i].id === category || all_categories[i].name === category) {
                    return all_categories[i].id;
                }
            }
        },
        findSupplier() {
            let supplier = this.supplier;
            let all_suppliers = this.$parent.suppliers;

            for (let i = 0; i < all_suppliers.length; i++) {
                if (all_suppliers[i].id === supplier || all_suppliers[i].name === supplier) {
                    return all_suppliers[i].id;
                }
            }
        },
        listenForEscape() {
            let vm = this;
            window.addEventListener('keyup', function (event) {
                // If ESC arrow was pressed...
                if (vm.$route.name === 'invoice' && event.keyCode === 27) {
                    vm.$router.go(-1);
                }
            });
        }
    },
    beforeRouteUpdate(to, from, next) {
        this.$router.push(to);
        next();
    }
};
module.exports = single_invoice;