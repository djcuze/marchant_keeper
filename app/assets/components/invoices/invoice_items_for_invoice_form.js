const invoice_items_for_invoice_form = {
    template: '#invoice_items_for_invoice_form_template',
    computed: {
        invoice_items() {
            return this.$parent.invoice_items;
        },
        invoice_items_to_delete() {
            return this.$parent.invoice_items_to_delete;
        }
    },
    methods: {
        initializeSelect(item, index) {
            let select = $('#category_select_' + index);
            $('#custom_select_' + index).hide();
            let self = item;
            select.selectmenu({
                select: function (event, ui) {
                    self.category = ui.item.value;
                }
            });
            select.selectmenu('open');
        },
        addField() {
            // The below code ensures that the last entry is not blank before rendering another form
            let array = this.invoice_items;
            let last_object = array[array.length - 1];

            if (array.length > 0 && this.emptyFieldCheck(last_object)) {
                this.$root.notice.message = "Please fill out the previous addition before adding a new field";
                this.$root.notice.state = 'error';
            }
            else {
                let table = $('#invoice').find("tbody");
                this.$parent.invoice_items.push({
                    description: null,
                    category: null,
                    quantity: null,
                    unit_price: null,
                });
            }
        },
        removeField(index, item) {
            if (this.emptyFieldCheck(item)) {
                this.$parent.invoice_items.splice(index, 1);
            } else {
                this.$parent.invoice_items.splice(index, 1);
                this.$parent.invoice_items_to_delete.push(item);
                this.undoRemoveField();
            }
        },
        undoRemoveField() {
            // A messy AF function that just resets atm (Future plan is to save a history and individually undo removals)
            if ($('#reset').length === 0) {
                let reset = '<button id="reset">Reset</button>';
                $('.add_fields__button').after(reset);
            }
            $('#reset').click((event) => {
                event.preventDefault();
                let index = this.$parent.invoice_items_to_delete.length - 1;
                let object = this.$parent.invoice_items_to_delete[index];
                // console.log(index);
                // console.log(object);
                this.$parent.invoice_items.push(object);
                this.$parent.invoice_items_to_delete.splice(index, 1);

                // Hide the button if there's no items to delete left
                if (this.$parent.invoice_items_to_delete.length === 0) {
                    $('#reset').remove();
                }
            });
        },
        emptyFieldCheck(obj) {
            let values = Object.values(obj);
            for (let i = 0; i < values.length; i++) {
                if (values[i] != null && obj.constructor === Object) {
                    return false;
                }
            }
            return true;
        }
    }
};
module.exports = invoice_items_for_invoice_form;