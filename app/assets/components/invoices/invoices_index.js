const invoices_index = {
    name: 'Invoices_index',
    template: '#invoices_index_template',
    data() {
        return {
            invoices: [
                // An array of objects collected on creation with fetchData()
            ],
            results_per_page: 15,
            page_number: 1,
            settings_enabled: null
        };
    },
    mounted() {
        this.$root.checkForLogin();
        this.fetchData();
    },
    components: {
        table_row: {
            template: '#table_row_template',
            name: 'Invoice',
            props: [
                'id',
                'number',
                'supplier',
                'category',
                'date',
                'total'
            ],
            data() {
                return {
                    delete_confirmation: null
                };
            },
            computed: {

            },
            methods: {
                deleteInvoice(id) {  // <=== There is a comment referring to this in _variables.scss
                    let invoices = this.$parent.invoices;
                    for (let i = 0; i < invoices.length; i++) {
                        if (invoices[i].id === this.id) {
                            this.$parent.invoices.splice(i, 1);
                            try {
                                axios.delete('/app/controllers/invoices.php', {
                                    data: {
                                        id: id
                                    }
                                }).then(() => {
                                    this.delete_confirmation = null;
                                    this.$root.notice.message = 'Invoice deleted successfully';
                                    this.$root.notice.state = 'success';
                                });
                            } catch (error) {
                                console.log(error);
                                this.$root.notice.message = 'There was an error deleting the invoice';
                                this.$root.notice.state = 'error';
                            }
                        }
                    }
                },
                deleteConfirmation() {
                    this.delete_confirmation = true;
                }
            }
        }
    },
    methods: {
        // changePage( :string = 'previous' || 'next')
        changePage(direction) {
            direction = direction.toLowerCase();
            if (direction === 'previous' && this.page_number > 1) {
                this.page_number -= 1;
            } else if (direction === 'next' && this.computedArray.length >= this.results_per_page) {
                this.page_number += 1;
            }
        },
        // Sends a GET request for all invoices and adds them to the component's invoices array
        //
        // IMPORTANT!!
        //
        // Delayed to visualize loading icon
        fetchData() {
            let self = this;
            self.$root.loading = true;
            axios.get("/app/controllers/invoices.php").then(response => {
                self.invoices = response.data;
                self.$root.loading = false;
            }).catch(error => {
                console.log(error);
            });
        },
        // Toggles visibility of "Options" div that contains buttons to an Edit form and a Destroy action accordingly
        toggleSettings() {
            let e = this.settings_enabled;
            this.settings_enabled = (function () {
                if (e === true) {
                    return null;
                } else {
                    return true;
                }
            })();
        },
        updateResultsPerPageBy(count) {
            let buttons = $('.settings__control > div > button');
            buttons.removeClass('active');
            let result = $(".settings__control > div > button:contains(" + count + ")");
            result.toggleClass('active');
            this.results_per_page = count;
        },
    },
    computed: {
        computedArray() {
            let first = (this.results_per_page * (this.page_number - 1));
            let last = this.results_per_page * this.page_number;
            return this.invoices.slice(first, last);
        }
    },
    beforeRouteUpdate(to, from, next) {
        next();
    }
};
module.exports = invoices_index;