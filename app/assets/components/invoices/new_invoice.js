let invoice_items_for_invoice_form = require('./invoice_items_for_invoice_form');
const new_invoice = {
    template: '#new_invoice_template',
    name: 'new_invoice',
    data() {
        return {
            number: null,
            date: null,
            supplier: null,
            invoice_items: [{}],
            total: null,
            editable: this.$route.params.editable
        };
    },
    methods: {
        calculateTotal() {
            if (this.editable) {
                let total = 0;
                for (let n = 0; n < this.invoice_items.length; n++) {
                    total += (this.invoice_items[n].quantity * this.invoice_items[n].unit_price);
                }
                return total;
            } else {
                return this.total;
            }
        },
        initializeDatepicker() {
            let d = $("#datepicker");
            let self = this;
            d.datepicker({
                onSelect: function (selectedDate, datePicker) {
                    self.date = selectedDate;
                },
                dateFormat: 'yy-mm-dd'
            });
            d.datepicker("show");
        },
        findSupplier() {
            let supplier = this.supplier;
            let all_suppliers = this.$root.suppliers;

            for (let i = 0; i < all_suppliers.length; i++) {
                if (all_suppliers[i].id === supplier || all_suppliers[i].name === supplier) {
                    return all_suppliers[i].id;
                }
            }
        },
        findCategory(category) {
            console.log(category);
            if (category === 'Botanical') {
                return 1;
            } else if (category === 'Receptacle') {
                return 2;
            } else if (category === 'Chemical') {
                return 3;
            } else if (category === 'Hardware') {
                return 4;
            } else {
                let all_categories = this.$root.categories;
                for (let i = 0; i < all_categories.length; i++) {
                    if (all_categories[i].id === category || all_categories[i].attributes.name === category) {
                        return all_categories[i].id;
                    }
                }
            }
        },
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#supplier_id').selectmenu({
                select: function (event, ui) {
                    self.supplier = ui.item.value;
                }
            });
            $('#supplier_id').selectmenu('open');

        },
        emptyFieldCheck() {
            let self = this;
            for (let i = 0; i < this.invoice_items.length; i++) {
                let obj = self.invoice_items[i];
                if (Object.keys(obj).length === 0 && obj.constructor === Object) {
                    self.invoice_items.splice(i, 1);
                } else {
                    for (let value in obj) {
                        if (obj[value] === null) {
                            self.invoice_items.splice(i, 1);
                        }
                    }
                }
            }
        },
        validate() {
            this.emptyFieldCheck();
            // Invoice Form is empty
            if (!this.date || !this.number || !this.supplier) {
                return 'Error: Please fill out all fields';
            } else if (this.invoice_items.length <= 0) {
                return 'Error: Please add at least one item to the invoice';
            } else {
                return true;
            }
        },
        submit() {
            if (this.validate() === true) {
                try {
                    axios.post('../app/controllers/invoices.php', {
                        date: this.date,
                        number: this.number,
                        supplier_id: this.findSupplier(),
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then((response) => {
                        let invoice_items = this.invoice_items;
                        for (let n = 0; n < invoice_items.length; n++) {
                            let self = invoice_items[n];
                            if (Object.keys(self).length === 0 && self.constructor === Object) {
                                break;
                            } else {
                                axios.post('../app/controllers/invoice_items.php', {
                                    invoice_id: response.data.id,
                                    description: invoice_items[n].description,
                                    category_id: this.findCategory(invoice_items[n].category),
                                    quantity: invoice_items[n].quantity,
                                    unit_price: invoice_items[n].unit_price
                                })
                            }
                        }
                        this.$router.push('/invoices/' + response.data.id);
                        this.$root.notice.message = 'Invoice created successfully';
                        this.$root.notice.state = 'success';
                    });
                } catch (error) {
                    console.log(error);
                }
            } else {
                this.$root.notice.message = this.validate();
                this.$root.notice.state = 'error';
            }
        }
    },
    components: {
        invoice_items_for_invoice_form
    }
};

module.exports = new_invoice;