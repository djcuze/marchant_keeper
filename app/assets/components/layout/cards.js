const cards = {
    name: 'cards',
    data() {
        return {
            cards: this.$root.pages
        };
    },
    template: '#cards'
};

module.exports = cards;