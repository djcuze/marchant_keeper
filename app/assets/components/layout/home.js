let cards = require('./cards.js');

const home = {
    name: 'home',
    template: '#home',
    components: {
        cards
    }
};
module.exports = home;
