const login = {
    template: '#login_template',
    data() {
        return {
            username: null,
            password: null
        };
    },
    methods: {
        submit() {
            axios.post('/app/lib/login.php', {
                username: this.username,
                password: this.password
            }).then((response) => {
                this.$root.notice.message = response.data.message;
                this.$root.notice.state = response.data.status === 'authenticated' ? 'success' : 'error';
                if (response.data.status === 'authenticated') {
                    this.$root.auth = true;
                    this.$router.push('/');
                } else {

                }
            })
        }
    }
};

module.exports = login;