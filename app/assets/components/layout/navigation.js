const navigation = {
    name: 'navigation',
    data() {
        return {
            pages: this.$root.pages,
        };
    },
    template: '#navigation',
    mounted() {
        this.checkForDefaultImages();
    },
    methods: {
        checkForDefaultImages() {
            axios.get('/app/controllers/inventory.php').then((response) => {
                for (let items of response.data) {
                    if (items['attributes']['image_path'] === 'http://placehold.it/300/300') {
                        this.$root.inventory_items_with_default_image.push(items);
                    }
                }
            })
        }
    }
};

module.exports = navigation;