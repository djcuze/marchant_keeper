// Site
let home_page = require('./layout/home');
let login = require('./layout/login');

// let products = require('./products/products');

// Invoices
let single_invoice = require('./invoices/invoice');
let invoices_index = require('./invoices/invoices_index');
let new_invoice = require('./invoices/new_invoice');

// Inventory
let inventory_index = require('./inventory/inventory_index');
let single_inventory_item = require('./inventory/inventory_item');
let new_inventory_item = require('./inventory/new_inventory_item');

// Dashboard

let default_images_to_change = require('./dashboard/default_images_to_change');

// Routes
const routes = [
    {path: '/', component: home_page},
    {
        path: '/login',
        component: login
    },
    // Invoices
    {path: '/invoices', component: invoices_index, props: true},
    {name: 'new_invoice', path: '/invoices/new', component: new_invoice},
    {name: 'invoice', path: '/invoices/:id', component: single_invoice},

    // Inventory
    {path: '/inventory', component: inventory_index, props: true},
    {name: 'new_inventory_item', path: '/inventory/new', component: new_inventory_item},
    {name: 'inventory_item', path: '/inventory/:id', component: single_inventory_item},

    // Products - INCOMPLETE
    // {path: '/products', component: products},

    // Dashboard
    {path: '/dashboard', component: default_images_to_change}

];
const router = new VueRouter({
    routes,
    mode: 'history'
});

module.exports = router;