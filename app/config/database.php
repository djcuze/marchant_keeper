<?php

class Database
{
    private $db;
    private $password;
    private $username;
    private $host;
    private $database;
    public $conn;

    public function __construct()
    {
        try {
//            //Get Heroku ClearDB connection information
            if (file_exists(__DIR__ . '/../../secrets.php')) {
                include_once __DIR__ . '/../../secrets.php';
            }
            $uri = getenv("CLEARDB_DATABASE_URL");
            $cleardb_url = parse_url($uri);
            $this->host = $cleardb_url["host"];
            $this->username = $cleardb_url["user"];
            $this->password = $cleardb_url["pass"];
            $this->database = substr($cleardb_url["path"], 1);
            $this->db = new PDO("mysql:dbname=$this->database;host=$this->host", $this->username, $this->password);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get the database connection
    public function getConnection()
    {
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:dbname=$this->database;host=$this->host", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->exec("SET NAMES 'UTF8'");
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
            exit();
        }
        return $this->conn;
    }
}
