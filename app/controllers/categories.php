<?php
require_once '../config/headers.php';
require_once '../config/database.php';
require_once '../lib/sanitize.php';
require_once '../models/category.php';

$db = new Database();
$db = $db->getConnection();

$params = file_get_contents("php://input");
$category = new App\Models\Category($db, $params);

if ($_SERVER['REQUEST_METHOD'] !== NULL) {
    $r = $_SERVER['REQUEST_METHOD'];
    switch ($r) {
        case 'GET':
            $stmt = isset($_GET['id']) ? $category->read($_GET['id']) : $category->readAll();
            if ($stmt) {
                $result = isset($_GET['id']) ? $stmt->fetch(PDO::FETCH_ASSOC) : $stmt->fetchAll(PDO::FETCH_ASSOC);
                http_response_code(200);
                $result = $category->construct_json($result);
            } else {
                http_response_code(400);
            }
            break;
        case 'POST':
            $stmt = $category->create();
            http_response_code(201);
            $result = json_decode($params);
            $result->id = $db->lastInsertId();
            $result = $category->read($result->id);
            $result = $result->fetch(PDO::FETCH_ASSOC);
            $result = $category->construct_json($result);
            break;
        case 'PATCH':
            $obj = json_decode($params);
            $stmt = $category->update($obj->id);
            http_response_code(200);
            $result = $category->read($obj->id);
            $result = $result->fetch(PDO::FETCH_ASSOC);
            $result = $category->construct_json($result);
            break;
        case 'DELETE':
            $obj = json_decode($params);
            $stmt = $category->delete($obj->id);
            $code = $stmt->rowCount() === 1 ? 200 : 404;
            http_response_code($code);
            break;
        default:
            http_response_code(404);
            exit();
    }
    if ($stmt && $stmt->rowCount() > 0) {
        print_r(json_encode($result));
        exit();
    } else {
        http_response_code(404);
    };
}
