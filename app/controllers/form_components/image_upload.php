<?php
chdir('../../../');
$currentDir = getcwd();
$uploadDirectory = "/public/images/uploads/";

$errors = [];

$fileExtensions = ['jpeg', 'jpg', 'png'];


$file = new stdClass();

$file->name = $_FILES['photo']['name'];
$file->size = $_FILES['photo']['size'];
$file->tmp_name = $_FILES['photo']['tmp_name'];
$file->type = $_FILES['photo']['type'];
$tmp = explode('.', $file->name);
$fileExtension = strtolower(end($tmp));

$uploadPath = $currentDir . $uploadDirectory . basename($file->name);


if (!in_array($fileExtension, $fileExtensions)) {
    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
}

if ($file->size > 2000000) {
    $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
}

if (empty($errors)) {
    $didUpload = move_uploaded_file($file->tmp_name, $uploadPath);

    if ($didUpload) {
        print_r(json_encode($file, JSON_NUMERIC_CHECK));
    } else {
        echo "An error occurred somewhere. Try again or contact the admin";
    }
} else {
    foreach ($errors as $error) {
        echo $error . "These are the errors" . "\n";
    }
}