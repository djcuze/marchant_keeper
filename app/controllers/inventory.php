<?php
require_once '../config/headers.php';
require_once '../config/database.php';
require_once '../lib/sanitize.php';
require_once '../models/inventory.php';

$db = new Database();
$db = $db->getConnection();

$params = file_get_contents("php://input");
$inventory_item = new App\Models\Inventory($db, $params);

if ($_SERVER['REQUEST_METHOD'] !== NULL) {
    $r = $_SERVER['REQUEST_METHOD'];
    switch ($r) {
        case 'GET':
            $stmt = isset($_GET['id']) ? $inventory_item->read($_GET['id']) : $inventory_item->readAll();
            if ($stmt) {
                $result = isset($_GET['id']) ? $stmt->fetch(PDO::FETCH_ASSOC) : $stmt->fetchAll(PDO::FETCH_ASSOC);
                http_response_code(200);
            } else {
                http_response_code(400);
            }
            break;
        case 'POST':
            if ($inventory_item->create()) {
                http_response_code(201);
            }
            $stmt = $inventory_item->create();
            $result = $inventory_item->read($db->lastInsertId());
            $result = $result->fetch(PDO::FETCH_ASSOC);
            break;
        case 'PATCH':
            $obj = json_decode($params);
            $stmt = $inventory_item->update($obj->id);
            $result = $inventory_item->read($obj->id);
            $result = $result->fetch(PDO::FETCH_ASSOC);
            break;
        case 'DELETE':
            $obj = json_decode($params);
            $stmt = $inventory_item->delete($obj->id);
            $result = 'Inventory Item deleted';
            break;
        default:
            exit();
    }
    $result = $inventory_item->construct_json($result);
    if ($stmt) {
        print_r(json_encode($result));
        exit();
    } else {
        http_response_code(404);
    };
}
