<?php
require_once '../config/headers.php';
require_once '../config/database.php';
require_once '../lib/sanitize.php';
require_once '../lib/check_invoice_item_against_existing_inventory_items.php';
require_once '../models/invoice_item.php';
require_once '../models/inventory.php';

$db = new Database();
$db = $db->getConnection();

$params = file_get_contents("php://input");
$invoice_item = new App\Models\InvoiceItem($db, $params);

if ($_SERVER['REQUEST_METHOD'] !== NULL) {
    $r = $_SERVER['REQUEST_METHOD'];
    switch ($r) {
        case 'GET':
            $stmt = $invoice_item->readAllForInvoice($_GET['id']);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            break;
        case 'POST':
            print_r($params);
            $result = json_decode($params);
            $potential_match = CheckInvoiceItemsAgainstExistingInventoryItems::queryDatabase($result->description, $db);
            $inventory_item = new App\Models\Inventory($db, $params);
            //
            // Searches for an existing entry in the Inventory table.
            // If one is found, it updates its quantity
            if ($potential_match) {
                $inventory_item->updateQuantity($potential_match['id'], $result->quantity);
                $invoice_item->inventory_item_id = $potential_match['id'];
            } else {
                $inventory_item->category_id = $inventory_item->category_id;
                // creates a new entry
                $inventory_item->create();
                $invoice_item->inventory_item_id = $db->lastInsertId();
            }
            if ($invoice_item->create()) {
                http_response_code(200);
            } else {
                http_response_code(404);
            }
            $stmt = $invoice_item->create();
            $result->inventory_item_id = $invoice_item->inventory_item_id;
            $result->id = $db->lastInsertId();
            break;
        case 'PATCH':
            $obj = json_decode($params);
            $stmt = $invoice_item->update();
            $result = $invoice_item->read($obj->id);
            $result = $result->fetch(PDO::FETCH_ASSOC);
            break;
        case 'DELETE':
            $obj = json_decode($params);
            $stmt = $invoice_item->delete($obj->id, 2);
            $result = 'Invoice Item Deleted Successfully';
            break;
        default:
            exit();
    }
    if (isset($stmt)) {
        print_r(json_encode($result, JSON_NUMERIC_CHECK));
    } else exit();
}
