<?php
require_once '../config/headers.php';
require_once '../config/database.php';
require_once '../lib/sanitize.php';
require_once '../models/invoice.php';

$db = new Database();
$db = $db->getConnection();

$params = file_get_contents("php://input");
$invoice = new App\Models\Invoice($db, $params);

if ($_SERVER['REQUEST_METHOD'] !== NULL) {
    $r = $_SERVER['REQUEST_METHOD'];
    switch ($r) {
        case 'GET':
            $stmt = isset($_GET['id']) ? $invoice->read($_GET['id']) : $invoice->readAll();
            if ($stmt) {
                $result = isset($_GET['id']) ? $stmt->fetch(PDO::FETCH_ASSOC) : $stmt->fetchAll(PDO::FETCH_ASSOC);
                http_response_code(200);
                $result = $invoice->construct_json($result);
            } else {
                http_response_code(400);
            }
            break;
        case 'POST':
            if ($invoice->create()) {
                http_response_code(201);
            }
            $stmt = $invoice->create();
            $result = $invoice->read($db->lastInsertId());
            $result = $result->fetch(PDO::FETCH_ASSOC);
            break;
        case 'PATCH':
            $obj = json_decode($params);
            $stmt = $invoice->update($obj->id);
            $result = $invoice->read($obj->id);
            $result = $result->fetch(PDO::FETCH_ASSOC);
            $result = $invoice->construct_json($result);
            break;
        case 'DELETE':
            $obj = json_decode($params);
            $stmt = $invoice->delete($obj->id);
            $result = 'Invoice Deleted';
            break;
        default:
            http_response_code(404);
            exit();
    }
    if ($stmt && $stmt->rowCount() > 0) {
        print_r(json_encode($result));
        exit();
    } else {
        http_response_code(404);
    };
}
