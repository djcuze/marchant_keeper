<?php
require_once '../config/headers.php';
require_once '../config/database.php';
require_once '../lib/sanitize.php';
require_once '../models/supplier.php';

$db = new Database();
$db = $db->getConnection();

$params = file_get_contents("php://input");
$supplier = new App\Models\Supplier($db, $params);

if ($_SERVER['REQUEST_METHOD'] !== NULL) {
    $r = $_SERVER['REQUEST_METHOD'];
    switch ($r) {
        case 'GET':
            $stmt = isset($_GET['id']) ? $supplier->read($_GET['id']) : $supplier->readAll();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            break;
        case 'POST':
            $stmt = $supplier->create();
            $result = json_decode($params);
            $result->id = $db->lastInsertId();
            break;
        case 'PATCH':
            $obj = json_decode($params);
            $stmt = $supplier->update($obj->id);
            $result = $supplier->read($obj->id);
            $result = $result->fetch(PDO::FETCH_ASSOC);
            break;
        case 'DELETE':
            $obj = json_decode($params);
            $stmt = $supplier->delete($obj->id);
            $result = '';
            break;
        default:
            exit();
    }
    if (isset($stmt)) {
        print_r(json_encode($result, JSON_NUMERIC_CHECK));
    } else exit();
}
