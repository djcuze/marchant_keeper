<?php

class CheckInvoiceItemsAgainstExistingInventoryItems
{
    public static function queryDatabase($description, $db)
    {
        $input = htmlspecialchars(strip_tags($description), ENT_NOQUOTES);

        $query = ("SELECT * FROM inventory_items WHERE description = :description");
        $stmt = $db->prepare($query);
        $stmt->bindParam(':description', $input);
        $stmt->execute();

        $match = $stmt->fetch(PDO::FETCH_ASSOC);
        return $match ? $match : false;
    }
}