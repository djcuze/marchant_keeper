<?php

//namespace App\Functions;

class ImageCheck
{
    private $conn;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public static function fetchData($dbh)
    {
        $dba = new Database();
        $db = $dba->getConnection();
        include_once __DIR__ . '/../models/inventory_item.php';
        $images_to_change = [];
        $query = ('SELECT * FROM inventory_items WHERE image_path LIKE :default ');
        $stmt = $db->prepare($query);
        $default = '%placehold%';
        $stmt->bindParam(':default', $default);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        for ($i = 0; $i < sizeof($result); $i++) {
            $item = new \App\Models\InventoryItem($db);
            $item->id = $result[$i]['id'];
            $item->description = $result[$i]['description'];
            $item->image_path = $result[$i]['image_path'];
            $item->category = $result[$i]['category_id'];
            $item->quantity = $result[$i]['quantity'];
            array_push($images_to_change, $item);
        }
        return $images_to_change;
    }

    public function requiresUpload($size)
    {
        if ($size > 0) {
            $this->images_to_change = $this->fetchData($this->conn);
            return true;
        } else {
            return false;
        }
    }
}