<?php
class Func
{
    public static function checkLoginState($db)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_COOKIE['user_id']) && isset($_COOKIE['token']) && isset($_COOKIE['serial'])) {
            $query = "SELECT * FROM sessions WHERE sessions_user_id = :user_id AND token = :token AND serial = :serial";

            $user_id = $_COOKIE['user_id'];
            $token = $_COOKIE['token'];
            $serial = $_COOKIE['serial'];

            $stmt = $db->prepare($query);
            $stmt->execute(array(
                ':user_id' => $user_id,
                ':token' => $token,
                ':serial' => $serial));

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($row['sessions_user_id'] > 0) {
                if (
                    $row['sessions_user_id'] == $_COOKIE['user_id'] &&
                    $row['token'] == $_COOKIE['token'] &&
                    $row['serial'] == $_COOKIE['serial']
                ) {
                    if (
                        $row['sessions_user_id'] == $_SESSION['user_id'] &&
                        $row['token'] == $_SESSION['token'] &&
                        $row['serial'] == $_SESSION['serial']
                    ) {
                        return true;
                    } else {
                        func::createSession($_COOKIE['username'], $_COOKIE['user_id'], $_COOKIE['token'], $_COOKIE['serial']);
                        return true;
                    }
                }
            }
            // Not logged in
        } else {
            $_SESSION['logged_in'] = false;
            return false;
        };
    }

    public static function createRecord($db, $username, $user_id)
    {
        $query = "INSERT INTO sessions (sessions_user_id, token, serial) VALUES (:user_id, :token, :serial)";
        $db->prepare('DELETE FROM sessions WHERE sessions_user_id = :sessions_user_id')->execute(array(':sessions_user_id' => $user_id));

        $token = func::createString(30);
        $serial = func::createString(30);

        func::createCookie($username, $user_id, $token, $serial);
        func::createSession($username, $user_id, $token, $serial);

        $stmt = $db->prepare($query);

        $stmt->execute(array(':user_id' => $user_id, ':token' => $token, ':serial' => $serial));
    }

    public static function createCookie($username, $user_id, $token, $serial)
    {
        setcookie('user_id', $user_id, time() + (86400) * 30, "/");
        setcookie('username', $username, time() + (86400) * 30, "/");
        setcookie('token', $token, time() + (86400) * 30, "/");
        setcookie('serial', $serial, time() + (86400) * 30, "/");
    }

    public static function deleteCookies($username, $user_id, $token, $serial)
    {
        setcookie('user_id', $user_id, time() - 1, "/");
        setcookie('username', $username, time() - 1, "/");
        setcookie('token', $token, time() - 1, "/");
        setcookie('serial', $serial, time() - 1, "/");
    }

    public static function createSession($username, $user_id, $token, $serial)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['user_id'] = $user_id;
        $_SESSION['token'] = $token;
        $_SESSION['serial'] = $serial;
        $_SESSION['username'] = $username;
    }

    public static function createString($len)
    {
        $string = "712jfyhf91u2fkAsu198hrnsc92js";

        return substr(str_shuffle($string), 0, 30);
    }
}