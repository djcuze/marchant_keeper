<?php

// Note that this class is never used in Production. 
// 'Here if I need it'
require_once __DIR__ . '/sanitize.php';

class InvoiceTransaction
{
    public $invoice;
    public $invoice_items = [];
    public $quantities_to_update = [];
    private $conn;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    public function addInvoiceItem($invoice_item)
    {
        array_push($this->invoice_items, $invoice_item);
    }

    public function begin()
    {
        $db = $this->conn;
        $db->beginTransaction();

        // Insert the invoice
        $query = "INSERT INTO invoices (number,date,supplier_id) VALUES (:number, :date, :supplier_id)";
        $number = sanitize($this->invoice->number);
        $date = sanitize($this->invoice->date);
        $supplier_id = sanitize($this->invoice->supplier);
        $stmt = $db->prepare($query);
        $stmt->bindParam(':number', $number);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam('supplier_id', $supplier_id);
        $stmt->execute();

        $invoice_id = $db->lastInsertId();
        // Insert the Invoice Items
        $this->assemble($this->invoice_items, $invoice_id);
        return true;
    }

    public function assemble($array, $invoice_id)
    {
        for ($i = 0; $i < sizeof($array); $i++) {
            // Returns an object if one is found
            $match = CheckInvoiceItemsAgainstExistingInventoryItems::queryDatabase($array[$i]->description, $this->conn);
            if ($match !== false) {
                $this->updateInventoryItemQuantity($match['id'], $match['quantity']);
                $this->invoice_items[$i]->inventory_item_id = $match['id'];
            } else {
                $this->createInventoryItem($array[$i]);
                $array[$i]->inventory_item_id = $this->conn->lastInsertId();
            }
            $array[$i]->invoice_id = $invoice_id;
            $this->createInvoiceItem($array[$i]);
        };
        return $array;
    }

    private function createInventoryItem($item)
    {
        $query = "INSERT INTO inventory_items (description, quantity, category_id) VALUES (:description, :quantity, :category_id)";
        $stmt = $this->conn->prepare($query);

        $quantity = sanitize($item->quantity);
        $description = sanitize($item->description);
        $category_id = sanitize($item->category);

        $stmt->bindParam(':quantity', $quantity);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':category_id', $category_id);
        if ($stmt->execute()) {
            return true;
        } else return false;
    }


    private function createInvoiceItem($item)
    {
        $query = "INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (:quantity, :unit_price, :inventory_item_id, :invoice_id)";
        $stmt = $this->conn->prepare($query);

        $quantity = sanitize($item->quantity);
        $unit_price = sanitize($item->unit_price);
        $inventory_item_id = sanitize($item->inventory_item_id);
        $invoice_id = sanitize($item->invoice_id);

        $stmt->bindParam(':quantity', $quantity);
        $stmt->bindParam(':unit_price', $unit_price);
        $stmt->bindParam(':inventory_item_id', $inventory_item_id);
        $stmt->bindParam(':invoice_id', $invoice_id);
        if ($stmt->execute()) {
            return true;
        } else return false;
    }

// WARNING: As this is a private function, it has had its tests in '/tests/unit/InvoiceTransactionTest' commented out.
    private function updateInventoryItemQuantity($id, $quantity)
    {
        $query = ("UPDATE inventory_items SET quantity = :quantity WHERE id = :id");
        $stmt = $this->conn->prepare($query);
        $id = sanitize($id);
        $quantity = sanitize($quantity);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':quantity', $quantity);
        $stmt->execute();
        // Returns 0 for false, 1 for true
        return $stmt->rowCount() > 0;
    }
}