<?php
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/./function.php';

$database = new Database();
$db = $database->getConnection();

$data = json_decode(file_get_contents("php://input"));

// If Not Logged In, Login
if (!func::checkLoginState($db)) {
    if (!isset($_SESSION)) {
        session_start();
    };
    if (isset($data->password) && isset($data->username)) {
        $query = "SELECT * FROM users WHERE name = :username AND pass = :password";

        // Sanitize
        $username = htmlspecialchars(strip_tags($data->username), ENT_NOQUOTES);
        $password = htmlspecialchars(strip_tags($data->password), ENT_NOQUOTES);

        $stmt = $db->prepare($query);
        $stmt->execute(array(':username' => $username, ':password' => $password));

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row['user_id'] > 0) {
            func::createRecord($db, $row['name'], $row['user_id']);
            $data->message = 'Login Successful';
            $data->status = 'authenticated';
        } else {
            $data->message = 'Error: Username and/or password incorrect';
            $status = 'not_authenticated';
        }
    } else {
        $data->message = 'Please enter a username and password';
        $data->status = 'not_authenticated';
    }
} else {
    $data->message = 'Already Logged in';
    $data->status = 'authenticated';
}
print_r(json_encode($data));