<?php include_once('./function.php');
func::deleteCookies($_COOKIE['username'], $_COOKIE['user_id'], $_COOKIE['token'], $_COOKIE['serial']);
session_destroy();
session_start();
$_SESSION['authenticated'] = false;
header('location:/');
