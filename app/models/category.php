<?php

namespace App\Models;

class Category
{
    private $conn;
    public $name;

    public function __construct($db, $params)
    {
        $this->conn = $db;
        $this->validate($params);
    }

    function readAll()
    {
        $query = "SELECT * FROM categories ORDER BY id ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function read($id)
    {
        try {
            $query = ("SELECT * FROM categories WHERE id = :id");
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (\PDOException $e) {
            print_r($e);
            exit();
        }
    }

    function create()
    {
        try {
            $query = ("INSERT INTO categories (name) VALUES (:name)");
            $stmt = $this->conn->prepare($query);
            $name = sanitize($this->name);
            $stmt->bindParam(":name", $name);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (\PDOException $e) {
            print_r($e);
            exit();
        }
    }

    function update($id)
    {
        try {
            $query = ("UPDATE categories SET name = :name WHERE id = :id");
            $stmt = $this->conn->prepare($query);
            $name = sanitize($this->name);
            $id = sanitize($id);
            $stmt->bindParam(":name", $name);
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (Exception $e) {
            print_r($e);
            exit();
        }
    }

    function delete($id)
    {
        try {
            $query = "DELETE FROM categories WHERE id = :id";
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return $stmt;
            } else return false;
        } catch (\PDOException $e) {
            exit();
        }
    }

    private function validate($params)
    {
        $params = json_decode($params);
        $this->name = isset($params->name) ? $params->name : null;
    }

    public function construct_json($input)
    {
        if ($_SERVER["REQUEST_METHOD"] === "GET" && !isset($_GET["id"])) {
            foreach ($input as &$category) {
                $attributes = array("attributes" => [
                    "name" => $category["name"]
                ]);
                $id = array("id" => (integer)$category["id"]);
                $category = array("type" => 'categories') + $id + $attributes;
            }
        } else {
            $attributes = array("attributes" => [
                "name" => $input["name"]
            ]);
            $id = array("id" => (integer)$input["id"]);
            $input = array("type" => 'category') + $id + $attributes;
        }
        $output = $input;
        return $output;
    }
}
