<?php

namespace App\Models;

class Inventory
{
    private $conn;
    public $description;
    public $quantity;
    public $image_path = 'http://placehold.it/300/300';
    public $category_id;

    // constructor with $db as database connection
    // also requires $params to be passed to it upon creation
    public function __construct($db, $params = null)
    {
        $this->conn = $db;
        if ($params) {
            $this->validate($params);
        }
    }

    function readAll()
    {
        // select all query
        $query = "SELECT * FROM inventory_items ORDER BY id ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function read($id)
    {
        try {
            $query = ("SELECT * FROM inventory_items WHERE id = :id");
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e ? $e->getMessage() : exit();
        }
    }

    function create()
    {
        // query to insert record
        try {
            $query = ("INSERT INTO inventory_items (description, quantity, image_path, category_id) VALUES (:description, :quantity, :image_path, :category_id)");
            $stmt = $this->conn->prepare($query);

            $description = sanitize($this->description);
            $quantity = sanitize($this->quantity);
            $category_id = sanitize($this->category_id);
            $image_path = sanitize($this->image_path);

            $stmt->bindParam(":description", $description);
            $stmt->bindParam(":quantity", $quantity);
            $stmt->bindParam(":image_path", $image_path);
            $stmt->bindParam(":category_id", $category_id);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            return $e ? $e->getMessage() : exit();
        }
    }

    function update($id)
    {
        try {
            $query = ("UPDATE  inventory_items 
                      SET     description = :description,
                              quantity  = :quantity,
                              category_id = :category_id,
                              image_path  = :image_path
                      WHERE   id = :id");
            $stmt = $this->conn->prepare($query);
            // Sanitize
            $description = sanitize($this->description);
            $quantity = sanitize($this->quantity);
            $category_id = sanitize($this->category_id);
            $image_path = sanitize($this->image_path);
            $id = sanitize($id);
            // Bind Params
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':quantity', $quantity);
            $stmt->bindParam(':category_id', $category_id);
            $stmt->bindParam(':image_path', $image_path);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e;
            exit();
        }
    }

    function updateQuantity($id, $quantity)
    {
        try {
            $stmt = $this->read($id);
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $this->quantity = $result['quantity'];

            $query = ("UPDATE inventory_items SET quantity = :quantity WHERE id = :id");
            $stmt = $this->conn->prepare($query);
            // Calculates the new quantity
            $this->quantity = $this->quantity + $quantity;
            $quantity = sanitize($this->quantity);
            $id = sanitize($id);
            $stmt->bindParam(":quantity", $quantity);
            $stmt->bindParam(":id", $id);
            $stmt->execute();
        } catch (\PDOException $e) {
            exit();
        }
    }

    function delete($id)
    {
        try {
            $query = "DELETE FROM inventory_items WHERE id = :id";
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e ? $e->getMessage() : exit();
        }
    }

    private function validate($params)
    {
        $params = json_decode($params);
        $this->description = isset($params->description) ? $params->description : null;
        $this->quantity = isset($params->quantity) ? $params->quantity : null;
        $this->category_id = isset($params->category_id) ? $params->category_id : null;
        $this->image_path = isset($params->image_path) ? $params->image_path : 'http://placehold.it/300/300';
    }

    public function construct_json($input)
    {
        if ($_SERVER["REQUEST_METHOD"] === "GET" && !isset($_GET["id"])) {
            foreach ($input as &$inventory_item) {
                $attributes = array("attributes" => [
                    "description" => $inventory_item["description"],
                    "quantity" => (integer)$inventory_item["quantity"],
                    "image_path" => $inventory_item["image_path"],
                    "category" => (integer)$inventory_item["category_id"]
                ]);
                $id = array("id" => (integer)$inventory_item["id"]);
                $inventory_item = array("type" => 'inventory_items') + $id + $attributes;
            }
        } else {
            $attributes = array("attributes" => [
                "description" => $input["description"],
                "quantity" => (integer)$input["quantity"],
                "image_path" => $input["image_path"],
                "category" => (integer)$input["category_id"]
            ]);
            $id = array("id" => (integer)$input["id"]);
            $input = array("type" => 'invoices') + $id + $attributes;
        }
        $output = $input;
        return $output;
    }
}