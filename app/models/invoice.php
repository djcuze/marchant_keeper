<?php

namespace App\Models;

class Invoice
{
    private $conn;
    public $number;
    public $date;
    public $supplier_id;
    public $total;

    // constructor with $db as database connection
    // also requires $params to be passed to it upon creation
    public function __construct($db, $params)
    {
        $this->conn = $db;
        $this->validate($params);
    }

    function readAll()
    {
        try {
            $query = "SELECT * FROM invoices_with_total ORDER BY id ASC";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    function read($id)
    {
        try {
            $query = "SELECT * FROM invoices_with_total WHERE id = :id";
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            print_r($e);
            exit();
        }
    }

    function create()
    {
        try {
            $query = ("INSERT INTO invoices (number, date, supplier_id) VALUES (:number, :date, :supplier_id)");
            $stmt = $this->conn->prepare($query);

            $number = sanitize($this->number);
            $date = sanitize($this->date);
            $supplier_id = sanitize($this->supplier_id);

            $stmt->bindParam(":number", $number);
            $stmt->bindParam(":date", $date);
            $stmt->bindParam(":supplier_id", $supplier_id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (\PDOException $e) {
            print_r($e->getMessage());
            exit();
        }
    }

    function update($id)
    {
        try {
            $query = ("UPDATE invoices 
                      SET     number = :number,
                              date = :date,
                              supplier_id = :supplier_id
                      WHERE   id = :id");
            $stmt = $this->conn->prepare($query);
            // Sanitize
            $number = sanitize($this->number);
            $date = sanitize($this->date);
            $supplier_id = sanitize($this->supplier_id);
            $id = sanitize($id);
            // Bind Params
            $stmt->bindParam(':number', $number);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':supplier_id', $supplier_id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e;
            exit();
        }
    }

    function delete($id)
    {
        try {
            $query = "DELETE FROM invoices WHERE id = :id";
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (\PDOException $e) {
            return $e ? 'Error deleting invoice' : exit();
        }
    }

    private function validate($params)
    {
        $params = json_decode($params);
        $this->number = isset($params->number) ? $params->number : null;
        $this->date = isset($params->date) ? $params->date : null;
        $this->supplier_id = isset($params->supplier_id) ? $params->supplier_id : null;
    }

    public function construct_json($input)
    {
        if ($_SERVER["REQUEST_METHOD"] === "GET" && !isset($_GET["id"])) {
            foreach ($input as &$invoice) {
                $attributes = array("attributes" => [
                    "number" => $invoice["number"],
                    "date" => $invoice["date"],
                    "total" => (string)$invoice["total"],
                    "supplier" => $invoice["supplier"],
                    "category" => $invoice["category"]
                ]);
                $id = array("id" => (integer)$invoice["id"]);
                $invoice = array("type" => 'invoices') + $id + $attributes;
            }
        } else {
            $attributes = array("attributes" => [
                "number" => $input["number"],
                "date" => $input["date"],
                "total" => (string)$input["total"],
                "supplier" => $input["supplier"],
                "category" => $input["category"]
            ]);
            $id = array("id" => (integer)$input["id"]);
            $input = array("type" => 'invoices') + $id + $attributes;
        }
        $output = $input;
        return $output;
    }
}