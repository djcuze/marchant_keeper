<?php

namespace App\Models;

class InvoiceItem
{
    private $conn;
    public $description;
    public $quantity;
    public $unit_price;
    public $invoice_id;
    public $inventory_item_id;
    public $id;

    // constructor with $db as database connection
    // also requires $params to be passed to it upon creation
    public function __construct($db, $params)
    {
        $this->conn = $db;
        $this->validate($params);
    }

    function readAllForInvoice($invoice_id)
    {
        // select all query
        $query = ("SELECT * FROM invoice_items_with_description_and_category WHERE invoice_id = :invoice_id");
        $stmt = $this->conn->prepare($query);
        $invoice_id = sanitize($invoice_id);
        $stmt->bindParam(":invoice_id", $invoice_id);
        $stmt->execute();
        return $stmt;
    }

    function create()
    {
        try {
            // query to insert record
            $query = ("
            INSERT INTO invoice_items (quantity, unit_price, invoice_id, inventory_item_id) 
            VALUES (:quantity, :unit_price, :invoice_id, :inventory_item_id)");
            $stmt = $this->conn->prepare($query);
            $quantity = sanitize($this->quantity);
            $unit_price = sanitize($this->unit_price);
            $invoice_id = sanitize($this->invoice_id);
            $inventory_item_id = sanitize($this->inventory_item_id);

            $stmt->bindParam(":quantity", $quantity);
            $stmt->bindParam(":unit_price", $unit_price);
            $stmt->bindParam(":invoice_id", $invoice_id);
            $stmt->bindParam(":inventory_item_id", $inventory_item_id);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e ? $e->getMessage() : exit();
        }
    }

    function update()
    {
        try {
            // update query
            $query = ("
            UPDATE  invoice_items 
            SET     quantity = :quantity, 
                    unit_price = :unit_price
            WHERE   invoice_id = :invoice_id
            AND     inventory_item_id = :inventory_item_id
            ");
            $stmt = $this->conn->prepare($query);

            $quantity = sanitize($this->quantity);
            $unit_price = sanitize($this->unit_price);
            $invoice_id = sanitize($this->invoice_id);
            $inventory_item_id = sanitize($this->inventory_item_id);

            $stmt->bindParam(":quantity", $quantity);
            $stmt->bindParam(":unit_price", $unit_price);
            $stmt->bindParam(":invoice_id", $invoice_id);
            $stmt->bindParam(":inventory_item_id", $inventory_item_id);

            $stmt->execute();
            // Still need to adjust quantity at this point
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e ? $e->getMessage() : exit();
        }
    }

    function delete($invoice_id, $inventory_item_id)
    {
        try {
            $query = "DELETE FROM invoice_items WHERE invoice_id = :invoice_id AND inventory_item_id = :inventory_item_id";
            $stmt = $this->conn->prepare($query);
            $invoice_id = sanitize($invoice_id);
            $inventory_item_id = sanitize($inventory_item_id);
            $stmt->bindParam(':invoice_id', $invoice_id);
            $stmt->bindParam(':inventory_item_id', $inventory_item_id);
            $stmt->execute();
            return ($stmt->rowCount() === 1) ? $stmt : false;
        } catch (\PDOException $e) {
            echo $e ? $e->getMessage() : exit();
        }
    }

    private function validate($params)
    {
        $params = json_decode($params);
        $this->quantity = isset($params->quantity) ? $params->quantity : null;
        $this->description = isset($params->description) ? $params->description : null;
        $this->unit_price = isset($params->unit_price) ? $params->unit_price : null;
        $this->invoice_id = isset($params->invoice_id) ? $params->invoice_id : null;
        $this->inventory_item_id = isset($params->inventory_item_id) ? $params->inventory_item_id : null;
    }
}
