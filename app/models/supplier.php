<?php

namespace App\Models;

class Supplier
{
    private $conn;
    public $name;

    // constructor with $db as database connection
    public function __construct($db, $params)
    {
        $this->conn = $db;
        $this->validate($params);
    }

    function readAll()
    {
        // select all query
        $query = "SELECT * FROM suppliers ORDER BY id ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function read($id)
    {
        try {
            $query = "SELECT * FROM suppliers WHERE id = :id";
            $stmt = $this->conn->prepare($query);
            $id = sanitize($id);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (Exception $e) {
            print_r($e);
            exit();
        }
    }

    function create()
    {
        try {
            $query = "INSERT INTO suppliers (name) VALUES (:name)";
            $stmt = $this->conn->prepare($query);
            $name = sanitize($this->name);
            $stmt->bindParam(':name', $name);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (Exception $e) {
            print_r($e);
            exit();
        }
    }

    function update($id)
    {
        try {
            $query = "UPDATE suppliers SET name = :name WHERE id = :id";
            $stmt = $this->conn->prepare($query);
            $name = sanitize($this->name);
            $id = sanitize($id);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return ($stmt->rowCount() > 0) ? $stmt : false;
        } catch (Exception $e) {
            print_r($e);
            exit();
        }
    }
//
//    function delete($id)
//    {
//        try {
//            $query = "DELETE FROM suppliers WHERE id = :id";
//            $stmt = $this->conn->prepare($query);
//            $id = sanitize($id);
//            $stmt->bindParam(':id', $id);
//            $stmt->execute();
//            return ($stmt->rowCount() > 0) ? $stmt : false;
//        } catch (Exception $e) {
//            return $e ? 'Error deleting invoice' : exit();
//            exit();
//        }
//    }

    private function validate($params)
    {
        $params = json_decode($params);
        $this->name = isset($params->name) ? $params->name : null;
    }
}
