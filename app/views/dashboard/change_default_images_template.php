<script type="x-template" id="dashboard_template">
    <section class="dashboard">
        <table>
            <col style="width:10%">
            <col style="width:50%">
            <col style="width:40%">
            <thead>
            <tr>
                <th>Current Image</th>
                <th>Upload New Image</th>
                <th>Item Description</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in $root.inventory_items_with_default_image" is="table_row"
                :prop_description="item.attributes.description"
                :prop_image_path="item.attributes.image_path"
                :quantity="item.attributes.quantity"
                :category="findCategory(item.attributes.category_id)"
                :id="item.id"></tr>
            </tbody>
        </table>
        <button @click.prevent="submitChanges()">Submit</button>
    </section>
</script>

<script type="x-template" id="dashboard_table_row_template">
    <tr>
        <td><img class="thumbnail--small" :src="data_image_path" alt=""/></td>
        <td>
            <image_upload></image_upload>
        </td>
        <td>{{ data_description }}</td>
    </tr>
</script>
