<script type="x-template" id="image_upload_template">
    <section class="column">
        <fieldset>
            <legend>Upload Image</legend>
            <div class="dropbox">
                <input type="file" name="photo" :disabled="isSaving"
                       @change="filesChange($event.target.name, $event.target.files)"
                       accept="image/*" class="file__input">
                <p>
                    Drag your image here to upload<br> or click to browse
                </p>
                <p v-if="isSaving">
                    Uploading file...
                </p>
            </div>
            <!--SUCCESS-->
            <div v-if="isSuccess">
                <p class="upload--success">Uploaded file successfully.</p>
                <img :src="uploadedFile.url" class="thumbnail">
            </div>
            <!--FAILED-->
            <div v-if="isFailed">
                <h2>Uploaded failed.</h2>
                <p>
                    <a href="javascript:void(0)" @click="reset()">Try again</a>
                </p>
                <pre>{{ uploadError }}</pre>
            </div>
        </fieldset>
    </section>
</script>