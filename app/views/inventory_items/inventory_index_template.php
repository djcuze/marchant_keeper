<script type="x-template" id="inventory_index_template">
    <section id="inventory_items" class="inventory_index">
        <header>
            <h1 class="heading">Inventory</h1>
            <div class="settings__control">
                <div class="results_per_page">
                    <!-- Results per page -->
                    <button name="results_per_page_15" @click="updateResultsPerPageBy(15)" class="active">15</button>
                    <button name="results_per_page_30" @click="updateResultsPerPageBy(30)">30</button>
                    <button name="results_per_page_60" @click="updateResultsPerPageBy(60)">60</button>
                </div>
                <button name="new" @click.prevent="$router.push('/invoices/new')">+</button>
                <button name="settings" @click.prevent="toggleSettings()">
                    <?php include './public/images/icons/settings.svg' ?></button>
            </div>
        </header>
        <table>
            <col style="width:4%"/>
            <col style="width:46%"/>
            <col style="width:20%"/>
            <col style="width:20%"/>
            <col style="width:10%" class="settings__options"/>
            <thead>
            <tr>
                <th>Thumbnail</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Category</th>
                <th class="settings__options" v-if="settings_enabled">
                    Options
                </th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="this.$root.loading">
                <td class="clear">
                    <div id="loading"><?php include './public/images/icons/loading.svg' ?></div>
                </td>
            </tr>
            <tr v-for="item in computedArray" :key="item.id" is="table_row"
                :id='item.id'
                :description="item.attributes.description"
                :quantity="item.attributes.quantity"
                :category="item.attributes.category"
                :image_path="item.attributes.image_path">
            </tr>
            </tbody>
        </table>
        <footer>
            <button v-if="this.page_number != 1"
                    class="prev" @click.prevent="changePage('previous')">Prev
            </button>
            <button class="next" @click.prevent="changePage('next')">Next</button>
        </footer>
    </section>
</script>

<script type="x-template" id="inventory_table_row_template">
    <tr class="table__row" @click="$router.push({name: 'inventory_item', params: { id: id}})">
        <td class="table__cell--with-image">
            <img class="thumbnail" v-bind:src="image_path"/>
        </td>
        <td>{{ description }}</td>
        <td>{{ quantity }}</td>
        <td>{{ category }}</td>
        <td class='settings__options' v-if="this.$parent.settings_enabled" @click.stop>
            <button name="edit"
                    @click="$router.push({ name: 'inventory_item', params: { id: id, editable: true}})"></button>
            <button name="delete" @click="deleteConfirmation()">
                <?php include './public/images/icons/delete.svg' ?>
            </button>
        </td>
        <div id='confirmation' v-if="delete_confirmation" @click.stop>
            <p>{{ description }}</p>
            <button @click="deleteInventoryItem(id)">Yes</button>
            <button @click="delete_confirmation = null">No</button>
        </div>
    </tr>
</script>