<script type="x-template" id="inventory_item_template" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <section id="inventory_item" @click.stop>
        <form method="post" id="edit_inventory_form">
            <div>
                <h1>{{ description }}</h1>
                <fieldset>
                    <legend>Quantity</legend>
                    <input v-if="editable" v-model="quantity"
                           class="form__input" for="quantity">
                    <p v-else>{{ quantity }}</p>
                </fieldset>
                <fieldset>
                    <legend>Category</legend>
                    <fieldset v-if="editable">
                        <span class="custom_select" @click="initializeSelect()">
                    -- Select --
                    <span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span>
                </span>
                        <select class="dropdown" title="category_id" id="category_id" v-model="category">
                            <option v-for="category in $root.categories" v-bind:value="category.id">
                                {{category.name}}
                            </option>
                        </select>
                    </fieldset>
                    <p v-else>{{ category }}</p>
                </fieldset>
                <fieldset>
                    <legend></legend>
                </fieldset>
            </div>
            <div class="image__container">
                <img v-bind:src="image_path" alt="" style="width:100%" v-if="!editable"/>
                <image_upload v-else></image_upload>
            </div>
            <input type="submit" v-if="editable"
                   @click.prevent="updateInventoryItem()" class="button">
        </form>
        <div class="inventory__information" v-if="description === 'Monstera'">
            <p>When growing in the wild, plants of the genus <span>Monstera</span> climb up the trunks and along
                the
                branches of
                trees, clinging to the bark by means of thick aerial roots, which not only anchor the plant to the
                tree but also take up water and nutrients. Only one species, <span>M. deliciosa</span>, is a popular
                house plant;
                others occasionally used indoors are less decorative. The shiny leaves of a mature <span>M.
                        deliciosa</span> grow
                up to 18 inches across, have 12-inch-long stalks, and are basically heart-shaped, but deeply incised
                from the edges almost to the central vein, and perforated in the remaining sections. This breaking
                up of the leaf area helps the wild plants withstand tropical winds - and perhaps explain the origin
                of one of their common names, hurricane plant. Other common names are spitleaf, Swiss cheese, and
                window plant.
                <br>
                Mature monsteras with active aerial roots have the most attractive leaves, with the most pronounced
                incisions and holes. Young plants have leaves that may be entirely unbroken. Immature monsteras are
                sometimes sold as <span>Philodendron pertusum</span> (not a valid name). As they develop, they
                acquire
                the
                characteristic perforations and split edges. The inflorescence which is rarely produced indoors, is
                a half-oval, creamy white spathe with a thick 10-inch-long spadix in the middle. The spadix develops
                into a white, edible fruit that tastes something like pineapple. It can occur at any time of year
                once the plant reaches maturity.
                <br>
                Indoor monsteras make dramatic plants whether they are young specimens with only three or four
                leaves or tall, mature ones that need to be supported on stout canes or poles. If treated well, they
                can grow very big - 10-15 feet tall and 6-8 feet across - and thrive indoors for years. Moss-covered
                poles that simulate the bark of trees the plants climb on in the wild add considerably to the health
                of monsteras grown indoors. There is a form, <span>M. d.</span> 'Variegata,' with leaves splashed
                with
                white or
                cream-colored patches of irregular shapes and sizes.
                <br>
            </p>
            <p>
                <strong>Temperature</strong> Normal room temperatures are suitable. In temperatures above 21°C
                (70°F) place pots on trays of damp pebbles for increased humidity.
                <br>
                <strong>Watering</strong> Water sparingly, making the potting mixture barely moist and allowing the
                top third to dry out before watering again.
                <br>
                <strong>Feeding</strong> Apply standard liquid fertilizer once every two weeks during the active
                growth period only.
                <br>
                <strong>Potting and repotting</strong> Use a soil-based potting mixture (<span>see page 429</span>)
                with
                the addition of a one-third portion of course leaf mold. Move plants into pots one size larger each
                spring until maximum convenient pot size is reached. Thereafter, topdress plants (<span>see page
                        428</span>) with fresh potting mixture annually.
                <br>
                <strong>Propagation</strong> Home propagation is difficult because of the awkwardly large expanse of
                leaf and consequent high loss of moisture through transpiration. A tip cutting, including at least
                two leaves, will root in spring, however, if it is planted in a 4-inch pot containing a moistened
                equal-parts mixture of peat moss and sand. Enclose the cutting in a plastic bag, and keep it warm in
                bright filtered light. When new growth indicates that the cutting has rooted, report it in the
                mixture recommended for adult plants, and treat it as mature.
                <br>
                If it proves impracticable to root cuttings, plants can be air layered (<span>see page 440</span>) -
                a
                reliable
                but slow method. Seed, too, will germinate easily in a warm room, but plants from seed lack
                attractive incised leaves for at least two years.
                <br>
                <strong>Special points</strong> Keep the leaves clean by frequent sponging. Train aerial roots to
                moss-covered poles if these are used; if not, train them into the potting mixture.
            </p>

        </div>
        <footer>
            <button @click="editable = true" v-if="!editable">Edit Inventory Item</button>
        </footer>
    </section>
</script>