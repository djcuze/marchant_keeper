<?php include_once './app/views/form_components/image_upload_template.php'; ?>
<script type="x-template" id="new_inventory_item_template" xmlns="http://www.w3.org/1999/xhtml"
        xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <section id="inventory_item" @click.stop>
        <form method="post" class="form">
            <h1>New Inventory Item</h1>
            <div class="grid">
                <section class="column">
                    <fieldset>
                        <legend>Description</legend>
                        <input class="form__input" type="text" placeholder="Description" title="description"
                               id="description" v-model="description">
                    </fieldset>
                    <fieldset>
                        <legend>Quantity</legend>
                        <input id="quantity" type="text" title="quantity" v-model="quantity">
                    </fieldset>
                    <fieldset>
                        <legend>Category</legend>
                        <span class="custom_select" @click="initializeSelect()">
                    -- Select --
                    <span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span>
                </span>
                        <select class="dropdown" title="category_id" id="category_id" v-model="category">
                            <option v-for="category in $root.categories" v-bind:value="category.id">
                                {{category.attributes.name}}
                            </option>
                        </select>
                    </fieldset>
                    <input type="submit" value="Create Inventory Item" class="button"
                           @click.prevent="submit()">
                </section>
                <image_upload></image_upload>
            </div>
        </form>
    </section>
</script>