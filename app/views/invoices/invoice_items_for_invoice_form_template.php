<script type="x-template" id="invoice_items_for_invoice_form_template" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <form action="" class="add_fields__form">
        <table>
            <thead>
            <tr>
                <th>Description</th>
                <th>Category</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(item, index) in this.invoice_items" class="invoice_item">
                <td>
                    <button @click.prevent="removeField(index, item)"></button>
                    <input type="text" v-model="item.description" placeholder="Description" :id="'description_' + index">
                </td>
                <td>
                  <span class="custom_select" :id="'custom_select_' + index" @click="initializeSelect(item, index)">
                      <p v-if="item.category">{{ item.category }}</p>
                      <p v-else>-- Select --</p>
                    <span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span>
                  </span>
                    <select title="category" v-model="item.category" v-bind:id="'category_select_' + index ">
                        <option v-for="category in $root.categories">
                            {{category.attributes.name}}
                        </option>
                    </select>
                </td>
                <td><input type="text" v-model="item.quantity" placeholder="Quantity" :id="'quantity_' + index"></td>
                <td><input type="text" v-model="item.unit_price" placeholder="Unit Price" :id="'unit_price_' + index"></td>
                <td>${{ item.unit_price * item.quantity }}</td>
            </tr>
            </tbody>
        </table>
        <button class="add_fields__button" @click.prevent="addField()">Add Item</button>
    </form>
</script>