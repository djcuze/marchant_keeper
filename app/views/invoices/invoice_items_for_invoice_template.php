<script type="x-template" id="invoice_items_for_invoice_template">
    <table v-if="this.$parent.editable == undefined">
        <thead>
        <tr>
        <th>Description</th>
        <th>Category</th>
        <th>Quantity</th>
        <th>Unit Price</th>
    <th>Total</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="item in this.$parent.invoice_items">
        <td>{{ item.description }}</td>
    <td>{{ item.category }}</td>
    <td>{{ item.quantity }}</td>
    <td>${{ item.unit_price }}</td>
    <td>${{ (item.unit_price * item.quantity).toFixed(2) }}</td>
    </tr>
    </tbody>
    </table>
</script>