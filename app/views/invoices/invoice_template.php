<?php
include_once './app/views/invoices/invoice_items_for_invoice_template.php';
include_once './app/views/invoices/invoice_items_for_invoice_form_template.php';
?>
<script type='x-template' id="invoice_template">
    <section id="invoice" class="ui">
        <form method="post" id="form">
            <h1 v-if="editable">Editing Invoice</h1>
            <fieldset>
                <legend>Number</legend>
                <input type="text" id="number" v-if="editable" v-model="number" title="number"/>
                <h1 v-else>{{ number }}</h1>
            </fieldset>
            <fieldset>
                <legend>Supplier</legend>
                <span class="custom_select" v-if="editable" @click="initializeSelect()">
                    {{ supplier }}
                    <span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span>
                </span>
                <div class="ui-selectmenu-menu ui-front">
                    <ul aria-hidden="true" aria-labelledby="supplier_id-button" id="supplier_id-menu" role="listbox" tabindex="0"
                        class="ui-menu ui-corner-bottom ui-widget ui-widget-content"></ul>
                </div>
                <select v-model="supplier" id="supplier_id" title="supplier_id" v-if="editable">
                    <option v-for="supplier in $root.suppliers">
                        {{supplier.name}}
                    </option>
                </select>
                <p v-else>{{ supplier }}</p>
            </fieldset>
            <fieldset>
                <legend>Date</legend>
                <input type="text" id="datepicker" v-if="editable" v-model="date" title="date"
                       @click="initializeDatepicker()"/>
                <p v-else>{{ date }}</p>
            </fieldset>
            <input type="submit" v-if="editable" value="Save Invoice" @click.prevent="updateInvoice()"/>
            <p><span>Total: </span>${{ calculateTotal() }}</p>
        </form>
        <invoice_items_for_invoice></invoice_items_for_invoice>
        <invoice_items_for_invoice_form v-if="this.editable"></invoice_items_for_invoice_form>
        <footer>
            <button @click="editable = true" v-if="!editable && $root.auth">Edit Invoice</button>
        </footer>
    </section>
</script>
