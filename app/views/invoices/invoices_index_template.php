<script type='x-template' id='invoices_index_template'>
    <section class='invoices_index'>
        <header>
            <h1 class="heading">Invoices</h1>
            <div class="settings__control">
                <div class="results_per_page">
                    <!-- Results per page -->
                    <button name="results_per_page_15" @click="updateResultsPerPageBy(15)" class="active">15</button>
                    <button name="results_per_page_30" @click="updateResultsPerPageBy(30)">30</button>
                    <button name="results_per_page_60" @click="updateResultsPerPageBy(60)">60</button>
                </div>
                <button name="new" @click.prevent="$router.push('/invoices/new')">+</button>
                <button name="settings" class="settings" id="settings" @click.prevent="toggleSettings()">
                    <?php include './public/images/icons/settings.svg' ?></button>
            </div>
        </header>
        <table>
            <colgroup>
                <col/>
                <col/>
                <col/>
                <col/>
                <col/>
            </colgroup>
            <thead>
            <tr>
                <th>Invoice #</th>
                <th>Supplier</th>
                <th>Category</th>
                <th>Date</th>
                <th>Total</th>
                <th class='settings__options' v-if="settings_enabled">Options</th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="this.$root.loading">
                <td class="clear">
                    <div id="loading"><?php include './public/images/icons/loading.svg' ?></div>
                </td>
            </tr>
            <tr v-for='invoice in computedArray' :key='invoice.id' is='table_row'
                :id='invoice.id'
                :number='invoice.attributes.number'
                :supplier='invoice.attributes.supplier'
                :category='invoice.attributes.category'
                :date='invoice.attributes.date'
                :total='invoice.attributes.total'>
            </tr>
            </tbody>
        </table>
        <footer>
            <button v-if="this.page_number != 1"
                    class="prev" @click.prevent="changePage('previous')">Prev
            </button>
            <button class="next" @click.prevent="changePage('next')">Next</button>
        </footer>
    </section>
</script>

<script type='x-template' id='table_row_template'>
    <tr class='table__row' @click="$router.push({ name: 'invoice', params: { id: id}})">
        <td class="table__cell">{{ number }}</td>
        <td class="table__cell">{{ supplier }}</td>
        <td class="table__cell">{{ category }}</td>
        <td class='table__cell text--bold'>{{date}}</td>
        <td class="table__cell">${{ total }}</td>
        <td class='settings__options' v-if="this.$parent.settings_enabled" @click.stop>
            <button name="edit"
                    @click="$router.push({ name: 'inventory_item', params: { id: id, editable: true}})"></button>
            <button name="delete" @click="deleteConfirmation()">
                <?php include './public/images/icons/delete.svg' ?>
            </button>
        </td>
        <div id='confirmation' v-if="delete_confirmation" @click.stop>
            <p>Are you sure you want to delete Invoice: {{ number }}</p>
            <button name="yes" @click="deleteInvoice(id)">Yes</button>
            <button name="no" @click="delete_confirmation = null">No</button>
        </div>
    </tr>
</script>