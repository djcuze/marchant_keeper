<script type="x-template" id="new_invoice_template">
    <section id="invoice" class="ui">
        <form method="post" id="invoice_form">
            <h1>New Invoice</h1>
            <fieldset>
                <legend>Number</legend>
                <input type="text" id="number" v-model="number" title="number"/>
            </fieldset>
            <fieldset>
                <legend>Supplier</legend>
                <span class="custom_select" @click="initializeSelect()">
                    -- Select --
                    <span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span>
                </span>
                <select v-model="supplier" id="supplier_id" title="supplier_id">
                    <option v-for="supplier in $root.suppliers">
                        {{supplier.name}}
                    </option>
                </select>
            </fieldset>
            <fieldset>
                <legend>Date</legend>
                <input type="text" id="datepicker" v-model="date" title="date" @click="initializeDatepicker()"/>
            </fieldset>
            <input type="submit" value="Save Invoice" @click.prevent="submit()"/>
            <p><span>Total: </span>${{ calculateTotal() }}</p>
        </form>
        <invoice_items_for_invoice_form></invoice_items_for_invoice_form>
    </section>
</script>