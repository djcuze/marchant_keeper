<script type="x-template" id='cards'>
    <div class='card__container'>
        <div class='card' v-for='card in cards' v-if="$root.auth" @click="$router.push('/' + card.title)">
            <img class="card__image"
                 :src="'./public/images/icons/' + card.title + '.png'"
                 :alt="'Use this to navigate to ' + card.title">
            <div class='card__link__container'>
                <router-link :to="'/' + card.title" class='card__link'>{{card.title}}</router-link>
                <router-link :to="'/' + card.title + '/new'" class='card__button'>+</router-link>
            </div>
        </div>
        <div class='card' v-else>
            <img class="card__image" src="./public/images/icons/inventory.png"
                 alt="'Use this to navigate to inventory">
            <div class='card__link__container'>
                <router-link to="/inventory" class='card__link'>Inventory</router-link>
            </div>
        </div>
    </div>
</script>