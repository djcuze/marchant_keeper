<?php require('cards.php') ?>

<script type="x-template" id="home">
    <section id="home">
        <img class='logo' href="Marchant & Co. Botanicals" src='./public/images/logo.png'/>
        <a class="social" href="https://www.instagram.com/marchant_and_co._/" target="_blank">
            <img class="icon center" src="./public/images/icons/instagram.png"/>
            @marchant_and_co._
        </a>
        <cards v-if="$root.auth"></cards>
        <a class="text" href="https://www.linkedin.com/in/natejacoby/">&copy; Nathan Jacoby 2018</a>
    </section>
</script>