<script type="x-template" id="login_template">
    <section class="login">
        <form method="post" class="login form__header" id="login__form">
            <h1 class="heading">Login</h1>
            <br>
            <fieldset>
                <legend>Username:</legend>
                <input type="text" name='username' v-model="username" class="form__input" required>
            </fieldset>
            <fieldset>
                <legend>Password:</legend>
                <input type="password" v-model='password' name="password" class="form__input" required>
            </fieldset>
            <input type="submit" class="button" name="submit" @click.prevent="submit()"/>
        </form>
    </section>
</script>
