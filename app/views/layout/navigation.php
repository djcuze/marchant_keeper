<script type="x-template" id="navigation">
    <ul class='navigation'>
        <li class='navigation__item home'>
            <router-link to="/" id='home_button' class='navigation__link' name="home"></router-link>
        </li>
        <li class="navigation__item" v-for="page in pages" v-if="$root.auth">
            <router-link :to="'/' + page.title" class="navigation__link" :name="page.title">{{ page.title }}
            </router-link>
            <router-link @click.prevent="$router.push('/'+ page.title +'/new')" :to="'/' + page.title + '/new'"
                         class="button--circular">+
            </router-link>
        </li>
        <li v-if="this.$root.inventory_items_with_default_image.length > 0 && $root.auth" class="dashboard">
            <router-link to="/dashboard"
                         :class="'dashboard__notice items_to_change_' + $root.inventory_items_with_default_image.length">
                There are <span>{{ $root.inventory_items_with_default_image.length }}</span> items that need
                attention
            </router-link>
        <li>
        <li class="navigation__item status authenticated" v-if="$root.auth">
            <a name='logout' class=" navigation__link" href="./app/lib/logout.php" id="button">Logout</a>
        </li>
        <li class="navigation__item status not_authenticated" v-else>
            <router-link name="login" to="login" class="navigation__link">Login/Register</router-link>
        </li>
    </ul>
</script>