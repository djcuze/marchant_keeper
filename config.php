<?php
session_start();
include_once './app/views/layout/navigation.php';
include_once './app/lib/function.php';
include_once './app/config/database.php';
// Requirements
include_once './app/views/layout/home.php';
include_once './app/views/layout/login_template.php';
include_once './app/views/inventory_items/inventory_index_template.php';
include_once './app/views/inventory_items/new.php';
include_once './app/views/inventory_items/inventory_item_template.php';
include_once './app/views/invoices/invoices_index_template.php';
include_once './app/views/invoices/invoice_template.php';
include_once './app/views/invoices/new_invoice_template.php';
include_once './app/views/dashboard/change_default_images_template.php';

$database = new Database();
$db = $database->getConnection();