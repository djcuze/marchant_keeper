<!DOCTYPE html>
<?php require_once './config.php'; ?>
<html>
<head>
    <base href="http://localhost:63342/"/>
    <?php include_once './app/config/analytics.php' ?>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Marchant & Co Botanics</title>
    <?php include_once './public/bundle_dev.php'; ?>
    <link rel="stylesheet" href="./public/style.css"/>
    <script src="./public/bundle.js" defer></script>
</head>
<body>
<div id="app">
    <?php func::checkLoginState($db); ?>
    <h1 id="notice" :class="notice.state" v-if="notice.message">{{ message }}</h1>
    <button class="back__button" @click.prevent="$router.go(-1)">Back</button>
    <navigation></navigation>
    <router-view></router-view>
</div>
</body>
</html>
