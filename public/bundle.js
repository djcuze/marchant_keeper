/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/assets/components/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/assets/components/app.js":
/*!**************************************!*\
  !*** ./app/assets/components/app.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let navigation = __webpack_require__(/*! ./layout/navigation.js */ "./app/assets/components/layout/navigation.js");
let home = __webpack_require__(/*! ./layout/home.js */ "./app/assets/components/layout/home.js");
let router = __webpack_require__(/*! ./router.js */ "./app/assets/components/router.js");
let login = __webpack_require__(/*! ./layout/login.js */ "./app/assets/components/layout/login.js");

const app = new Vue({
    data() {
        return {
            categories: [],
            suppliers: [],
            notice: {
                message: null,
                state: null
            },
            auth: null,
            loading: false,
            pages: [
                {title: 'invoices'},
                {title: 'inventory'},
            ],
            inventory_items_with_default_image: []
        };
    },
    mounted() {
        this.getCategories();
        this.getSuppliers();
        this.browserDetection();
        this.checkForLogin();
    },
    computed: {
        message() {
            if (this.notice.message !== null) {
                this.resetNotice();
                return this.notice.message;
            }
        }
    },
    components: {
        login,
        home,
        navigation,

    },
    methods: {
        resetNotice() {
            let self = this;
            setTimeout(function () {
                $('#notice').fadeOut('slow', function () {
                    self.notice.message = null;
                    self.notice.state = null;
                })
            }, 2000);
        },
        checkForLogin() {
            if (document.cookie && this.getCookie('user_id') && this.getCookie('serial') && this.getCookie('token')) {
                this.auth = true;
                return true;
            } else {
                this.$router.push('/login');
                return false;
            }
        },
        browserDetection() {
            // Returns true if mobile browser
            let check = false;
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        },
        findCategory(category) {
            let all_categories = this.$root.categories;
            for (let i = 0; i < all_categories.length; i++) {
                if (all_categories[i].id === category) {
                    return all_categories[i].name;
                } else if (all_categories[i].name === category) {
                    return all_categories[i].id;
                }
            }
        },
        getCategories(url) {
            axios.get('/app/controllers/categories.php').then((response) => {
                this.categories = response.data;
            }).catch(error => {
                console.log(error);
            });
        },
        getSuppliers(url) {
            axios.get('/app/controllers/suppliers.php').then((response) => {
                this.suppliers = response.data;
            }).catch(error => {
                console.log(error);
            });
        },
        getCookie(cname) {
            let name = cname + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    },
    router,
}).$mount('#app');



/***/ }),

/***/ "./app/assets/components/dashboard/default_images_to_change.js":
/*!*********************************************************************!*\
  !*** ./app/assets/components/dashboard/default_images_to_change.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let image_upload = __webpack_require__(/*! ../form_components/image_upload */ "./app/assets/components/form_components/image_upload.js");
const default_images_to_change = {
    name: 'default_images_to_change',
    template: '#dashboard_template',
    components: {
        table_row: {
            name: 'inventory_item',
            template: '#dashboard_table_row_template',
            props: ['id', 'prop_image_path', 'prop_description', 'quantity', 'category'],
            components: {
                image_upload
            },
            data() {
                return {
                    data_description: null,
                    data_image_path: null
                }
            },
            methods: {
                sendChanges() {
                    let item_to_pass = {
                        id: this.id,
                        description: this.data_description,
                        image_path: this.data_image_path,
                        quantity: this.quantity,
                        category: this.category
                    };
                    this.$parent.items_to_change.push(item_to_pass);
                }
            },
            computed: {
                image_path: {
                    get: function () {
                        return this.prop_image_path;
                    },
                    set: function (new_value) {
                        this.data_image_path = new_value;
                        this.sendChanges();
                    }
                },
                description: {
                    get: function () {
                        return this.prop_description;
                    },
                    set: function (new_value) {
                        this.data_description = new_value;
                        this.sendChanges();
                    }
                }
            },
            mounted() {
                this.data_image_path = this.image_path;
                this.data_description = this.description;
            },
        }
    },
    data() {
        return {
            items_to_change: []
        }
    },
    methods: {
        findCategory(category) {
            let all_categories = this.$root.categories;

            for (let i = 0; i < all_categories.length; i++) {
                if (all_categories[i].id === category) {
                    return all_categories[i].name;
                } else if (all_categories[i].name === category) {
                    return all_categories[i].id;
                }
            }
        },
        submitChanges() {
            for (let i = 0; i < this.items_to_change.length; i++) {
                axios.patch('/app/controllers/inventory.php', {
                    id: this.items_to_change[i].id,
                    description: this.items_to_change[i].description,
                    quantity: this.items_to_change[i].quantity,
                    image_path: this.items_to_change[i].image_path,
                    category_id: this.findCategory(this.items_to_change[i].category)
                })
            }
        }
    }
};
module.exports = default_images_to_change;

/***/ }),

/***/ "./app/assets/components/form_components/image_upload.js":
/*!***************************************************************!*\
  !*** ./app/assets/components/form_components/image_upload.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const STATUS_INITIAL = 0, STATUS_SAVING = 1, STATUS_SUCCESS = 2, STATUS_FAILED = 3;

function upload(formData) {
    for (let [key, value] of formData.entries()) {
        return axios.post('/app/controllers/form_components/image_upload.php',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        );
    }
}

const image_upload = {
    name: 'image_upload',
    template: '#image_upload_template',
    data() {
        return {
            uploadedFile: [],
            uploadError: null,
            currentStatus: null
        };
    },
    computed: {
        isInitial() {
            return this.currentStatus === STATUS_INITIAL;
        },
        isSaving() {
            return this.currentStatus === STATUS_SAVING;
        },
        isSuccess() {
            return this.currentStatus === STATUS_SUCCESS;
        },
        isFailed() {
            return this.currentStatus === STATUS_FAILED;
        }
    },
    methods: {
        reset() {
            // reset form to initial state
            this.uploadedFile = [];
            this.currentStatus = STATUS_INITIAL;
            this.uploadError = null;
        },
        save(formData) {
            // upload data to the server
            this.currentStatus = STATUS_SAVING;

            upload(formData)
                .then(response => {
                    this.uploadedFile = response.data;
                    this.uploadedFile.url = this.$parent.image_path = '/public/images/uploads/' + response.data.name;
                    this.currentStatus = STATUS_SUCCESS;
                })
                .catch(err => {
                    this.uploadError = err.response;
                    this.currentStatus = STATUS_FAILED;
                });
        },
        filesChange(fieldName, fileList) {
            // handle file changes
            const formData = new FormData();

            if (!fileList.length) return;

            // append the file to FormData
            formData.append(fieldName, fileList[0], fileList[0].name);
            // save it
            this.save(formData);
        }
    },
    mounted() {
        this.reset();
    },
};
module.exports = image_upload;

/***/ }),

/***/ "./app/assets/components/inventory/inventory_index.js":
/*!************************************************************!*\
  !*** ./app/assets/components/inventory/inventory_index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const inventory_index = {
    name: 'inventory_index',
    template: '#inventory_index_template',
    data() {
        return {
            inventory: [
                // An array of objects collected on creation with fetchData()
            ],
            results_per_page: 15,
            page_number: 1,
            settings_enabled: null
        };
    },
    mounted() {
        this.fetchData();
    },
    components: {
        table_row: {
            template: '#inventory_table_row_template',
            name: 'inventory_item',
            props: [
                'id',
                'description',
                'quantity',
                'category',
                'image_path'
            ],
            data() {
                return {
                    delete_confirmation: null
                };
            },
            methods: {
                deleteInventoryItem(id) {  // <=== There is a comment referring to this in _variables.scss
                    let inventory = this.$parent.inventory;
                    for (let i = 0; i < inventory.length; i++) {
                        if (inventory[i].id === this.id) {
                            this.$parent.inventory.splice(i, 1);
                            try {
                                axios.delete('/app/controllers/inventory.php', {
                                    data: {
                                        id: id
                                    }
                                }).then(
                                    this.delete_confirmation = null,
                                    this.$root.notice.message = 'Inventory Item deleted successfully',
                                    this.$root.notice.state = 'success'
                                );
                            } catch (error) {
                                console.log(error);
                                this.$root.notice.message = 'There was an error deleting the inventory item';
                                this.$root.notice.state = 'error';
                            }
                        }
                    }
                },
                deleteConfirmation() {
                    this.delete_confirmation = true;
                }
            }
        }
    },
    methods: {
        changePage(direction) {
            direction = direction.toLowerCase();
            if (direction === 'previous' && this.page_number > 1) {
                this.page_number -= 1;
            } else if (direction === 'next' && this.computedArray.length >= this.results_per_page) {
                this.page_number += 1;
            }
        },
        fetchData() {
            let self = this;
            self.$root.loading = true;
            axios.get('/app/controllers/inventory.php').then((response) => {
                this.inventory = response.data;
                self.$root.loading = false;
            }).catch(error => {
                console.log(error);
            });
        },
        updateResultsPerPageBy(count) {
            let buttons = $('.settings__control > div > button');
            buttons.removeClass('active');
            let result = $(".settings__control > div > button:contains(" + count + ")");
            result.toggleClass('active');
            this.results_per_page = count;
        },
        // Toggles visibility of "Options" div that contains buttons to an Edit form and a Destroy action accordingly
        toggleSettings() {
            let e = this.settings_enabled;
            this.settings_enabled = (function () {
                if (e === true) {
                    return null;
                } else {
                    return true;
                }
            })();
        }
    },
    computed: {
        computedArray() {
            let first = (this.results_per_page * (this.page_number - 1));
            let last = this.results_per_page * this.page_number;
            return this.inventory.slice(first, last);
        }
    },
    beforeRouteUpdate(to, from, next) {
        next();
    }
};
module.exports = inventory_index;

/***/ }),

/***/ "./app/assets/components/inventory/inventory_item.js":
/*!***********************************************************!*\
  !*** ./app/assets/components/inventory/inventory_item.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let image_upload = __webpack_require__(/*! ../form_components/image_upload */ "./app/assets/components/form_components/image_upload.js");
let single_inventory_item = {
    name: 'inventory_item',
    template: '#inventory_item_template',
    data() {
        return {
            description: null,
            quantity: null,
            image_path: null,
            category_id: null,
            editable: this.$route.params.editable
        };
    },
    components: {
        image_upload
    },
    mounted() {
        this.fetchData(this.$route.params.id);
    },
    computed: {
        categories() {
            return this.$root.categories
        },
        category() {
            return this.findCategory(this.category_id);
        }
    },
    methods: {
        fetchData(id) {
            let self = this;
            self.$root.loading = true;
            axios.get("/app/controllers/inventory.php?id=" + id)
                .then(response => {
                    this.description = response.data.attributes.description;
                    this.quantity = response.data.attributes.quantity;
                    this.image_path = response.data.attributes.image_path;
                    this.category_id = response.data.attributes.category_id;
                    self.$root.loading = false;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#category_id').selectmenu({
                select: function (event, ui) {
                    self.category = ui.item.value;
                }
            });
            $('#category_id').selectmenu('open');

        },
        findCategory(category) {
            for (let i = 0; i < this.categories.length; i++) {
                if (this.categories[i].id === category) {
                    return this.categories[i].name;
                } else if (this.categories[i].name === category) {
                    return this.categories[i].id;
                }
            }
        },
        updateInventoryItem() {
            axios.patch("/app/controllers/inventory.php", {
                id: this.$route.params.id,
                description: this.description,
                category_id: this.category_id,
                image_path: this.image_path,
                quantity: this.quantity,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((response) => {
                this.editable = false;
                this.$root.notice.message = 'Inventory Item updated successfully';
                this.$root.notice.state = 'success';
            })
        }
    },
    beforeRouteUpdate(to, from, next) {
        this.$router.push(to);
        next();
    }
};
module.exports = single_inventory_item;

/***/ }),

/***/ "./app/assets/components/inventory/new_inventory_item.js":
/*!***************************************************************!*\
  !*** ./app/assets/components/inventory/new_inventory_item.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let image_upload = __webpack_require__(/*! ../form_components/image_upload */ "./app/assets/components/form_components/image_upload.js");
const inventory_item_new = {
    name: 'new_inventory_item',
    data() {
        return {
            description: null,
            quantity: null,
            category: null,
            image_path: null
        };
    },
    components: {
        image_upload
    },
    methods: {
        validate() {
            if (!this.description || !this.category || !this.quantity) {
                return 'Error: Please fill out all fields';
            } else {
                return true;
            }
        },
        submit() {
            if (this.validate() === true) {
                try {
                    axios.post('/app/controllers/inventory.php', {
                        description: this.description,
                        quantity: this.quantity,
                        category_id: this.category,
                        image_path: this.image_path,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then((response) => {
                        this.$router.push('/inventory/' + response.data.id);
                        this.$root.notice.message = 'Inventory Item created successfully';
                        this.$root.notice.state = 'success';
                    });
                } catch (error) {
                    console.log(error);
                }
            } else {
                this.$root.notice.message = this.validate();
                this.$root.notice.state = 'error';
            }
        },
        getData() {
            $('#fileToUpload').addClass('half__visible');
            let file_name = document.getElementById('fileToUpload').value;
            $('#file_name').append(file_name);
        },
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#category_id').selectmenu({
                select: function (event, ui) {
                    self.category = ui.item.value;
                }
            });
            $('#category_id').selectmenu('open');
        }
    },
    template: '#new_inventory_item_template'
};
module.exports = inventory_item_new;

/***/ }),

/***/ "./app/assets/components/invoices/invoice.js":
/*!***************************************************!*\
  !*** ./app/assets/components/invoices/invoice.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let invoice_items_for_invoice = __webpack_require__(/*! ./invoice_items_for_invoice */ "./app/assets/components/invoices/invoice_items_for_invoice.js");
let invoice_items_for_invoice_form = __webpack_require__(/*! ./invoice_items_for_invoice_form */ "./app/assets/components/invoices/invoice_items_for_invoice_form.js");
let single_invoice = {
    name: 'Invoice',
    template: '#invoice_template',
    data() {
        return {
            number: null,
            date: null,
            supplier: null,
            invoice_items: [{}],
            total: null,
            editable: this.$route.params.editable,
            invoice_items_to_delete: []
        };
    },
    components: {
        // Components that render a template
        invoice_items_for_invoice,
        invoice_items_for_invoice_form
    },
    mounted() {
        this.fetchData(this.$route.params.id);
        this.fetchInvoiceItems(this.$route.params.id);
        this.listenForEscape();
    },
    methods: {
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#supplier_id').selectmenu({
                select: function (event, ui) {
                    self.supplier = ui.item.value;
                }
            });
            $('#supplier_id').selectmenu('open');

        },
        fetchData(id) {
            let self = this;
            self.$root.loading = true;
            axios.get("/app/controllers/invoices.php?id=" + id)
                .then(response => {
                    this.number = response.data.attributes.number;
                    this.date = response.data.attributes.date;
                    this.supplier = response.data.attributes.supplier;
                    this.total = response.data.attributes.total;
                    self.$root.loading = false;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        fetchInvoiceItems(id) {
            axios.get('/app/controllers/invoice_items.php?id=' + id)
                .then(response => {
                    this.invoice_items = response.data;
                });
        },
        validateForm() {
            // validateForm();
            let number = $("#number").val();
            let supplier = $('#supplier');
            let date = $('#date');

            // Ensures input
            if (number.length <= 0) {
                let validation = false;
                let message = 'Please enter an invoice number';
                let problematic_input = 'number';
                return {validation, message, problematic_input};
            }
            else {
                let validation = true;
                let problematic_input, message = null;
                return {validation, message, problematic_input};
            }
        },
        updateInvoice() {
            let items_to_delete = this.invoice_items_to_delete;
            // If there are items that require removing from the Invoice:
            if (items_to_delete && items_to_delete.length !== 0) {
                for (let i = 0; i < items_to_delete.length; i++) {
                    axios.patch('/app/controllers/invoice_items.php', {
                        quantity: items_to_delete[i].quantity,
                        description: items_to_delete[i].description,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                }
            } else {
                let form = this.validateForm();
                if (form.validation === true) {
                    // Creates the invoice
                    axios.patch('', "/app/controllers/invoices.php", {
                        id: this.$route.params.id,
                        number: this.number,
                        date: this.date,
                        supplier: this.findSupplier(),
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(() => {
                        try { // Updating the invoice Items

                            // First, checks whether the invoice_item in question is a new entry, or an update
                            let invoice_items = this.invoice_items;

                            for (let n = 0; n < invoice_items.length; n++) {
                                let self = invoice_items[n];
                                if (Object.keys(self).length === 0 && self.constructor === Object) {
                                    break;
                                } else {
                                    axios.patch('../app/controllers/invoice_items.php', {
                                        id: invoice_items[n].id,
                                        description: invoice_items[n].description,
                                        category: this.findCategory(invoice_items[n].category),
                                        quantity: invoice_items[n].quantity,
                                        unit_price: invoice_items[n].unit_price,
                                        invoice_id: this.$route.params.id
                                    });
                                }
                            }
                        } catch (error) {
                            console.log(error);
                        }
                    }).then(
                        this.editable = null,
                        this.$root.notice.message = 'Invoice Updated Successfully',
                        this.$root.notice.state = 'success'
                    ).catch(error => {
                        console.log(error);
                        this.$root.notice.message = 'There was a problem updating the invoice';
                        this.$root.notice.state = 'error';
                    });
                } else {
                    $('#' + form.problematic_input).after('<p class="error">' + form.message + '</p>');
                }
            }
            this.invoice_items_to_delete = [];
        },
        calculateTotal() {
            if (this.editable && this.invoice_items) {
                let total = 0;
                for (let n = 0; n < this.invoice_items.length; n++) {
                    total += (this.invoice_items[n].quantity * this.invoice_items[n].unit_price);
                }
                return total.toFixed(2);
            } else {
                return this.total;
            }
        },
        initializeDatepicker() {
            let d = $("#datepicker");
            let self = this;
            d.datepicker({
                onSelect: function (selectedDate, datePicker) {
                    self.date = selectedDate;
                },
                dateFormat: 'yy-mm-dd'
            });
            // d.datepicker();
            d.datepicker("show");
        },
        findCategory(category) {
            let all_categories = this.$root.categories;

            for (let i = 0; i < all_categories.length; i++) {
                if (all_categories[i].id === category || all_categories[i].name === category) {
                    return all_categories[i].id;
                }
            }
        },
        findSupplier() {
            let supplier = this.supplier;
            let all_suppliers = this.$parent.suppliers;

            for (let i = 0; i < all_suppliers.length; i++) {
                if (all_suppliers[i].id === supplier || all_suppliers[i].name === supplier) {
                    return all_suppliers[i].id;
                }
            }
        },
        listenForEscape() {
            let vm = this;
            window.addEventListener('keyup', function (event) {
                // If ESC arrow was pressed...
                if (vm.$route.name === 'invoice' && event.keyCode === 27) {
                    vm.$router.go(-1);
                }
            });
        }
    },
    beforeRouteUpdate(to, from, next) {
        this.$router.push(to);
        next();
    }
};
module.exports = single_invoice;

/***/ }),

/***/ "./app/assets/components/invoices/invoice_items_for_invoice.js":
/*!*********************************************************************!*\
  !*** ./app/assets/components/invoices/invoice_items_for_invoice.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const invoice_items_for_invoice = {
    template: '#invoice_items_for_invoice_template'
};
module.exports = invoice_items_for_invoice;

/***/ }),

/***/ "./app/assets/components/invoices/invoice_items_for_invoice_form.js":
/*!**************************************************************************!*\
  !*** ./app/assets/components/invoices/invoice_items_for_invoice_form.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const invoice_items_for_invoice_form = {
    template: '#invoice_items_for_invoice_form_template',
    computed: {
        invoice_items() {
            return this.$parent.invoice_items;
        },
        invoice_items_to_delete() {
            return this.$parent.invoice_items_to_delete;
        }
    },
    methods: {
        initializeSelect(item, index) {
            let select = $('#category_select_' + index);
            $('#custom_select_' + index).hide();
            let self = item;
            select.selectmenu({
                select: function (event, ui) {
                    self.category = ui.item.value;
                }
            });
            select.selectmenu('open');
        },
        addField() {
            // The below code ensures that the last entry is not blank before rendering another form
            let array = this.invoice_items;
            let last_object = array[array.length - 1];

            if (array.length > 0 && this.emptyFieldCheck(last_object)) {
                this.$root.notice.message = "Please fill out the previous addition before adding a new field";
                this.$root.notice.state = 'error';
            }
            else {
                let table = $('#invoice').find("tbody");
                this.$parent.invoice_items.push({
                    description: null,
                    category: null,
                    quantity: null,
                    unit_price: null,
                });
            }
        },
        removeField(index, item) {
            if (this.emptyFieldCheck(item)) {
                this.$parent.invoice_items.splice(index, 1);
            } else {
                this.$parent.invoice_items.splice(index, 1);
                this.$parent.invoice_items_to_delete.push(item);
                this.undoRemoveField();
            }
        },
        undoRemoveField() {
            // A messy AF function that just resets atm (Future plan is to save a history and individually undo removals)
            if ($('#reset').length === 0) {
                let reset = '<button id="reset">Reset</button>';
                $('.add_fields__button').after(reset);
            }
            $('#reset').click((event) => {
                event.preventDefault();
                let index = this.$parent.invoice_items_to_delete.length - 1;
                let object = this.$parent.invoice_items_to_delete[index];
                // console.log(index);
                // console.log(object);
                this.$parent.invoice_items.push(object);
                this.$parent.invoice_items_to_delete.splice(index, 1);

                // Hide the button if there's no items to delete left
                if (this.$parent.invoice_items_to_delete.length === 0) {
                    $('#reset').remove();
                }
            });
        },
        emptyFieldCheck(obj) {
            let values = Object.values(obj);
            for (let i = 0; i < values.length; i++) {
                if (values[i] != null && obj.constructor === Object) {
                    return false;
                }
            }
            return true;
        }
    }
};
module.exports = invoice_items_for_invoice_form;

/***/ }),

/***/ "./app/assets/components/invoices/invoices_index.js":
/*!**********************************************************!*\
  !*** ./app/assets/components/invoices/invoices_index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const invoices_index = {
    name: 'Invoices_index',
    template: '#invoices_index_template',
    data() {
        return {
            invoices: [
                // An array of objects collected on creation with fetchData()
            ],
            results_per_page: 15,
            page_number: 1,
            settings_enabled: null
        };
    },
    mounted() {
        this.$root.checkForLogin();
        this.fetchData();
    },
    components: {
        table_row: {
            template: '#table_row_template',
            name: 'Invoice',
            props: [
                'id',
                'number',
                'supplier',
                'category',
                'date',
                'total'
            ],
            data() {
                return {
                    delete_confirmation: null
                };
            },
            computed: {

            },
            methods: {
                deleteInvoice(id) {  // <=== There is a comment referring to this in _variables.scss
                    let invoices = this.$parent.invoices;
                    for (let i = 0; i < invoices.length; i++) {
                        if (invoices[i].id === this.id) {
                            this.$parent.invoices.splice(i, 1);
                            try {
                                axios.delete('/app/controllers/invoices.php', {
                                    data: {
                                        id: id
                                    }
                                }).then(() => {
                                    this.delete_confirmation = null;
                                    this.$root.notice.message = 'Invoice deleted successfully';
                                    this.$root.notice.state = 'success';
                                });
                            } catch (error) {
                                console.log(error);
                                this.$root.notice.message = 'There was an error deleting the invoice';
                                this.$root.notice.state = 'error';
                            }
                        }
                    }
                },
                deleteConfirmation() {
                    this.delete_confirmation = true;
                }
            }
        }
    },
    methods: {
        // changePage( :string = 'previous' || 'next')
        changePage(direction) {
            direction = direction.toLowerCase();
            if (direction === 'previous' && this.page_number > 1) {
                this.page_number -= 1;
            } else if (direction === 'next' && this.computedArray.length >= this.results_per_page) {
                this.page_number += 1;
            }
        },
        // Sends a GET request for all invoices and adds them to the component's invoices array
        //
        // IMPORTANT!!
        //
        // Delayed to visualize loading icon
        fetchData() {
            let self = this;
            self.$root.loading = true;
            axios.get("/app/controllers/invoices.php").then(response => {
                self.invoices = response.data;
                self.$root.loading = false;
            }).catch(error => {
                console.log(error);
            });
        },
        // Toggles visibility of "Options" div that contains buttons to an Edit form and a Destroy action accordingly
        toggleSettings() {
            let e = this.settings_enabled;
            this.settings_enabled = (function () {
                if (e === true) {
                    return null;
                } else {
                    return true;
                }
            })();
        },
        updateResultsPerPageBy(count) {
            let buttons = $('.settings__control > div > button');
            buttons.removeClass('active');
            let result = $(".settings__control > div > button:contains(" + count + ")");
            result.toggleClass('active');
            this.results_per_page = count;
        },
    },
    computed: {
        computedArray() {
            let first = (this.results_per_page * (this.page_number - 1));
            let last = this.results_per_page * this.page_number;
            return this.invoices.slice(first, last);
        }
    },
    beforeRouteUpdate(to, from, next) {
        next();
    }
};
module.exports = invoices_index;

/***/ }),

/***/ "./app/assets/components/invoices/new_invoice.js":
/*!*******************************************************!*\
  !*** ./app/assets/components/invoices/new_invoice.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let invoice_items_for_invoice_form = __webpack_require__(/*! ./invoice_items_for_invoice_form */ "./app/assets/components/invoices/invoice_items_for_invoice_form.js");
const new_invoice = {
    template: '#new_invoice_template',
    name: 'new_invoice',
    data() {
        return {
            number: null,
            date: null,
            supplier: null,
            invoice_items: [{}],
            total: null,
            editable: this.$route.params.editable
        };
    },
    methods: {
        calculateTotal() {
            if (this.editable) {
                let total = 0;
                for (let n = 0; n < this.invoice_items.length; n++) {
                    total += (this.invoice_items[n].quantity * this.invoice_items[n].unit_price);
                }
                return total;
            } else {
                return this.total;
            }
        },
        initializeDatepicker() {
            let d = $("#datepicker");
            let self = this;
            d.datepicker({
                onSelect: function (selectedDate, datePicker) {
                    self.date = selectedDate;
                },
                dateFormat: 'yy-mm-dd'
            });
            d.datepicker("show");
        },
        findSupplier() {
            let supplier = this.supplier;
            let all_suppliers = this.$root.suppliers;

            for (let i = 0; i < all_suppliers.length; i++) {
                if (all_suppliers[i].id === supplier || all_suppliers[i].name === supplier) {
                    return all_suppliers[i].id;
                }
            }
        },
        findCategory(category) {
            console.log(category);
            if (category === 'Botanical') {
                return 1;
            } else if (category === 'Receptacle') {
                return 2;
            } else if (category === 'Chemical') {
                return 3;
            } else if (category === 'Hardware') {
                return 4;
            } else {
                let all_categories = this.$root.categories;
                for (let i = 0; i < all_categories.length; i++) {
                    if (all_categories[i].id === category || all_categories[i].attributes.name === category) {
                        return all_categories[i].id;
                    }
                }
            }
        },
        initializeSelect() {
            $('fieldset > .custom_select').hide();
            let self = this;
            $('#supplier_id').selectmenu({
                select: function (event, ui) {
                    self.supplier = ui.item.value;
                }
            });
            $('#supplier_id').selectmenu('open');

        },
        emptyFieldCheck() {
            let self = this;
            for (let i = 0; i < this.invoice_items.length; i++) {
                let obj = self.invoice_items[i];
                if (Object.keys(obj).length === 0 && obj.constructor === Object) {
                    self.invoice_items.splice(i, 1);
                } else {
                    for (let value in obj) {
                        if (obj[value] === null) {
                            self.invoice_items.splice(i, 1);
                        }
                    }
                }
            }
        },
        validate() {
            this.emptyFieldCheck();
            // Invoice Form is empty
            if (!this.date || !this.number || !this.supplier) {
                return 'Error: Please fill out all fields';
            } else if (this.invoice_items.length <= 0) {
                return 'Error: Please add at least one item to the invoice';
            } else {
                return true;
            }
        },
        submit() {
            if (this.validate() === true) {
                try {
                    axios.post('../app/controllers/invoices.php', {
                        date: this.date,
                        number: this.number,
                        supplier_id: this.findSupplier(),
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then((response) => {
                        let invoice_items = this.invoice_items;
                        for (let n = 0; n < invoice_items.length; n++) {
                            let self = invoice_items[n];
                            if (Object.keys(self).length === 0 && self.constructor === Object) {
                                break;
                            } else {
                                axios.post('../app/controllers/invoice_items.php', {
                                    invoice_id: response.data.id,
                                    description: invoice_items[n].description,
                                    category_id: this.findCategory(invoice_items[n].category),
                                    quantity: invoice_items[n].quantity,
                                    unit_price: invoice_items[n].unit_price
                                })
                            }
                        }
                        this.$router.push('/invoices/' + response.data.id);
                        this.$root.notice.message = 'Invoice created successfully';
                        this.$root.notice.state = 'success';
                    });
                } catch (error) {
                    console.log(error);
                }
            } else {
                this.$root.notice.message = this.validate();
                this.$root.notice.state = 'error';
            }
        }
    },
    components: {
        invoice_items_for_invoice_form
    }
};

module.exports = new_invoice;

/***/ }),

/***/ "./app/assets/components/layout/cards.js":
/*!***********************************************!*\
  !*** ./app/assets/components/layout/cards.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const cards = {
    name: 'cards',
    data() {
        return {
            cards: this.$root.pages
        };
    },
    template: '#cards'
};

module.exports = cards;

/***/ }),

/***/ "./app/assets/components/layout/home.js":
/*!**********************************************!*\
  !*** ./app/assets/components/layout/home.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let cards = __webpack_require__(/*! ./cards.js */ "./app/assets/components/layout/cards.js");

const home = {
    name: 'home',
    template: '#home',
    components: {
        cards
    }
};
module.exports = home;


/***/ }),

/***/ "./app/assets/components/layout/login.js":
/*!***********************************************!*\
  !*** ./app/assets/components/layout/login.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const login = {
    template: '#login_template',
    data() {
        return {
            username: null,
            password: null
        };
    },
    methods: {
        submit() {
            axios.post('/app/lib/login.php', {
                username: this.username,
                password: this.password
            }).then((response) => {
                this.$root.notice.message = response.data.message;
                this.$root.notice.state = response.data.status === 'authenticated' ? 'success' : 'error';
                if (response.data.status === 'authenticated') {
                    this.$root.auth = true;
                    this.$router.push('/');
                } else {

                }
            })
        }
    }
};

module.exports = login;

/***/ }),

/***/ "./app/assets/components/layout/navigation.js":
/*!****************************************************!*\
  !*** ./app/assets/components/layout/navigation.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const navigation = {
    name: 'navigation',
    data() {
        return {
            pages: this.$root.pages,
        };
    },
    template: '#navigation',
    mounted() {
        this.checkForDefaultImages();
    },
    methods: {
        checkForDefaultImages() {
            axios.get('/app/controllers/inventory.php').then((response) => {
                for (let items of response.data) {
                    if (items['attributes']['image_path'] === 'http://placehold.it/300/300') {
                        this.$root.inventory_items_with_default_image.push(items);
                    }
                }
            })
        }
    }
};

module.exports = navigation;

/***/ }),

/***/ "./app/assets/components/router.js":
/*!*****************************************!*\
  !*** ./app/assets/components/router.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Site
let home_page = __webpack_require__(/*! ./layout/home */ "./app/assets/components/layout/home.js");
let login = __webpack_require__(/*! ./layout/login */ "./app/assets/components/layout/login.js");

// let products = require('./products/products');

// Invoices
let single_invoice = __webpack_require__(/*! ./invoices/invoice */ "./app/assets/components/invoices/invoice.js");
let invoices_index = __webpack_require__(/*! ./invoices/invoices_index */ "./app/assets/components/invoices/invoices_index.js");
let new_invoice = __webpack_require__(/*! ./invoices/new_invoice */ "./app/assets/components/invoices/new_invoice.js");

// Inventory
let inventory_index = __webpack_require__(/*! ./inventory/inventory_index */ "./app/assets/components/inventory/inventory_index.js");
let single_inventory_item = __webpack_require__(/*! ./inventory/inventory_item */ "./app/assets/components/inventory/inventory_item.js");
let new_inventory_item = __webpack_require__(/*! ./inventory/new_inventory_item */ "./app/assets/components/inventory/new_inventory_item.js");

// Dashboard

let default_images_to_change = __webpack_require__(/*! ./dashboard/default_images_to_change */ "./app/assets/components/dashboard/default_images_to_change.js");

// Routes
const routes = [
    {path: '/', component: home_page},
    {
        path: '/login',
        component: login
    },
    // Invoices
    {path: '/invoices', component: invoices_index, props: true},
    {name: 'new_invoice', path: '/invoices/new', component: new_invoice},
    {name: 'invoice', path: '/invoices/:id', component: single_invoice},

    // Inventory
    {path: '/inventory', component: inventory_index, props: true},
    {name: 'new_inventory_item', path: '/inventory/new', component: new_inventory_item},
    {name: 'inventory_item', path: '/inventory/:id', component: single_inventory_item},

    // Products - INCOMPLETE
    // {path: '/products', component: products},

    // Dashboard
    {path: '/dashboard', component: default_images_to_change}

];
const router = new VueRouter({
    routes,
    mode: 'history'
});

module.exports = router;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXBwL2Fzc2V0cy9jb21wb25lbnRzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvZGFzaGJvYXJkL2RlZmF1bHRfaW1hZ2VzX3RvX2NoYW5nZS5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvZm9ybV9jb21wb25lbnRzL2ltYWdlX3VwbG9hZC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvaW52ZW50b3J5L2ludmVudG9yeV9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvaW52ZW50b3J5L2ludmVudG9yeV9pdGVtLmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9pbnZlbnRvcnkvbmV3X2ludmVudG9yeV9pdGVtLmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9pbnZvaWNlcy9pbnZvaWNlLmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9pbnZvaWNlcy9pbnZvaWNlX2l0ZW1zX2Zvcl9pbnZvaWNlLmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9pbnZvaWNlcy9pbnZvaWNlX2l0ZW1zX2Zvcl9pbnZvaWNlX2Zvcm0uanMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2Fzc2V0cy9jb21wb25lbnRzL2ludm9pY2VzL2ludm9pY2VzX2luZGV4LmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9pbnZvaWNlcy9uZXdfaW52b2ljZS5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvbGF5b3V0L2NhcmRzLmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9sYXlvdXQvaG9tZS5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvbGF5b3V0L2xvZ2luLmpzIiwid2VicGFjazovLy8uL2FwcC9hc3NldHMvY29tcG9uZW50cy9sYXlvdXQvbmF2aWdhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvYXNzZXRzL2NvbXBvbmVudHMvcm91dGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixrQkFBa0I7QUFDbkMsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsMkJBQTJCLDJCQUEyQjtBQUN0RDtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDLDJCQUEyQixlQUFlO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7OztBQy9HRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBLDJCQUEyQiwyQkFBMkI7QUFDdEQ7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSwyQkFBMkIsaUNBQWlDO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBDOzs7Ozs7Ozs7OztBQ3RGQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSw4Qjs7Ozs7Ozs7Ozs7QUM3RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0EsbUNBQW1DLHNCQUFzQjtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUM7Ozs7Ozs7Ozs7O0FDL0dBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiOztBQUVBLFNBQVM7QUFDVDtBQUNBLDJCQUEyQiw0QkFBNEI7QUFDdkQ7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Qzs7Ozs7Ozs7Ozs7QUNyRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7QUNoRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjs7QUFFQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QjtBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QjtBQUN4QjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiw0QkFBNEI7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsNkJBQTZCOztBQUU3QjtBQUNBOztBQUVBLDJDQUEyQywwQkFBMEI7QUFDckU7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDO0FBQ3JDO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLCtCQUErQjtBQUM5RDtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBLDJCQUEyQiwyQkFBMkI7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBLDJCQUEyQiwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDOzs7Ozs7Ozs7OztBQ3ZNQTtBQUNBO0FBQ0E7QUFDQSwyQzs7Ozs7Ozs7Ozs7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBLDJCQUEyQixtQkFBbUI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdEOzs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiOztBQUVBLGFBQWE7QUFDYjtBQUNBLG1DQUFtQztBQUNuQztBQUNBLG1DQUFtQyxxQkFBcUI7QUFDeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQzs7Ozs7Ozs7Ozs7QUMxSEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiwrQkFBK0I7QUFDOUQ7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQSwyQkFBMkIsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsK0JBQStCLDJCQUEyQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiOztBQUVBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsMkJBQTJCLCtCQUErQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0EsdUNBQXVDLDBCQUEwQjtBQUNqRTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNkI7Ozs7Ozs7Ozs7O0FDbkpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7OztBQ1ZBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7OztBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7OztBQ3hCQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUssZ0NBQWdDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUssMERBQTBEO0FBQy9ELEtBQUssbUVBQW1FO0FBQ3hFLEtBQUssa0VBQWtFOztBQUV2RTtBQUNBLEtBQUssNERBQTREO0FBQ2pFLEtBQUssa0ZBQWtGO0FBQ3ZGLEtBQUssaUZBQWlGOztBQUV0RjtBQUNBLFFBQVEsdUNBQXVDOztBQUUvQztBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHdCIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL1wiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2FwcC9hc3NldHMvY29tcG9uZW50cy9hcHAuanNcIik7XG4iLCJsZXQgbmF2aWdhdGlvbiA9IHJlcXVpcmUoJy4vbGF5b3V0L25hdmlnYXRpb24uanMnKTtcbmxldCBob21lID0gcmVxdWlyZSgnLi9sYXlvdXQvaG9tZS5qcycpO1xubGV0IHJvdXRlciA9IHJlcXVpcmUoJy4vcm91dGVyLmpzJyk7XG5sZXQgbG9naW4gPSByZXF1aXJlKCcuL2xheW91dC9sb2dpbi5qcycpO1xuXG5jb25zdCBhcHAgPSBuZXcgVnVlKHtcbiAgICBkYXRhKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY2F0ZWdvcmllczogW10sXG4gICAgICAgICAgICBzdXBwbGllcnM6IFtdLFxuICAgICAgICAgICAgbm90aWNlOiB7XG4gICAgICAgICAgICAgICAgbWVzc2FnZTogbnVsbCxcbiAgICAgICAgICAgICAgICBzdGF0ZTogbnVsbFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGF1dGg6IG51bGwsXG4gICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgIHBhZ2VzOiBbXG4gICAgICAgICAgICAgICAge3RpdGxlOiAnaW52b2ljZXMnfSxcbiAgICAgICAgICAgICAgICB7dGl0bGU6ICdpbnZlbnRvcnknfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpbnZlbnRvcnlfaXRlbXNfd2l0aF9kZWZhdWx0X2ltYWdlOiBbXVxuICAgICAgICB9O1xuICAgIH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgdGhpcy5nZXRDYXRlZ29yaWVzKCk7XG4gICAgICAgIHRoaXMuZ2V0U3VwcGxpZXJzKCk7XG4gICAgICAgIHRoaXMuYnJvd3NlckRldGVjdGlvbigpO1xuICAgICAgICB0aGlzLmNoZWNrRm9yTG9naW4oKTtcbiAgICB9LFxuICAgIGNvbXB1dGVkOiB7XG4gICAgICAgIG1lc3NhZ2UoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpY2UubWVzc2FnZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHRoaXMucmVzZXROb3RpY2UoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpY2UubWVzc2FnZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG4gICAgY29tcG9uZW50czoge1xuICAgICAgICBsb2dpbixcbiAgICAgICAgaG9tZSxcbiAgICAgICAgbmF2aWdhdGlvbixcblxuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICByZXNldE5vdGljZSgpIHtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICQoJyNub3RpY2UnKS5mYWRlT3V0KCdzbG93JywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLm5vdGljZS5tZXNzYWdlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5ub3RpY2Uuc3RhdGUgPSBudWxsO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LCAyMDAwKTtcbiAgICAgICAgfSxcbiAgICAgICAgY2hlY2tGb3JMb2dpbigpIHtcbiAgICAgICAgICAgIGlmIChkb2N1bWVudC5jb29raWUgJiYgdGhpcy5nZXRDb29raWUoJ3VzZXJfaWQnKSAmJiB0aGlzLmdldENvb2tpZSgnc2VyaWFsJykgJiYgdGhpcy5nZXRDb29raWUoJ3Rva2VuJykpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmF1dGggPSB0cnVlO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCgnL2xvZ2luJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBicm93c2VyRGV0ZWN0aW9uKCkge1xuICAgICAgICAgICAgLy8gUmV0dXJucyB0cnVlIGlmIG1vYmlsZSBicm93c2VyXG4gICAgICAgICAgICBsZXQgY2hlY2sgPSBmYWxzZTtcbiAgICAgICAgICAgIChmdW5jdGlvbiAoYSkge1xuICAgICAgICAgICAgICAgIGlmICgvKGFuZHJvaWR8YmJcXGQrfG1lZWdvKS4rbW9iaWxlfGF2YW50Z298YmFkYVxcL3xibGFja2JlcnJ5fGJsYXplcnxjb21wYWx8ZWxhaW5lfGZlbm5lY3xoaXB0b3B8aWVtb2JpbGV8aXAoaG9uZXxvZCl8aXJpc3xraW5kbGV8bGdlIHxtYWVtb3xtaWRwfG1tcHxtb2JpbGUuK2ZpcmVmb3h8bmV0ZnJvbnR8b3BlcmEgbShvYnxpbilpfHBhbG0oIG9zKT98cGhvbmV8cChpeGl8cmUpXFwvfHBsdWNrZXJ8cG9ja2V0fHBzcHxzZXJpZXMoNHw2KTB8c3ltYmlhbnx0cmVvfHVwXFwuKGJyb3dzZXJ8bGluayl8dm9kYWZvbmV8d2FwfHdpbmRvd3MgY2V8eGRhfHhpaW5vL2kudGVzdChhKSB8fCAvMTIwN3w2MzEwfDY1OTB8M2dzb3w0dGhwfDUwWzEtNl1pfDc3MHN8ODAyc3xhIHdhfGFiYWN8YWMoZXJ8b298c1xcLSl8YWkoa298cm4pfGFsKGF2fGNhfGNvKXxhbW9pfGFuKGV4fG55fHl3KXxhcHR1fGFyKGNofGdvKXxhcyh0ZXx1cyl8YXR0d3xhdShkaXxcXC1tfHIgfHMgKXxhdmFufGJlKGNrfGxsfG5xKXxiaShsYnxyZCl8YmwoYWN8YXopfGJyKGV8dil3fGJ1bWJ8YndcXC0obnx1KXxjNTVcXC98Y2FwaXxjY3dhfGNkbVxcLXxjZWxsfGNodG18Y2xkY3xjbWRcXC18Y28obXB8bmQpfGNyYXd8ZGEoaXR8bGx8bmcpfGRidGV8ZGNcXC1zfGRldml8ZGljYXxkbW9ifGRvKGN8cClvfGRzKDEyfFxcLWQpfGVsKDQ5fGFpKXxlbShsMnx1bCl8ZXIoaWN8azApfGVzbDh8ZXooWzQtN10wfG9zfHdhfHplKXxmZXRjfGZseShcXC18Xyl8ZzEgdXxnNTYwfGdlbmV8Z2ZcXC01fGdcXC1tb3xnbyhcXC53fG9kKXxncihhZHx1bil8aGFpZXxoY2l0fGhkXFwtKG18cHx0KXxoZWlcXC18aGkocHR8dGEpfGhwKCBpfGlwKXxoc1xcLWN8aHQoYyhcXC18IHxffGF8Z3xwfHN8dCl8dHApfGh1KGF3fHRjKXxpXFwtKDIwfGdvfG1hKXxpMjMwfGlhYyggfFxcLXxcXC8pfGlicm98aWRlYXxpZzAxfGlrb218aW0xa3xpbm5vfGlwYXF8aXJpc3xqYSh0fHYpYXxqYnJvfGplbXV8amlnc3xrZGRpfGtlaml8a2d0KCB8XFwvKXxrbG9ufGtwdCB8a3djXFwtfGt5byhjfGspfGxlKG5vfHhpKXxsZyggZ3xcXC8oa3xsfHUpfDUwfDU0fFxcLVthLXddKXxsaWJ3fGx5bnh8bTFcXC13fG0zZ2F8bTUwXFwvfG1hKHRlfHVpfHhvKXxtYygwMXwyMXxjYSl8bVxcLWNyfG1lKHJjfHJpKXxtaShvOHxvYXx0cyl8bW1lZnxtbygwMXwwMnxiaXxkZXxkb3x0KFxcLXwgfG98dil8enopfG10KDUwfHAxfHYgKXxtd2JwfG15d2F8bjEwWzAtMl18bjIwWzItM118bjMwKDB8Mil8bjUwKDB8Mnw1KXxuNygwKDB8MSl8MTApfG5lKChjfG0pXFwtfG9ufHRmfHdmfHdnfHd0KXxub2soNnxpKXxuenBofG8yaW18b3AodGl8d3YpfG9yYW58b3dnMXxwODAwfHBhbihhfGR8dCl8cGR4Z3xwZygxM3xcXC0oWzEtOF18YykpfHBoaWx8cGlyZXxwbChheXx1Yyl8cG5cXC0yfHBvKGNrfHJ0fHNlKXxwcm94fHBzaW98cHRcXC1nfHFhXFwtYXxxYygwN3wxMnwyMXwzMnw2MHxcXC1bMi03XXxpXFwtKXxxdGVrfHIzODB8cjYwMHxyYWtzfHJpbTl8cm8odmV8em8pfHM1NVxcL3xzYShnZXxtYXxtbXxtc3xueXx2YSl8c2MoMDF8aFxcLXxvb3xwXFwtKXxzZGtcXC98c2UoYyhcXC18MHwxKXw0N3xtY3xuZHxyaSl8c2doXFwtfHNoYXJ8c2llKFxcLXxtKXxza1xcLTB8c2woNDV8aWQpfHNtKGFsfGFyfGIzfGl0fHQ1KXxzbyhmdHxueSl8c3AoMDF8aFxcLXx2XFwtfHYgKXxzeSgwMXxtYil8dDIoMTh8NTApfHQ2KDAwfDEwfDE4KXx0YShndHxsayl8dGNsXFwtfHRkZ1xcLXx0ZWwoaXxtKXx0aW1cXC18dFxcLW1vfHRvKHBsfHNoKXx0cyg3MHxtXFwtfG0zfG01KXx0eFxcLTl8dXAoXFwuYnxnMXxzaSl8dXRzdHx2NDAwfHY3NTB8dmVyaXx2aShyZ3x0ZSl8dmsoNDB8NVswLTNdfFxcLXYpfHZtNDB8dm9kYXx2dWxjfHZ4KDUyfDUzfDYwfDYxfDcwfDgwfDgxfDgzfDg1fDk4KXx3M2MoXFwtfCApfHdlYmN8d2hpdHx3aShnIHxuY3xudyl8d21sYnx3b251fHg3MDB8eWFzXFwtfHlvdXJ8emV0b3x6dGVcXC0vaS50ZXN0KGEuc3Vic3RyKDAsIDQpKSkgY2hlY2sgPSB0cnVlO1xuICAgICAgICAgICAgfSkobmF2aWdhdG9yLnVzZXJBZ2VudCB8fCBuYXZpZ2F0b3IudmVuZG9yIHx8IHdpbmRvdy5vcGVyYSk7XG4gICAgICAgICAgICByZXR1cm4gY2hlY2s7XG4gICAgICAgIH0sXG4gICAgICAgIGZpbmRDYXRlZ29yeShjYXRlZ29yeSkge1xuICAgICAgICAgICAgbGV0IGFsbF9jYXRlZ29yaWVzID0gdGhpcy4kcm9vdC5jYXRlZ29yaWVzO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxfY2F0ZWdvcmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmIChhbGxfY2F0ZWdvcmllc1tpXS5pZCA9PT0gY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFsbF9jYXRlZ29yaWVzW2ldLm5hbWU7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChhbGxfY2F0ZWdvcmllc1tpXS5uYW1lID09PSBjYXRlZ29yeSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYWxsX2NhdGVnb3JpZXNbaV0uaWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBnZXRDYXRlZ29yaWVzKHVybCkge1xuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvYXBwL2NvbnRyb2xsZXJzL2NhdGVnb3JpZXMucGhwJykudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3JpZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgICBnZXRTdXBwbGllcnModXJsKSB7XG4gICAgICAgICAgICBheGlvcy5nZXQoJy9hcHAvY29udHJvbGxlcnMvc3VwcGxpZXJzLnBocCcpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zdXBwbGllcnMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgICBnZXRDb29raWUoY25hbWUpIHtcbiAgICAgICAgICAgIGxldCBuYW1lID0gY25hbWUgKyBcIj1cIjtcbiAgICAgICAgICAgIGxldCBkZWNvZGVkQ29va2llID0gZGVjb2RlVVJJQ29tcG9uZW50KGRvY3VtZW50LmNvb2tpZSk7XG4gICAgICAgICAgICBsZXQgY2EgPSBkZWNvZGVkQ29va2llLnNwbGl0KCc7Jyk7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNhLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgbGV0IGMgPSBjYVtpXTtcbiAgICAgICAgICAgICAgICB3aGlsZSAoYy5jaGFyQXQoMCkgPT0gJyAnKSB7XG4gICAgICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGMuaW5kZXhPZihuYW1lKSA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lLmxlbmd0aCwgYy5sZW5ndGgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBcIlwiO1xuICAgICAgICB9XG4gICAgfSxcbiAgICByb3V0ZXIsXG59KS4kbW91bnQoJyNhcHAnKTtcblxuIiwibGV0IGltYWdlX3VwbG9hZCA9IHJlcXVpcmUoJy4uL2Zvcm1fY29tcG9uZW50cy9pbWFnZV91cGxvYWQnKTtcbmNvbnN0IGRlZmF1bHRfaW1hZ2VzX3RvX2NoYW5nZSA9IHtcbiAgICBuYW1lOiAnZGVmYXVsdF9pbWFnZXNfdG9fY2hhbmdlJyxcbiAgICB0ZW1wbGF0ZTogJyNkYXNoYm9hcmRfdGVtcGxhdGUnLFxuICAgIGNvbXBvbmVudHM6IHtcbiAgICAgICAgdGFibGVfcm93OiB7XG4gICAgICAgICAgICBuYW1lOiAnaW52ZW50b3J5X2l0ZW0nLFxuICAgICAgICAgICAgdGVtcGxhdGU6ICcjZGFzaGJvYXJkX3RhYmxlX3Jvd190ZW1wbGF0ZScsXG4gICAgICAgICAgICBwcm9wczogWydpZCcsICdwcm9wX2ltYWdlX3BhdGgnLCAncHJvcF9kZXNjcmlwdGlvbicsICdxdWFudGl0eScsICdjYXRlZ29yeSddLFxuICAgICAgICAgICAgY29tcG9uZW50czoge1xuICAgICAgICAgICAgICAgIGltYWdlX3VwbG9hZFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGRhdGEoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgZGF0YV9kZXNjcmlwdGlvbjogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgZGF0YV9pbWFnZV9wYXRoOiBudWxsXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG1ldGhvZHM6IHtcbiAgICAgICAgICAgICAgICBzZW5kQ2hhbmdlcygpIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGl0ZW1fdG9fcGFzcyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiB0aGlzLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IHRoaXMuZGF0YV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgICAgICAgICAgIGltYWdlX3BhdGg6IHRoaXMuZGF0YV9pbWFnZV9wYXRoLFxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IHRoaXMucXVhbnRpdHksXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeTogdGhpcy5jYXRlZ29yeVxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuaXRlbXNfdG9fY2hhbmdlLnB1c2goaXRlbV90b19wYXNzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY29tcHV0ZWQ6IHtcbiAgICAgICAgICAgICAgICBpbWFnZV9wYXRoOiB7XG4gICAgICAgICAgICAgICAgICAgIGdldDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucHJvcF9pbWFnZV9wYXRoO1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBzZXQ6IGZ1bmN0aW9uIChuZXdfdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YV9pbWFnZV9wYXRoID0gbmV3X3ZhbHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZW5kQ2hhbmdlcygpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjoge1xuICAgICAgICAgICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3BfZGVzY3JpcHRpb247XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHNldDogZnVuY3Rpb24gKG5ld192YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhX2Rlc2NyaXB0aW9uID0gbmV3X3ZhbHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZW5kQ2hhbmdlcygpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG1vdW50ZWQoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhX2ltYWdlX3BhdGggPSB0aGlzLmltYWdlX3BhdGg7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhX2Rlc2NyaXB0aW9uID0gdGhpcy5kZXNjcmlwdGlvbjtcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH1cbiAgICB9LFxuICAgIGRhdGEoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBpdGVtc190b19jaGFuZ2U6IFtdXG4gICAgICAgIH1cbiAgICB9LFxuICAgIG1ldGhvZHM6IHtcbiAgICAgICAgZmluZENhdGVnb3J5KGNhdGVnb3J5KSB7XG4gICAgICAgICAgICBsZXQgYWxsX2NhdGVnb3JpZXMgPSB0aGlzLiRyb290LmNhdGVnb3JpZXM7XG5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYWxsX2NhdGVnb3JpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAoYWxsX2NhdGVnb3JpZXNbaV0uaWQgPT09IGNhdGVnb3J5KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBhbGxfY2F0ZWdvcmllc1tpXS5uYW1lO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYWxsX2NhdGVnb3JpZXNbaV0ubmFtZSA9PT0gY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFsbF9jYXRlZ29yaWVzW2ldLmlkO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgc3VibWl0Q2hhbmdlcygpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5pdGVtc190b19jaGFuZ2UubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBheGlvcy5wYXRjaCgnL2FwcC9jb250cm9sbGVycy9pbnZlbnRvcnkucGhwJywge1xuICAgICAgICAgICAgICAgICAgICBpZDogdGhpcy5pdGVtc190b19jaGFuZ2VbaV0uaWQsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiB0aGlzLml0ZW1zX3RvX2NoYW5nZVtpXS5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IHRoaXMuaXRlbXNfdG9fY2hhbmdlW2ldLnF1YW50aXR5LFxuICAgICAgICAgICAgICAgICAgICBpbWFnZV9wYXRoOiB0aGlzLml0ZW1zX3RvX2NoYW5nZVtpXS5pbWFnZV9wYXRoLFxuICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeV9pZDogdGhpcy5maW5kQ2F0ZWdvcnkodGhpcy5pdGVtc190b19jaGFuZ2VbaV0uY2F0ZWdvcnkpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn07XG5tb2R1bGUuZXhwb3J0cyA9IGRlZmF1bHRfaW1hZ2VzX3RvX2NoYW5nZTsiLCJjb25zdCBTVEFUVVNfSU5JVElBTCA9IDAsIFNUQVRVU19TQVZJTkcgPSAxLCBTVEFUVVNfU1VDQ0VTUyA9IDIsIFNUQVRVU19GQUlMRUQgPSAzO1xuXG5mdW5jdGlvbiB1cGxvYWQoZm9ybURhdGEpIHtcbiAgICBmb3IgKGxldCBba2V5LCB2YWx1ZV0gb2YgZm9ybURhdGEuZW50cmllcygpKSB7XG4gICAgICAgIHJldHVybiBheGlvcy5wb3N0KCcvYXBwL2NvbnRyb2xsZXJzL2Zvcm1fY29tcG9uZW50cy9pbWFnZV91cGxvYWQucGhwJyxcbiAgICAgICAgICAgIGZvcm1EYXRhLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdtdWx0aXBhcnQvZm9ybS1kYXRhJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmNvbnN0IGltYWdlX3VwbG9hZCA9IHtcbiAgICBuYW1lOiAnaW1hZ2VfdXBsb2FkJyxcbiAgICB0ZW1wbGF0ZTogJyNpbWFnZV91cGxvYWRfdGVtcGxhdGUnLFxuICAgIGRhdGEoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB1cGxvYWRlZEZpbGU6IFtdLFxuICAgICAgICAgICAgdXBsb2FkRXJyb3I6IG51bGwsXG4gICAgICAgICAgICBjdXJyZW50U3RhdHVzOiBudWxsXG4gICAgICAgIH07XG4gICAgfSxcbiAgICBjb21wdXRlZDoge1xuICAgICAgICBpc0luaXRpYWwoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50U3RhdHVzID09PSBTVEFUVVNfSU5JVElBTDtcbiAgICAgICAgfSxcbiAgICAgICAgaXNTYXZpbmcoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50U3RhdHVzID09PSBTVEFUVVNfU0FWSU5HO1xuICAgICAgICB9LFxuICAgICAgICBpc1N1Y2Nlc3MoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50U3RhdHVzID09PSBTVEFUVVNfU1VDQ0VTUztcbiAgICAgICAgfSxcbiAgICAgICAgaXNGYWlsZWQoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50U3RhdHVzID09PSBTVEFUVVNfRkFJTEVEO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIHJlc2V0KCkge1xuICAgICAgICAgICAgLy8gcmVzZXQgZm9ybSB0byBpbml0aWFsIHN0YXRlXG4gICAgICAgICAgICB0aGlzLnVwbG9hZGVkRmlsZSA9IFtdO1xuICAgICAgICAgICAgdGhpcy5jdXJyZW50U3RhdHVzID0gU1RBVFVTX0lOSVRJQUw7XG4gICAgICAgICAgICB0aGlzLnVwbG9hZEVycm9yID0gbnVsbDtcbiAgICAgICAgfSxcbiAgICAgICAgc2F2ZShmb3JtRGF0YSkge1xuICAgICAgICAgICAgLy8gdXBsb2FkIGRhdGEgdG8gdGhlIHNlcnZlclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50U3RhdHVzID0gU1RBVFVTX1NBVklORztcblxuICAgICAgICAgICAgdXBsb2FkKGZvcm1EYXRhKVxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRlZEZpbGUgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZGVkRmlsZS51cmwgPSB0aGlzLiRwYXJlbnQuaW1hZ2VfcGF0aCA9ICcvcHVibGljL2ltYWdlcy91cGxvYWRzLycgKyByZXNwb25zZS5kYXRhLm5hbWU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFN0YXR1cyA9IFNUQVRVU19TVUNDRVNTO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKGVyciA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkRXJyb3IgPSBlcnIucmVzcG9uc2U7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFN0YXR1cyA9IFNUQVRVU19GQUlMRUQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICAgIGZpbGVzQ2hhbmdlKGZpZWxkTmFtZSwgZmlsZUxpc3QpIHtcbiAgICAgICAgICAgIC8vIGhhbmRsZSBmaWxlIGNoYW5nZXNcbiAgICAgICAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG5cbiAgICAgICAgICAgIGlmICghZmlsZUxpc3QubGVuZ3RoKSByZXR1cm47XG5cbiAgICAgICAgICAgIC8vIGFwcGVuZCB0aGUgZmlsZSB0byBGb3JtRGF0YVxuICAgICAgICAgICAgZm9ybURhdGEuYXBwZW5kKGZpZWxkTmFtZSwgZmlsZUxpc3RbMF0sIGZpbGVMaXN0WzBdLm5hbWUpO1xuICAgICAgICAgICAgLy8gc2F2ZSBpdFxuICAgICAgICAgICAgdGhpcy5zYXZlKGZvcm1EYXRhKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgdGhpcy5yZXNldCgpO1xuICAgIH0sXG59O1xubW9kdWxlLmV4cG9ydHMgPSBpbWFnZV91cGxvYWQ7IiwiY29uc3QgaW52ZW50b3J5X2luZGV4ID0ge1xuICAgIG5hbWU6ICdpbnZlbnRvcnlfaW5kZXgnLFxuICAgIHRlbXBsYXRlOiAnI2ludmVudG9yeV9pbmRleF90ZW1wbGF0ZScsXG4gICAgZGF0YSgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGludmVudG9yeTogW1xuICAgICAgICAgICAgICAgIC8vIEFuIGFycmF5IG9mIG9iamVjdHMgY29sbGVjdGVkIG9uIGNyZWF0aW9uIHdpdGggZmV0Y2hEYXRhKClcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICByZXN1bHRzX3Blcl9wYWdlOiAxNSxcbiAgICAgICAgICAgIHBhZ2VfbnVtYmVyOiAxLFxuICAgICAgICAgICAgc2V0dGluZ3NfZW5hYmxlZDogbnVsbFxuICAgICAgICB9O1xuICAgIH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgdGhpcy5mZXRjaERhdGEoKTtcbiAgICB9LFxuICAgIGNvbXBvbmVudHM6IHtcbiAgICAgICAgdGFibGVfcm93OiB7XG4gICAgICAgICAgICB0ZW1wbGF0ZTogJyNpbnZlbnRvcnlfdGFibGVfcm93X3RlbXBsYXRlJyxcbiAgICAgICAgICAgIG5hbWU6ICdpbnZlbnRvcnlfaXRlbScsXG4gICAgICAgICAgICBwcm9wczogW1xuICAgICAgICAgICAgICAgICdpZCcsXG4gICAgICAgICAgICAgICAgJ2Rlc2NyaXB0aW9uJyxcbiAgICAgICAgICAgICAgICAncXVhbnRpdHknLFxuICAgICAgICAgICAgICAgICdjYXRlZ29yeScsXG4gICAgICAgICAgICAgICAgJ2ltYWdlX3BhdGgnXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGF0YSgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBkZWxldGVfY29uZmlybWF0aW9uOiBudWxsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtZXRob2RzOiB7XG4gICAgICAgICAgICAgICAgZGVsZXRlSW52ZW50b3J5SXRlbShpZCkgeyAgLy8gPD09PSBUaGVyZSBpcyBhIGNvbW1lbnQgcmVmZXJyaW5nIHRvIHRoaXMgaW4gX3ZhcmlhYmxlcy5zY3NzXG4gICAgICAgICAgICAgICAgICAgIGxldCBpbnZlbnRvcnkgPSB0aGlzLiRwYXJlbnQuaW52ZW50b3J5O1xuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGludmVudG9yeS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGludmVudG9yeVtpXS5pZCA9PT0gdGhpcy5pZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5pbnZlbnRvcnkuc3BsaWNlKGksIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zLmRlbGV0ZSgnL2FwcC9jb250cm9sbGVycy9pbnZlbnRvcnkucGhwJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBpZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxldGVfY29uZmlybWF0aW9uID0gbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSAnSW52ZW50b3J5IEl0ZW0gZGVsZXRlZCBzdWNjZXNzZnVsbHknLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2Uuc3RhdGUgPSAnc3VjY2VzcydcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSAnVGhlcmUgd2FzIGFuIGVycm9yIGRlbGV0aW5nIHRoZSBpbnZlbnRvcnkgaXRlbSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLnN0YXRlID0gJ2Vycm9yJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGRlbGV0ZUNvbmZpcm1hdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxldGVfY29uZmlybWF0aW9uID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuICAgIG1ldGhvZHM6IHtcbiAgICAgICAgY2hhbmdlUGFnZShkaXJlY3Rpb24pIHtcbiAgICAgICAgICAgIGRpcmVjdGlvbiA9IGRpcmVjdGlvbi50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gJ3ByZXZpb3VzJyAmJiB0aGlzLnBhZ2VfbnVtYmVyID4gMSkge1xuICAgICAgICAgICAgICAgIHRoaXMucGFnZV9udW1iZXIgLT0gMTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZGlyZWN0aW9uID09PSAnbmV4dCcgJiYgdGhpcy5jb21wdXRlZEFycmF5Lmxlbmd0aCA+PSB0aGlzLnJlc3VsdHNfcGVyX3BhZ2UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnBhZ2VfbnVtYmVyICs9IDE7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZldGNoRGF0YSgpIHtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgIHNlbGYuJHJvb3QubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBheGlvcy5nZXQoJy9hcHAvY29udHJvbGxlcnMvaW52ZW50b3J5LnBocCcpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbnZlbnRvcnkgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIHNlbGYuJHJvb3QubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgICB1cGRhdGVSZXN1bHRzUGVyUGFnZUJ5KGNvdW50KSB7XG4gICAgICAgICAgICBsZXQgYnV0dG9ucyA9ICQoJy5zZXR0aW5nc19fY29udHJvbCA+IGRpdiA+IGJ1dHRvbicpO1xuICAgICAgICAgICAgYnV0dG9ucy5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICBsZXQgcmVzdWx0ID0gJChcIi5zZXR0aW5nc19fY29udHJvbCA+IGRpdiA+IGJ1dHRvbjpjb250YWlucyhcIiArIGNvdW50ICsgXCIpXCIpO1xuICAgICAgICAgICAgcmVzdWx0LnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgIHRoaXMucmVzdWx0c19wZXJfcGFnZSA9IGNvdW50O1xuICAgICAgICB9LFxuICAgICAgICAvLyBUb2dnbGVzIHZpc2liaWxpdHkgb2YgXCJPcHRpb25zXCIgZGl2IHRoYXQgY29udGFpbnMgYnV0dG9ucyB0byBhbiBFZGl0IGZvcm0gYW5kIGEgRGVzdHJveSBhY3Rpb24gYWNjb3JkaW5nbHlcbiAgICAgICAgdG9nZ2xlU2V0dGluZ3MoKSB7XG4gICAgICAgICAgICBsZXQgZSA9IHRoaXMuc2V0dGluZ3NfZW5hYmxlZDtcbiAgICAgICAgICAgIHRoaXMuc2V0dGluZ3NfZW5hYmxlZCA9IChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKGUgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkoKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgY29tcHV0ZWQ6IHtcbiAgICAgICAgY29tcHV0ZWRBcnJheSgpIHtcbiAgICAgICAgICAgIGxldCBmaXJzdCA9ICh0aGlzLnJlc3VsdHNfcGVyX3BhZ2UgKiAodGhpcy5wYWdlX251bWJlciAtIDEpKTtcbiAgICAgICAgICAgIGxldCBsYXN0ID0gdGhpcy5yZXN1bHRzX3Blcl9wYWdlICogdGhpcy5wYWdlX251bWJlcjtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmludmVudG9yeS5zbGljZShmaXJzdCwgbGFzdCk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGJlZm9yZVJvdXRlVXBkYXRlKHRvLCBmcm9tLCBuZXh0KSB7XG4gICAgICAgIG5leHQoKTtcbiAgICB9XG59O1xubW9kdWxlLmV4cG9ydHMgPSBpbnZlbnRvcnlfaW5kZXg7IiwibGV0IGltYWdlX3VwbG9hZCA9IHJlcXVpcmUoJy4uL2Zvcm1fY29tcG9uZW50cy9pbWFnZV91cGxvYWQnKTtcbmxldCBzaW5nbGVfaW52ZW50b3J5X2l0ZW0gPSB7XG4gICAgbmFtZTogJ2ludmVudG9yeV9pdGVtJyxcbiAgICB0ZW1wbGF0ZTogJyNpbnZlbnRvcnlfaXRlbV90ZW1wbGF0ZScsXG4gICAgZGF0YSgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBudWxsLFxuICAgICAgICAgICAgcXVhbnRpdHk6IG51bGwsXG4gICAgICAgICAgICBpbWFnZV9wYXRoOiBudWxsLFxuICAgICAgICAgICAgY2F0ZWdvcnlfaWQ6IG51bGwsXG4gICAgICAgICAgICBlZGl0YWJsZTogdGhpcy4kcm91dGUucGFyYW1zLmVkaXRhYmxlXG4gICAgICAgIH07XG4gICAgfSxcbiAgICBjb21wb25lbnRzOiB7XG4gICAgICAgIGltYWdlX3VwbG9hZFxuICAgIH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgdGhpcy5mZXRjaERhdGEodGhpcy4kcm91dGUucGFyYW1zLmlkKTtcbiAgICB9LFxuICAgIGNvbXB1dGVkOiB7XG4gICAgICAgIGNhdGVnb3JpZXMoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy4kcm9vdC5jYXRlZ29yaWVzXG4gICAgICAgIH0sXG4gICAgICAgIGNhdGVnb3J5KCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmluZENhdGVnb3J5KHRoaXMuY2F0ZWdvcnlfaWQpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIGZldGNoRGF0YShpZCkge1xuICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgc2VsZi4kcm9vdC5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIGF4aW9zLmdldChcIi9hcHAvY29udHJvbGxlcnMvaW52ZW50b3J5LnBocD9pZD1cIiArIGlkKVxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IHJlc3BvbnNlLmRhdGEuYXR0cmlidXRlcy5kZXNjcmlwdGlvbjtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xdWFudGl0eSA9IHJlc3BvbnNlLmRhdGEuYXR0cmlidXRlcy5xdWFudGl0eTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZV9wYXRoID0gcmVzcG9uc2UuZGF0YS5hdHRyaWJ1dGVzLmltYWdlX3BhdGg7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2F0ZWdvcnlfaWQgPSByZXNwb25zZS5kYXRhLmF0dHJpYnV0ZXMuY2F0ZWdvcnlfaWQ7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuJHJvb3QubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgICBpbml0aWFsaXplU2VsZWN0KCkge1xuICAgICAgICAgICAgJCgnZmllbGRzZXQgPiAuY3VzdG9tX3NlbGVjdCcpLmhpZGUoKTtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgICQoJyNjYXRlZ29yeV9pZCcpLnNlbGVjdG1lbnUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdDogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLmNhdGVnb3J5ID0gdWkuaXRlbS52YWx1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoJyNjYXRlZ29yeV9pZCcpLnNlbGVjdG1lbnUoJ29wZW4nKTtcblxuICAgICAgICB9LFxuICAgICAgICBmaW5kQ2F0ZWdvcnkoY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5jYXRlZ29yaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuY2F0ZWdvcmllc1tpXS5pZCA9PT0gY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2F0ZWdvcmllc1tpXS5uYW1lO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5jYXRlZ29yaWVzW2ldLm5hbWUgPT09IGNhdGVnb3J5KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNhdGVnb3JpZXNbaV0uaWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB1cGRhdGVJbnZlbnRvcnlJdGVtKCkge1xuICAgICAgICAgICAgYXhpb3MucGF0Y2goXCIvYXBwL2NvbnRyb2xsZXJzL2ludmVudG9yeS5waHBcIiwge1xuICAgICAgICAgICAgICAgIGlkOiB0aGlzLiRyb3V0ZS5wYXJhbXMuaWQsXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IHRoaXMuZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlfaWQ6IHRoaXMuY2F0ZWdvcnlfaWQsXG4gICAgICAgICAgICAgICAgaW1hZ2VfcGF0aDogdGhpcy5pbWFnZV9wYXRoLFxuICAgICAgICAgICAgICAgIHF1YW50aXR5OiB0aGlzLnF1YW50aXR5LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZWRpdGFibGUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5tZXNzYWdlID0gJ0ludmVudG9yeSBJdGVtIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5JztcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5zdGF0ZSA9ICdzdWNjZXNzJztcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9LFxuICAgIGJlZm9yZVJvdXRlVXBkYXRlKHRvLCBmcm9tLCBuZXh0KSB7XG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHRvKTtcbiAgICAgICAgbmV4dCgpO1xuICAgIH1cbn07XG5tb2R1bGUuZXhwb3J0cyA9IHNpbmdsZV9pbnZlbnRvcnlfaXRlbTsiLCJsZXQgaW1hZ2VfdXBsb2FkID0gcmVxdWlyZSgnLi4vZm9ybV9jb21wb25lbnRzL2ltYWdlX3VwbG9hZCcpO1xuY29uc3QgaW52ZW50b3J5X2l0ZW1fbmV3ID0ge1xuICAgIG5hbWU6ICduZXdfaW52ZW50b3J5X2l0ZW0nLFxuICAgIGRhdGEoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogbnVsbCxcbiAgICAgICAgICAgIHF1YW50aXR5OiBudWxsLFxuICAgICAgICAgICAgY2F0ZWdvcnk6IG51bGwsXG4gICAgICAgICAgICBpbWFnZV9wYXRoOiBudWxsXG4gICAgICAgIH07XG4gICAgfSxcbiAgICBjb21wb25lbnRzOiB7XG4gICAgICAgIGltYWdlX3VwbG9hZFxuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICB2YWxpZGF0ZSgpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5kZXNjcmlwdGlvbiB8fCAhdGhpcy5jYXRlZ29yeSB8fCAhdGhpcy5xdWFudGl0eSkge1xuICAgICAgICAgICAgICAgIHJldHVybiAnRXJyb3I6IFBsZWFzZSBmaWxsIG91dCBhbGwgZmllbGRzJztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHN1Ym1pdCgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnZhbGlkYXRlKCkgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBheGlvcy5wb3N0KCcvYXBwL2NvbnRyb2xsZXJzL2ludmVudG9yeS5waHAnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogdGhpcy5kZXNjcmlwdGlvbixcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiB0aGlzLnF1YW50aXR5LFxuICAgICAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnlfaWQ6IHRoaXMuY2F0ZWdvcnksXG4gICAgICAgICAgICAgICAgICAgICAgICBpbWFnZV9wYXRoOiB0aGlzLmltYWdlX3BhdGgsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKCcvaW52ZW50b3J5LycgKyByZXNwb25zZS5kYXRhLmlkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSAnSW52ZW50b3J5IEl0ZW0gY3JlYXRlZCBzdWNjZXNzZnVsbHknO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2Uuc3RhdGUgPSAnc3VjY2Vzcyc7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSB0aGlzLnZhbGlkYXRlKCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2Uuc3RhdGUgPSAnZXJyb3InO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBnZXREYXRhKCkge1xuICAgICAgICAgICAgJCgnI2ZpbGVUb1VwbG9hZCcpLmFkZENsYXNzKCdoYWxmX192aXNpYmxlJyk7XG4gICAgICAgICAgICBsZXQgZmlsZV9uYW1lID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2ZpbGVUb1VwbG9hZCcpLnZhbHVlO1xuICAgICAgICAgICAgJCgnI2ZpbGVfbmFtZScpLmFwcGVuZChmaWxlX25hbWUpO1xuICAgICAgICB9LFxuICAgICAgICBpbml0aWFsaXplU2VsZWN0KCkge1xuICAgICAgICAgICAgJCgnZmllbGRzZXQgPiAuY3VzdG9tX3NlbGVjdCcpLmhpZGUoKTtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgICQoJyNjYXRlZ29yeV9pZCcpLnNlbGVjdG1lbnUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdDogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLmNhdGVnb3J5ID0gdWkuaXRlbS52YWx1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoJyNjYXRlZ29yeV9pZCcpLnNlbGVjdG1lbnUoJ29wZW4nKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgdGVtcGxhdGU6ICcjbmV3X2ludmVudG9yeV9pdGVtX3RlbXBsYXRlJ1xufTtcbm1vZHVsZS5leHBvcnRzID0gaW52ZW50b3J5X2l0ZW1fbmV3OyIsImxldCBpbnZvaWNlX2l0ZW1zX2Zvcl9pbnZvaWNlID0gcmVxdWlyZSgnLi9pbnZvaWNlX2l0ZW1zX2Zvcl9pbnZvaWNlJyk7XG5sZXQgaW52b2ljZV9pdGVtc19mb3JfaW52b2ljZV9mb3JtID0gcmVxdWlyZSgnLi9pbnZvaWNlX2l0ZW1zX2Zvcl9pbnZvaWNlX2Zvcm0nKTtcbmxldCBzaW5nbGVfaW52b2ljZSA9IHtcbiAgICBuYW1lOiAnSW52b2ljZScsXG4gICAgdGVtcGxhdGU6ICcjaW52b2ljZV90ZW1wbGF0ZScsXG4gICAgZGF0YSgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG51bWJlcjogbnVsbCxcbiAgICAgICAgICAgIGRhdGU6IG51bGwsXG4gICAgICAgICAgICBzdXBwbGllcjogbnVsbCxcbiAgICAgICAgICAgIGludm9pY2VfaXRlbXM6IFt7fV0sXG4gICAgICAgICAgICB0b3RhbDogbnVsbCxcbiAgICAgICAgICAgIGVkaXRhYmxlOiB0aGlzLiRyb3V0ZS5wYXJhbXMuZWRpdGFibGUsXG4gICAgICAgICAgICBpbnZvaWNlX2l0ZW1zX3RvX2RlbGV0ZTogW11cbiAgICAgICAgfTtcbiAgICB9LFxuICAgIGNvbXBvbmVudHM6IHtcbiAgICAgICAgLy8gQ29tcG9uZW50cyB0aGF0IHJlbmRlciBhIHRlbXBsYXRlXG4gICAgICAgIGludm9pY2VfaXRlbXNfZm9yX2ludm9pY2UsXG4gICAgICAgIGludm9pY2VfaXRlbXNfZm9yX2ludm9pY2VfZm9ybVxuICAgIH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgdGhpcy5mZXRjaERhdGEodGhpcy4kcm91dGUucGFyYW1zLmlkKTtcbiAgICAgICAgdGhpcy5mZXRjaEludm9pY2VJdGVtcyh0aGlzLiRyb3V0ZS5wYXJhbXMuaWQpO1xuICAgICAgICB0aGlzLmxpc3RlbkZvckVzY2FwZSgpO1xuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICBpbml0aWFsaXplU2VsZWN0KCkge1xuICAgICAgICAgICAgJCgnZmllbGRzZXQgPiAuY3VzdG9tX3NlbGVjdCcpLmhpZGUoKTtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgICQoJyNzdXBwbGllcl9pZCcpLnNlbGVjdG1lbnUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdDogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnN1cHBsaWVyID0gdWkuaXRlbS52YWx1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoJyNzdXBwbGllcl9pZCcpLnNlbGVjdG1lbnUoJ29wZW4nKTtcblxuICAgICAgICB9LFxuICAgICAgICBmZXRjaERhdGEoaWQpIHtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgIHNlbGYuJHJvb3QubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBheGlvcy5nZXQoXCIvYXBwL2NvbnRyb2xsZXJzL2ludm9pY2VzLnBocD9pZD1cIiArIGlkKVxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5udW1iZXIgPSByZXNwb25zZS5kYXRhLmF0dHJpYnV0ZXMubnVtYmVyO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGUgPSByZXNwb25zZS5kYXRhLmF0dHJpYnV0ZXMuZGF0ZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdXBwbGllciA9IHJlc3BvbnNlLmRhdGEuYXR0cmlidXRlcy5zdXBwbGllcjtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50b3RhbCA9IHJlc3BvbnNlLmRhdGEuYXR0cmlidXRlcy50b3RhbDtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi4kcm9vdC5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICAgIGZldGNoSW52b2ljZUl0ZW1zKGlkKSB7XG4gICAgICAgICAgICBheGlvcy5nZXQoJy9hcHAvY29udHJvbGxlcnMvaW52b2ljZV9pdGVtcy5waHA/aWQ9JyArIGlkKVxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbnZvaWNlX2l0ZW1zID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgdmFsaWRhdGVGb3JtKCkge1xuICAgICAgICAgICAgLy8gdmFsaWRhdGVGb3JtKCk7XG4gICAgICAgICAgICBsZXQgbnVtYmVyID0gJChcIiNudW1iZXJcIikudmFsKCk7XG4gICAgICAgICAgICBsZXQgc3VwcGxpZXIgPSAkKCcjc3VwcGxpZXInKTtcbiAgICAgICAgICAgIGxldCBkYXRlID0gJCgnI2RhdGUnKTtcblxuICAgICAgICAgICAgLy8gRW5zdXJlcyBpbnB1dFxuICAgICAgICAgICAgaWYgKG51bWJlci5sZW5ndGggPD0gMCkge1xuICAgICAgICAgICAgICAgIGxldCB2YWxpZGF0aW9uID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgbGV0IG1lc3NhZ2UgPSAnUGxlYXNlIGVudGVyIGFuIGludm9pY2UgbnVtYmVyJztcbiAgICAgICAgICAgICAgICBsZXQgcHJvYmxlbWF0aWNfaW5wdXQgPSAnbnVtYmVyJztcbiAgICAgICAgICAgICAgICByZXR1cm4ge3ZhbGlkYXRpb24sIG1lc3NhZ2UsIHByb2JsZW1hdGljX2lucHV0fTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGxldCB2YWxpZGF0aW9uID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBsZXQgcHJvYmxlbWF0aWNfaW5wdXQsIG1lc3NhZ2UgPSBudWxsO1xuICAgICAgICAgICAgICAgIHJldHVybiB7dmFsaWRhdGlvbiwgbWVzc2FnZSwgcHJvYmxlbWF0aWNfaW5wdXR9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB1cGRhdGVJbnZvaWNlKCkge1xuICAgICAgICAgICAgbGV0IGl0ZW1zX3RvX2RlbGV0ZSA9IHRoaXMuaW52b2ljZV9pdGVtc190b19kZWxldGU7XG4gICAgICAgICAgICAvLyBJZiB0aGVyZSBhcmUgaXRlbXMgdGhhdCByZXF1aXJlIHJlbW92aW5nIGZyb20gdGhlIEludm9pY2U6XG4gICAgICAgICAgICBpZiAoaXRlbXNfdG9fZGVsZXRlICYmIGl0ZW1zX3RvX2RlbGV0ZS5sZW5ndGggIT09IDApIHtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGl0ZW1zX3RvX2RlbGV0ZS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBheGlvcy5wYXRjaCgnL2FwcC9jb250cm9sbGVycy9pbnZvaWNlX2l0ZW1zLnBocCcsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiBpdGVtc190b19kZWxldGVbaV0ucXVhbnRpdHksXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogaXRlbXNfdG9fZGVsZXRlW2ldLmRlc2NyaXB0aW9uLFxuICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgbGV0IGZvcm0gPSB0aGlzLnZhbGlkYXRlRm9ybSgpO1xuICAgICAgICAgICAgICAgIGlmIChmb3JtLnZhbGlkYXRpb24gPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gQ3JlYXRlcyB0aGUgaW52b2ljZVxuICAgICAgICAgICAgICAgICAgICBheGlvcy5wYXRjaCgnJywgXCIvYXBwL2NvbnRyb2xsZXJzL2ludm9pY2VzLnBocFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogdGhpcy4kcm91dGUucGFyYW1zLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgbnVtYmVyOiB0aGlzLm51bWJlcixcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGU6IHRoaXMuZGF0ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1cHBsaWVyOiB0aGlzLmZpbmRTdXBwbGllcigpLFxuICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHsgLy8gVXBkYXRpbmcgdGhlIGludm9pY2UgSXRlbXNcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEZpcnN0LCBjaGVja3Mgd2hldGhlciB0aGUgaW52b2ljZV9pdGVtIGluIHF1ZXN0aW9uIGlzIGEgbmV3IGVudHJ5LCBvciBhbiB1cGRhdGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaW52b2ljZV9pdGVtcyA9IHRoaXMuaW52b2ljZV9pdGVtcztcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IG4gPSAwOyBuIDwgaW52b2ljZV9pdGVtcy5sZW5ndGg7IG4rKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgc2VsZiA9IGludm9pY2VfaXRlbXNbbl07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChPYmplY3Qua2V5cyhzZWxmKS5sZW5ndGggPT09IDAgJiYgc2VsZi5jb25zdHJ1Y3RvciA9PT0gT2JqZWN0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zLnBhdGNoKCcuLi9hcHAvY29udHJvbGxlcnMvaW52b2ljZV9pdGVtcy5waHAnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IGludm9pY2VfaXRlbXNbbl0uaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGludm9pY2VfaXRlbXNbbl0uZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnk6IHRoaXMuZmluZENhdGVnb3J5KGludm9pY2VfaXRlbXNbbl0uY2F0ZWdvcnkpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiBpbnZvaWNlX2l0ZW1zW25dLnF1YW50aXR5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuaXRfcHJpY2U6IGludm9pY2VfaXRlbXNbbl0udW5pdF9wcmljZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnZvaWNlX2lkOiB0aGlzLiRyb3V0ZS5wYXJhbXMuaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVkaXRhYmxlID0gbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSAnSW52b2ljZSBVcGRhdGVkIFN1Y2Nlc3NmdWxseScsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5zdGF0ZSA9ICdzdWNjZXNzJ1xuICAgICAgICAgICAgICAgICAgICApLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSAnVGhlcmUgd2FzIGEgcHJvYmxlbSB1cGRhdGluZyB0aGUgaW52b2ljZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5zdGF0ZSA9ICdlcnJvcic7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyMnICsgZm9ybS5wcm9ibGVtYXRpY19pbnB1dCkuYWZ0ZXIoJzxwIGNsYXNzPVwiZXJyb3JcIj4nICsgZm9ybS5tZXNzYWdlICsgJzwvcD4nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLmludm9pY2VfaXRlbXNfdG9fZGVsZXRlID0gW107XG4gICAgICAgIH0sXG4gICAgICAgIGNhbGN1bGF0ZVRvdGFsKCkge1xuICAgICAgICAgICAgaWYgKHRoaXMuZWRpdGFibGUgJiYgdGhpcy5pbnZvaWNlX2l0ZW1zKSB7XG4gICAgICAgICAgICAgICAgbGV0IHRvdGFsID0gMDtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBuID0gMDsgbiA8IHRoaXMuaW52b2ljZV9pdGVtcy5sZW5ndGg7IG4rKykge1xuICAgICAgICAgICAgICAgICAgICB0b3RhbCArPSAodGhpcy5pbnZvaWNlX2l0ZW1zW25dLnF1YW50aXR5ICogdGhpcy5pbnZvaWNlX2l0ZW1zW25dLnVuaXRfcHJpY2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdG90YWwudG9GaXhlZCgyKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudG90YWw7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGluaXRpYWxpemVEYXRlcGlja2VyKCkge1xuICAgICAgICAgICAgbGV0IGQgPSAkKFwiI2RhdGVwaWNrZXJcIik7XG4gICAgICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICAgICAgICBkLmRhdGVwaWNrZXIoe1xuICAgICAgICAgICAgICAgIG9uU2VsZWN0OiBmdW5jdGlvbiAoc2VsZWN0ZWREYXRlLCBkYXRlUGlja2VyKSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuZGF0ZSA9IHNlbGVjdGVkRGF0ZTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGRhdGVGb3JtYXQ6ICd5eS1tbS1kZCdcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gZC5kYXRlcGlja2VyKCk7XG4gICAgICAgICAgICBkLmRhdGVwaWNrZXIoXCJzaG93XCIpO1xuICAgICAgICB9LFxuICAgICAgICBmaW5kQ2F0ZWdvcnkoY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgIGxldCBhbGxfY2F0ZWdvcmllcyA9IHRoaXMuJHJvb3QuY2F0ZWdvcmllcztcblxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxfY2F0ZWdvcmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmIChhbGxfY2F0ZWdvcmllc1tpXS5pZCA9PT0gY2F0ZWdvcnkgfHwgYWxsX2NhdGVnb3JpZXNbaV0ubmFtZSA9PT0gY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFsbF9jYXRlZ29yaWVzW2ldLmlkO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmluZFN1cHBsaWVyKCkge1xuICAgICAgICAgICAgbGV0IHN1cHBsaWVyID0gdGhpcy5zdXBwbGllcjtcbiAgICAgICAgICAgIGxldCBhbGxfc3VwcGxpZXJzID0gdGhpcy4kcGFyZW50LnN1cHBsaWVycztcblxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxfc3VwcGxpZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKGFsbF9zdXBwbGllcnNbaV0uaWQgPT09IHN1cHBsaWVyIHx8IGFsbF9zdXBwbGllcnNbaV0ubmFtZSA9PT0gc3VwcGxpZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFsbF9zdXBwbGllcnNbaV0uaWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBsaXN0ZW5Gb3JFc2NhcGUoKSB7XG4gICAgICAgICAgICBsZXQgdm0gPSB0aGlzO1xuICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgLy8gSWYgRVNDIGFycm93IHdhcyBwcmVzc2VkLi4uXG4gICAgICAgICAgICAgICAgaWYgKHZtLiRyb3V0ZS5uYW1lID09PSAnaW52b2ljZScgJiYgZXZlbnQua2V5Q29kZSA9PT0gMjcpIHtcbiAgICAgICAgICAgICAgICAgICAgdm0uJHJvdXRlci5nbygtMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGJlZm9yZVJvdXRlVXBkYXRlKHRvLCBmcm9tLCBuZXh0KSB7XG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHRvKTtcbiAgICAgICAgbmV4dCgpO1xuICAgIH1cbn07XG5tb2R1bGUuZXhwb3J0cyA9IHNpbmdsZV9pbnZvaWNlOyIsImNvbnN0IGludm9pY2VfaXRlbXNfZm9yX2ludm9pY2UgPSB7XG4gICAgdGVtcGxhdGU6ICcjaW52b2ljZV9pdGVtc19mb3JfaW52b2ljZV90ZW1wbGF0ZSdcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGludm9pY2VfaXRlbXNfZm9yX2ludm9pY2U7IiwiY29uc3QgaW52b2ljZV9pdGVtc19mb3JfaW52b2ljZV9mb3JtID0ge1xuICAgIHRlbXBsYXRlOiAnI2ludm9pY2VfaXRlbXNfZm9yX2ludm9pY2VfZm9ybV90ZW1wbGF0ZScsXG4gICAgY29tcHV0ZWQ6IHtcbiAgICAgICAgaW52b2ljZV9pdGVtcygpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLiRwYXJlbnQuaW52b2ljZV9pdGVtcztcbiAgICAgICAgfSxcbiAgICAgICAgaW52b2ljZV9pdGVtc190b19kZWxldGUoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy4kcGFyZW50Lmludm9pY2VfaXRlbXNfdG9fZGVsZXRlO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIGluaXRpYWxpemVTZWxlY3QoaXRlbSwgaW5kZXgpIHtcbiAgICAgICAgICAgIGxldCBzZWxlY3QgPSAkKCcjY2F0ZWdvcnlfc2VsZWN0XycgKyBpbmRleCk7XG4gICAgICAgICAgICAkKCcjY3VzdG9tX3NlbGVjdF8nICsgaW5kZXgpLmhpZGUoKTtcbiAgICAgICAgICAgIGxldCBzZWxmID0gaXRlbTtcbiAgICAgICAgICAgIHNlbGVjdC5zZWxlY3RtZW51KHtcbiAgICAgICAgICAgICAgICBzZWxlY3Q6IGZ1bmN0aW9uIChldmVudCwgdWkpIHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5jYXRlZ29yeSA9IHVpLml0ZW0udmFsdWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBzZWxlY3Quc2VsZWN0bWVudSgnb3BlbicpO1xuICAgICAgICB9LFxuICAgICAgICBhZGRGaWVsZCgpIHtcbiAgICAgICAgICAgIC8vIFRoZSBiZWxvdyBjb2RlIGVuc3VyZXMgdGhhdCB0aGUgbGFzdCBlbnRyeSBpcyBub3QgYmxhbmsgYmVmb3JlIHJlbmRlcmluZyBhbm90aGVyIGZvcm1cbiAgICAgICAgICAgIGxldCBhcnJheSA9IHRoaXMuaW52b2ljZV9pdGVtcztcbiAgICAgICAgICAgIGxldCBsYXN0X29iamVjdCA9IGFycmF5W2FycmF5Lmxlbmd0aCAtIDFdO1xuXG4gICAgICAgICAgICBpZiAoYXJyYXkubGVuZ3RoID4gMCAmJiB0aGlzLmVtcHR5RmllbGRDaGVjayhsYXN0X29iamVjdCkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5tZXNzYWdlID0gXCJQbGVhc2UgZmlsbCBvdXQgdGhlIHByZXZpb3VzIGFkZGl0aW9uIGJlZm9yZSBhZGRpbmcgYSBuZXcgZmllbGRcIjtcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5zdGF0ZSA9ICdlcnJvcic7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBsZXQgdGFibGUgPSAkKCcjaW52b2ljZScpLmZpbmQoXCJ0Ym9keVwiKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuaW52b2ljZV9pdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5OiBudWxsLFxuICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgdW5pdF9wcmljZTogbnVsbCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcmVtb3ZlRmllbGQoaW5kZXgsIGl0ZW0pIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmVtcHR5RmllbGRDaGVjayhpdGVtKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5pbnZvaWNlX2l0ZW1zLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5pbnZvaWNlX2l0ZW1zLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50Lmludm9pY2VfaXRlbXNfdG9fZGVsZXRlLnB1c2goaXRlbSk7XG4gICAgICAgICAgICAgICAgdGhpcy51bmRvUmVtb3ZlRmllbGQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdW5kb1JlbW92ZUZpZWxkKCkge1xuICAgICAgICAgICAgLy8gQSBtZXNzeSBBRiBmdW5jdGlvbiB0aGF0IGp1c3QgcmVzZXRzIGF0bSAoRnV0dXJlIHBsYW4gaXMgdG8gc2F2ZSBhIGhpc3RvcnkgYW5kIGluZGl2aWR1YWxseSB1bmRvIHJlbW92YWxzKVxuICAgICAgICAgICAgaWYgKCQoJyNyZXNldCcpLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIGxldCByZXNldCA9ICc8YnV0dG9uIGlkPVwicmVzZXRcIj5SZXNldDwvYnV0dG9uPic7XG4gICAgICAgICAgICAgICAgJCgnLmFkZF9maWVsZHNfX2J1dHRvbicpLmFmdGVyKHJlc2V0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICQoJyNyZXNldCcpLmNsaWNrKChldmVudCkgPT4ge1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgbGV0IGluZGV4ID0gdGhpcy4kcGFyZW50Lmludm9pY2VfaXRlbXNfdG9fZGVsZXRlLmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAgICAgbGV0IG9iamVjdCA9IHRoaXMuJHBhcmVudC5pbnZvaWNlX2l0ZW1zX3RvX2RlbGV0ZVtpbmRleF07XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coaW5kZXgpO1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKG9iamVjdCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50Lmludm9pY2VfaXRlbXMucHVzaChvYmplY3QpO1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5pbnZvaWNlX2l0ZW1zX3RvX2RlbGV0ZS5zcGxpY2UoaW5kZXgsIDEpO1xuXG4gICAgICAgICAgICAgICAgLy8gSGlkZSB0aGUgYnV0dG9uIGlmIHRoZXJlJ3Mgbm8gaXRlbXMgdG8gZGVsZXRlIGxlZnRcbiAgICAgICAgICAgICAgICBpZiAodGhpcy4kcGFyZW50Lmludm9pY2VfaXRlbXNfdG9fZGVsZXRlLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAkKCcjcmVzZXQnKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgZW1wdHlGaWVsZENoZWNrKG9iaikge1xuICAgICAgICAgICAgbGV0IHZhbHVlcyA9IE9iamVjdC52YWx1ZXMob2JqKTtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdmFsdWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlc1tpXSAhPSBudWxsICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gT2JqZWN0KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5tb2R1bGUuZXhwb3J0cyA9IGludm9pY2VfaXRlbXNfZm9yX2ludm9pY2VfZm9ybTsiLCJjb25zdCBpbnZvaWNlc19pbmRleCA9IHtcbiAgICBuYW1lOiAnSW52b2ljZXNfaW5kZXgnLFxuICAgIHRlbXBsYXRlOiAnI2ludm9pY2VzX2luZGV4X3RlbXBsYXRlJyxcbiAgICBkYXRhKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgaW52b2ljZXM6IFtcbiAgICAgICAgICAgICAgICAvLyBBbiBhcnJheSBvZiBvYmplY3RzIGNvbGxlY3RlZCBvbiBjcmVhdGlvbiB3aXRoIGZldGNoRGF0YSgpXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgcmVzdWx0c19wZXJfcGFnZTogMTUsXG4gICAgICAgICAgICBwYWdlX251bWJlcjogMSxcbiAgICAgICAgICAgIHNldHRpbmdzX2VuYWJsZWQ6IG51bGxcbiAgICAgICAgfTtcbiAgICB9LFxuICAgIG1vdW50ZWQoKSB7XG4gICAgICAgIHRoaXMuJHJvb3QuY2hlY2tGb3JMb2dpbigpO1xuICAgICAgICB0aGlzLmZldGNoRGF0YSgpO1xuICAgIH0sXG4gICAgY29tcG9uZW50czoge1xuICAgICAgICB0YWJsZV9yb3c6IHtcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnI3RhYmxlX3Jvd190ZW1wbGF0ZScsXG4gICAgICAgICAgICBuYW1lOiAnSW52b2ljZScsXG4gICAgICAgICAgICBwcm9wczogW1xuICAgICAgICAgICAgICAgICdpZCcsXG4gICAgICAgICAgICAgICAgJ251bWJlcicsXG4gICAgICAgICAgICAgICAgJ3N1cHBsaWVyJyxcbiAgICAgICAgICAgICAgICAnY2F0ZWdvcnknLFxuICAgICAgICAgICAgICAgICdkYXRlJyxcbiAgICAgICAgICAgICAgICAndG90YWwnXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGF0YSgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBkZWxldGVfY29uZmlybWF0aW9uOiBudWxsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjb21wdXRlZDoge1xuXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbWV0aG9kczoge1xuICAgICAgICAgICAgICAgIGRlbGV0ZUludm9pY2UoaWQpIHsgIC8vIDw9PT0gVGhlcmUgaXMgYSBjb21tZW50IHJlZmVycmluZyB0byB0aGlzIGluIF92YXJpYWJsZXMuc2Nzc1xuICAgICAgICAgICAgICAgICAgICBsZXQgaW52b2ljZXMgPSB0aGlzLiRwYXJlbnQuaW52b2ljZXM7XG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgaW52b2ljZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbnZvaWNlc1tpXS5pZCA9PT0gdGhpcy5pZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5pbnZvaWNlcy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXhpb3MuZGVsZXRlKCcvYXBwL2NvbnRyb2xsZXJzL2ludm9pY2VzLnBocCcsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGV0ZV9jb25maXJtYXRpb24gPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2UubWVzc2FnZSA9ICdJbnZvaWNlIGRlbGV0ZWQgc3VjY2Vzc2Z1bGx5JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLnN0YXRlID0gJ3N1Y2Nlc3MnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLm1lc3NhZ2UgPSAnVGhlcmUgd2FzIGFuIGVycm9yIGRlbGV0aW5nIHRoZSBpbnZvaWNlJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2Uuc3RhdGUgPSAnZXJyb3InO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZGVsZXRlQ29uZmlybWF0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGV0ZV9jb25maXJtYXRpb24gPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICAvLyBjaGFuZ2VQYWdlKCA6c3RyaW5nID0gJ3ByZXZpb3VzJyB8fCAnbmV4dCcpXG4gICAgICAgIGNoYW5nZVBhZ2UoZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBkaXJlY3Rpb24udG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIGlmIChkaXJlY3Rpb24gPT09ICdwcmV2aW91cycgJiYgdGhpcy5wYWdlX251bWJlciA+IDEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnBhZ2VfbnVtYmVyIC09IDE7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGRpcmVjdGlvbiA9PT0gJ25leHQnICYmIHRoaXMuY29tcHV0ZWRBcnJheS5sZW5ndGggPj0gdGhpcy5yZXN1bHRzX3Blcl9wYWdlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wYWdlX251bWJlciArPSAxO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICAvLyBTZW5kcyBhIEdFVCByZXF1ZXN0IGZvciBhbGwgaW52b2ljZXMgYW5kIGFkZHMgdGhlbSB0byB0aGUgY29tcG9uZW50J3MgaW52b2ljZXMgYXJyYXlcbiAgICAgICAgLy9cbiAgICAgICAgLy8gSU1QT1JUQU5UISFcbiAgICAgICAgLy9cbiAgICAgICAgLy8gRGVsYXllZCB0byB2aXN1YWxpemUgbG9hZGluZyBpY29uXG4gICAgICAgIGZldGNoRGF0YSgpIHtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgIHNlbGYuJHJvb3QubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICBheGlvcy5nZXQoXCIvYXBwL2NvbnRyb2xsZXJzL2ludm9pY2VzLnBocFwiKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICBzZWxmLmludm9pY2VzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICBzZWxmLiRyb290LmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgLy8gVG9nZ2xlcyB2aXNpYmlsaXR5IG9mIFwiT3B0aW9uc1wiIGRpdiB0aGF0IGNvbnRhaW5zIGJ1dHRvbnMgdG8gYW4gRWRpdCBmb3JtIGFuZCBhIERlc3Ryb3kgYWN0aW9uIGFjY29yZGluZ2x5XG4gICAgICAgIHRvZ2dsZVNldHRpbmdzKCkge1xuICAgICAgICAgICAgbGV0IGUgPSB0aGlzLnNldHRpbmdzX2VuYWJsZWQ7XG4gICAgICAgICAgICB0aGlzLnNldHRpbmdzX2VuYWJsZWQgPSAoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmIChlID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pKCk7XG4gICAgICAgIH0sXG4gICAgICAgIHVwZGF0ZVJlc3VsdHNQZXJQYWdlQnkoY291bnQpIHtcbiAgICAgICAgICAgIGxldCBidXR0b25zID0gJCgnLnNldHRpbmdzX19jb250cm9sID4gZGl2ID4gYnV0dG9uJyk7XG4gICAgICAgICAgICBidXR0b25zLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgIGxldCByZXN1bHQgPSAkKFwiLnNldHRpbmdzX19jb250cm9sID4gZGl2ID4gYnV0dG9uOmNvbnRhaW5zKFwiICsgY291bnQgKyBcIilcIik7XG4gICAgICAgICAgICByZXN1bHQudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgdGhpcy5yZXN1bHRzX3Blcl9wYWdlID0gY291bnQ7XG4gICAgICAgIH0sXG4gICAgfSxcbiAgICBjb21wdXRlZDoge1xuICAgICAgICBjb21wdXRlZEFycmF5KCkge1xuICAgICAgICAgICAgbGV0IGZpcnN0ID0gKHRoaXMucmVzdWx0c19wZXJfcGFnZSAqICh0aGlzLnBhZ2VfbnVtYmVyIC0gMSkpO1xuICAgICAgICAgICAgbGV0IGxhc3QgPSB0aGlzLnJlc3VsdHNfcGVyX3BhZ2UgKiB0aGlzLnBhZ2VfbnVtYmVyO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaW52b2ljZXMuc2xpY2UoZmlyc3QsIGxhc3QpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBiZWZvcmVSb3V0ZVVwZGF0ZSh0bywgZnJvbSwgbmV4dCkge1xuICAgICAgICBuZXh0KCk7XG4gICAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gaW52b2ljZXNfaW5kZXg7IiwibGV0IGludm9pY2VfaXRlbXNfZm9yX2ludm9pY2VfZm9ybSA9IHJlcXVpcmUoJy4vaW52b2ljZV9pdGVtc19mb3JfaW52b2ljZV9mb3JtJyk7XG5jb25zdCBuZXdfaW52b2ljZSA9IHtcbiAgICB0ZW1wbGF0ZTogJyNuZXdfaW52b2ljZV90ZW1wbGF0ZScsXG4gICAgbmFtZTogJ25ld19pbnZvaWNlJyxcbiAgICBkYXRhKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbnVtYmVyOiBudWxsLFxuICAgICAgICAgICAgZGF0ZTogbnVsbCxcbiAgICAgICAgICAgIHN1cHBsaWVyOiBudWxsLFxuICAgICAgICAgICAgaW52b2ljZV9pdGVtczogW3t9XSxcbiAgICAgICAgICAgIHRvdGFsOiBudWxsLFxuICAgICAgICAgICAgZWRpdGFibGU6IHRoaXMuJHJvdXRlLnBhcmFtcy5lZGl0YWJsZVxuICAgICAgICB9O1xuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICBjYWxjdWxhdGVUb3RhbCgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmVkaXRhYmxlKSB7XG4gICAgICAgICAgICAgICAgbGV0IHRvdGFsID0gMDtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBuID0gMDsgbiA8IHRoaXMuaW52b2ljZV9pdGVtcy5sZW5ndGg7IG4rKykge1xuICAgICAgICAgICAgICAgICAgICB0b3RhbCArPSAodGhpcy5pbnZvaWNlX2l0ZW1zW25dLnF1YW50aXR5ICogdGhpcy5pbnZvaWNlX2l0ZW1zW25dLnVuaXRfcHJpY2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdG90YWw7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnRvdGFsO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbml0aWFsaXplRGF0ZXBpY2tlcigpIHtcbiAgICAgICAgICAgIGxldCBkID0gJChcIiNkYXRlcGlja2VyXCIpO1xuICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgZC5kYXRlcGlja2VyKHtcbiAgICAgICAgICAgICAgICBvblNlbGVjdDogZnVuY3Rpb24gKHNlbGVjdGVkRGF0ZSwgZGF0ZVBpY2tlcikge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLmRhdGUgPSBzZWxlY3RlZERhdGU7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBkYXRlRm9ybWF0OiAneXktbW0tZGQnXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGQuZGF0ZXBpY2tlcihcInNob3dcIik7XG4gICAgICAgIH0sXG4gICAgICAgIGZpbmRTdXBwbGllcigpIHtcbiAgICAgICAgICAgIGxldCBzdXBwbGllciA9IHRoaXMuc3VwcGxpZXI7XG4gICAgICAgICAgICBsZXQgYWxsX3N1cHBsaWVycyA9IHRoaXMuJHJvb3Quc3VwcGxpZXJzO1xuXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFsbF9zdXBwbGllcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAoYWxsX3N1cHBsaWVyc1tpXS5pZCA9PT0gc3VwcGxpZXIgfHwgYWxsX3N1cHBsaWVyc1tpXS5uYW1lID09PSBzdXBwbGllcikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYWxsX3N1cHBsaWVyc1tpXS5pZDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZpbmRDYXRlZ29yeShjYXRlZ29yeSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coY2F0ZWdvcnkpO1xuICAgICAgICAgICAgaWYgKGNhdGVnb3J5ID09PSAnQm90YW5pY2FsJykge1xuICAgICAgICAgICAgICAgIHJldHVybiAxO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChjYXRlZ29yeSA9PT0gJ1JlY2VwdGFjbGUnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIDI7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNhdGVnb3J5ID09PSAnQ2hlbWljYWwnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIDM7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNhdGVnb3J5ID09PSAnSGFyZHdhcmUnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIDQ7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGxldCBhbGxfY2F0ZWdvcmllcyA9IHRoaXMuJHJvb3QuY2F0ZWdvcmllcztcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFsbF9jYXRlZ29yaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChhbGxfY2F0ZWdvcmllc1tpXS5pZCA9PT0gY2F0ZWdvcnkgfHwgYWxsX2NhdGVnb3JpZXNbaV0uYXR0cmlidXRlcy5uYW1lID09PSBjYXRlZ29yeSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFsbF9jYXRlZ29yaWVzW2ldLmlkO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbml0aWFsaXplU2VsZWN0KCkge1xuICAgICAgICAgICAgJCgnZmllbGRzZXQgPiAuY3VzdG9tX3NlbGVjdCcpLmhpZGUoKTtcbiAgICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgICAgICQoJyNzdXBwbGllcl9pZCcpLnNlbGVjdG1lbnUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdDogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnN1cHBsaWVyID0gdWkuaXRlbS52YWx1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoJyNzdXBwbGllcl9pZCcpLnNlbGVjdG1lbnUoJ29wZW4nKTtcblxuICAgICAgICB9LFxuICAgICAgICBlbXB0eUZpZWxkQ2hlY2soKSB7XG4gICAgICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuaW52b2ljZV9pdGVtcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGxldCBvYmogPSBzZWxmLmludm9pY2VfaXRlbXNbaV07XG4gICAgICAgICAgICAgICAgaWYgKE9iamVjdC5rZXlzKG9iaikubGVuZ3RoID09PSAwICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gT2JqZWN0KSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuaW52b2ljZV9pdGVtcy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgdmFsdWUgaW4gb2JqKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAob2JqW3ZhbHVlXSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuaW52b2ljZV9pdGVtcy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZhbGlkYXRlKCkge1xuICAgICAgICAgICAgdGhpcy5lbXB0eUZpZWxkQ2hlY2soKTtcbiAgICAgICAgICAgIC8vIEludm9pY2UgRm9ybSBpcyBlbXB0eVxuICAgICAgICAgICAgaWYgKCF0aGlzLmRhdGUgfHwgIXRoaXMubnVtYmVyIHx8ICF0aGlzLnN1cHBsaWVyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICdFcnJvcjogUGxlYXNlIGZpbGwgb3V0IGFsbCBmaWVsZHMnO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmludm9pY2VfaXRlbXMubGVuZ3RoIDw9IDApIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJ0Vycm9yOiBQbGVhc2UgYWRkIGF0IGxlYXN0IG9uZSBpdGVtIHRvIHRoZSBpbnZvaWNlJztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHN1Ym1pdCgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnZhbGlkYXRlKCkgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBheGlvcy5wb3N0KCcuLi9hcHAvY29udHJvbGxlcnMvaW52b2ljZXMucGhwJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZTogdGhpcy5kYXRlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbnVtYmVyOiB0aGlzLm51bWJlcixcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1cHBsaWVyX2lkOiB0aGlzLmZpbmRTdXBwbGllcigpLFxuICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgaW52b2ljZV9pdGVtcyA9IHRoaXMuaW52b2ljZV9pdGVtcztcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IG4gPSAwOyBuIDwgaW52b2ljZV9pdGVtcy5sZW5ndGg7IG4rKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzZWxmID0gaW52b2ljZV9pdGVtc1tuXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMoc2VsZikubGVuZ3RoID09PSAwICYmIHNlbGYuY29uc3RydWN0b3IgPT09IE9iamVjdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBheGlvcy5wb3N0KCcuLi9hcHAvY29udHJvbGxlcnMvaW52b2ljZV9pdGVtcy5waHAnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnZvaWNlX2lkOiByZXNwb25zZS5kYXRhLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGludm9pY2VfaXRlbXNbbl0uZGVzY3JpcHRpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeV9pZDogdGhpcy5maW5kQ2F0ZWdvcnkoaW52b2ljZV9pdGVtc1tuXS5jYXRlZ29yeSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogaW52b2ljZV9pdGVtc1tuXS5xdWFudGl0eSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuaXRfcHJpY2U6IGludm9pY2VfaXRlbXNbbl0udW5pdF9wcmljZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKCcvaW52b2ljZXMvJyArIHJlc3BvbnNlLmRhdGEuaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2UubWVzc2FnZSA9ICdJbnZvaWNlIGNyZWF0ZWQgc3VjY2Vzc2Z1bGx5JztcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLnN0YXRlID0gJ3N1Y2Nlc3MnO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5tZXNzYWdlID0gdGhpcy52YWxpZGF0ZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMuJHJvb3Qubm90aWNlLnN0YXRlID0gJ2Vycm9yJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG4gICAgY29tcG9uZW50czoge1xuICAgICAgICBpbnZvaWNlX2l0ZW1zX2Zvcl9pbnZvaWNlX2Zvcm1cbiAgICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IG5ld19pbnZvaWNlOyIsImNvbnN0IGNhcmRzID0ge1xuICAgIG5hbWU6ICdjYXJkcycsXG4gICAgZGF0YSgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGNhcmRzOiB0aGlzLiRyb290LnBhZ2VzXG4gICAgICAgIH07XG4gICAgfSxcbiAgICB0ZW1wbGF0ZTogJyNjYXJkcydcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gY2FyZHM7IiwibGV0IGNhcmRzID0gcmVxdWlyZSgnLi9jYXJkcy5qcycpO1xuXG5jb25zdCBob21lID0ge1xuICAgIG5hbWU6ICdob21lJyxcbiAgICB0ZW1wbGF0ZTogJyNob21lJyxcbiAgICBjb21wb25lbnRzOiB7XG4gICAgICAgIGNhcmRzXG4gICAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gaG9tZTtcbiIsImNvbnN0IGxvZ2luID0ge1xuICAgIHRlbXBsYXRlOiAnI2xvZ2luX3RlbXBsYXRlJyxcbiAgICBkYXRhKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdXNlcm5hbWU6IG51bGwsXG4gICAgICAgICAgICBwYXNzd29yZDogbnVsbFxuICAgICAgICB9O1xuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICBzdWJtaXQoKSB7XG4gICAgICAgICAgICBheGlvcy5wb3N0KCcvYXBwL2xpYi9sb2dpbi5waHAnLCB7XG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHRoaXMudXNlcm5hbWUsXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHRoaXMucGFzc3dvcmRcbiAgICAgICAgICAgIH0pLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5ub3RpY2UubWVzc2FnZSA9IHJlc3BvbnNlLmRhdGEubWVzc2FnZTtcbiAgICAgICAgICAgICAgICB0aGlzLiRyb290Lm5vdGljZS5zdGF0ZSA9IHJlc3BvbnNlLmRhdGEuc3RhdHVzID09PSAnYXV0aGVudGljYXRlZCcgPyAnc3VjY2VzcycgOiAnZXJyb3InO1xuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhLnN0YXR1cyA9PT0gJ2F1dGhlbnRpY2F0ZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvb3QuYXV0aCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKCcvJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGxvZ2luOyIsImNvbnN0IG5hdmlnYXRpb24gPSB7XG4gICAgbmFtZTogJ25hdmlnYXRpb24nLFxuICAgIGRhdGEoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBwYWdlczogdGhpcy4kcm9vdC5wYWdlcyxcbiAgICAgICAgfTtcbiAgICB9LFxuICAgIHRlbXBsYXRlOiAnI25hdmlnYXRpb24nLFxuICAgIG1vdW50ZWQoKSB7XG4gICAgICAgIHRoaXMuY2hlY2tGb3JEZWZhdWx0SW1hZ2VzKCk7XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIGNoZWNrRm9yRGVmYXVsdEltYWdlcygpIHtcbiAgICAgICAgICAgIGF4aW9zLmdldCgnL2FwcC9jb250cm9sbGVycy9pbnZlbnRvcnkucGhwJykudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpdGVtcyBvZiByZXNwb25zZS5kYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChpdGVtc1snYXR0cmlidXRlcyddWydpbWFnZV9wYXRoJ10gPT09ICdodHRwOi8vcGxhY2Vob2xkLml0LzMwMC8zMDAnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyb290LmludmVudG9yeV9pdGVtc193aXRoX2RlZmF1bHRfaW1hZ2UucHVzaChpdGVtcyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBuYXZpZ2F0aW9uOyIsIi8vIFNpdGVcbmxldCBob21lX3BhZ2UgPSByZXF1aXJlKCcuL2xheW91dC9ob21lJyk7XG5sZXQgbG9naW4gPSByZXF1aXJlKCcuL2xheW91dC9sb2dpbicpO1xuXG4vLyBsZXQgcHJvZHVjdHMgPSByZXF1aXJlKCcuL3Byb2R1Y3RzL3Byb2R1Y3RzJyk7XG5cbi8vIEludm9pY2VzXG5sZXQgc2luZ2xlX2ludm9pY2UgPSByZXF1aXJlKCcuL2ludm9pY2VzL2ludm9pY2UnKTtcbmxldCBpbnZvaWNlc19pbmRleCA9IHJlcXVpcmUoJy4vaW52b2ljZXMvaW52b2ljZXNfaW5kZXgnKTtcbmxldCBuZXdfaW52b2ljZSA9IHJlcXVpcmUoJy4vaW52b2ljZXMvbmV3X2ludm9pY2UnKTtcblxuLy8gSW52ZW50b3J5XG5sZXQgaW52ZW50b3J5X2luZGV4ID0gcmVxdWlyZSgnLi9pbnZlbnRvcnkvaW52ZW50b3J5X2luZGV4Jyk7XG5sZXQgc2luZ2xlX2ludmVudG9yeV9pdGVtID0gcmVxdWlyZSgnLi9pbnZlbnRvcnkvaW52ZW50b3J5X2l0ZW0nKTtcbmxldCBuZXdfaW52ZW50b3J5X2l0ZW0gPSByZXF1aXJlKCcuL2ludmVudG9yeS9uZXdfaW52ZW50b3J5X2l0ZW0nKTtcblxuLy8gRGFzaGJvYXJkXG5cbmxldCBkZWZhdWx0X2ltYWdlc190b19jaGFuZ2UgPSByZXF1aXJlKCcuL2Rhc2hib2FyZC9kZWZhdWx0X2ltYWdlc190b19jaGFuZ2UnKTtcblxuLy8gUm91dGVzXG5jb25zdCByb3V0ZXMgPSBbXG4gICAge3BhdGg6ICcvJywgY29tcG9uZW50OiBob21lX3BhZ2V9LFxuICAgIHtcbiAgICAgICAgcGF0aDogJy9sb2dpbicsXG4gICAgICAgIGNvbXBvbmVudDogbG9naW5cbiAgICB9LFxuICAgIC8vIEludm9pY2VzXG4gICAge3BhdGg6ICcvaW52b2ljZXMnLCBjb21wb25lbnQ6IGludm9pY2VzX2luZGV4LCBwcm9wczogdHJ1ZX0sXG4gICAge25hbWU6ICduZXdfaW52b2ljZScsIHBhdGg6ICcvaW52b2ljZXMvbmV3JywgY29tcG9uZW50OiBuZXdfaW52b2ljZX0sXG4gICAge25hbWU6ICdpbnZvaWNlJywgcGF0aDogJy9pbnZvaWNlcy86aWQnLCBjb21wb25lbnQ6IHNpbmdsZV9pbnZvaWNlfSxcblxuICAgIC8vIEludmVudG9yeVxuICAgIHtwYXRoOiAnL2ludmVudG9yeScsIGNvbXBvbmVudDogaW52ZW50b3J5X2luZGV4LCBwcm9wczogdHJ1ZX0sXG4gICAge25hbWU6ICduZXdfaW52ZW50b3J5X2l0ZW0nLCBwYXRoOiAnL2ludmVudG9yeS9uZXcnLCBjb21wb25lbnQ6IG5ld19pbnZlbnRvcnlfaXRlbX0sXG4gICAge25hbWU6ICdpbnZlbnRvcnlfaXRlbScsIHBhdGg6ICcvaW52ZW50b3J5LzppZCcsIGNvbXBvbmVudDogc2luZ2xlX2ludmVudG9yeV9pdGVtfSxcblxuICAgIC8vIFByb2R1Y3RzIC0gSU5DT01QTEVURVxuICAgIC8vIHtwYXRoOiAnL3Byb2R1Y3RzJywgY29tcG9uZW50OiBwcm9kdWN0c30sXG5cbiAgICAvLyBEYXNoYm9hcmRcbiAgICB7cGF0aDogJy9kYXNoYm9hcmQnLCBjb21wb25lbnQ6IGRlZmF1bHRfaW1hZ2VzX3RvX2NoYW5nZX1cblxuXTtcbmNvbnN0IHJvdXRlciA9IG5ldyBWdWVSb3V0ZXIoe1xuICAgIHJvdXRlcyxcbiAgICBtb2RlOiAnaGlzdG9yeSdcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHJvdXRlcjsiXSwic291cmNlUm9vdCI6IiJ9