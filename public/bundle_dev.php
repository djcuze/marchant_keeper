<link href=" https://fonts.googleapis.com/css?family=Barlow:300,400,600%7cExo+2" rel="stylesheet">
<script src="../app/assets/vendor/vue.js"
        defer></script>
<script src="../app/assets/vendor/vue-router.js" defer></script>
<script src="../app/assets/vendor/jquery-3.3.1.min.js"></script>
<script src="../app/assets/vendor/axios.js" defer></script>
<script src="../app/assets/vendor/jquery-ui.min.js" defer></script>
