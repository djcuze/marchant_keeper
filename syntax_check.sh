#!/bin/bash
# Checks PHP files
for file in `find ./app/*`
do
    EXTENSION="${file##*.}"
    if [ "${EXTENSION}" == "php" ]
    then
        RESULTS=`php -l ${file}`
        if [ "${RESULTS}" != "No syntax errors detected in ${file}" ]
        then
            echo ${RESULTS}
        fi
    fi
done

# Checks Javascripts
find ./app/assets/components -type f -name "*.js" -exec jshint --verbose {} \;
find ./public -type f -name "*.js" -exec jshint --verbose {} \;

