-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: mnc_keeper
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Botanical'),(3,'Chemical'),(4,'Hardware'),(2,'Receptacle');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_items`
--

DROP TABLE IF EXISTS `inventory_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `quantity` int(100) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`),
  KEY `category_idx` (`category_id`),
  CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_items`
--

LOCK TABLES `inventory_items` WRITE;
/*!40000 ALTER TABLE `inventory_items` DISABLE KEYS */;
INSERT INTO `inventory_items` VALUES (2,'Agave',1,'/public/images/uploads/agave.jpg',1),(3,'Bougainvillea',15,'/public/images/uploads/bouganvillea.jpg',1),(4,'French Marigold',1,'/public/images/uploads/marigold.jpg',1),(5,'Fiddle Leaf',13,'/public/images/uploads/fiddle_leaf.jpg',1),(6,'Impatiens',21,'/public/images/uploads/impatiens.jpg',1),(7,'Azaleas',21,'/public/images/uploads/azaleas.jpg',1),(8,'Aloe Vera',14,'/public/images/uploads/aloe_vera.jpg',1),(9,'Dracaena',6,'/public/images/uploads/dracaena.jpg',1),(10,'Plectranthus',24,'/public/images/uploads/plectranthus.jpg',1),(11,'Philodendron',15,'/public/images/uploads/philodendron.jpg',1),(12,'Christmas Cactus',13,'/public/images/uploads/christmascactus.jpg',1),(13,'Ficus',16,'/public/images/uploads/ficus.jpg',1),(14,'Chinese Evergreen',19,'/public/images/uploads/chineseevergreen.jpg',1),(15,'Snake plant',9,'/public/images/uploads/snake_image.jpg',1),(16,'Peperomia',13,'/public/images/uploads/peperomia.jpg',1),(17,'Asparagus Fern',1,'/public/images/uploads/asparagusfern.jpg',1),(18,'Anthurium',13,'/public/images/uploads/anthurium.jpg',1),(19,'Dieffenbachia',12,'/public/images/uploads/dieffenbachia.jpg',1),(20,'Jasmine',3,'/public/images/uploads/jasmine.jpg',1),(22,'Terracotta (small)',24,'http://placehold.it/300/300',2),(23,'Tall square',16,'http://placehold.it/300/300',2),(24,'Short square',21,'http://placehold.it/300/300',2),(25,'Circular white glazed pot',25,'http://placehold.it/300/300',2),(26,'Glass Terrarium',12,'http://placehold.it/300/300',2),(27,'Wooden bowl',16,'http://placehold.it/300/300',2),(28,'Large Concrete Pot',4,'http://placehold.it/300/300',2),(29,'Small wooden square',20,'http://placehold.it/300/300',2),(30,'Yellow cage',8,'http://placehold.it/300/300',2),(32,'Packaging labels',7,'http://placehold.it/300/300',4);
/*!40000 ALTER TABLE `inventory_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_items`
--

DROP TABLE IF EXISTS `invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_items` (
  `quantity` int(100) NOT NULL,
  `unit_price` float NOT NULL,
  `inventory_item_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`invoice_id`,`inventory_item_id`),
  KEY `invoice_items_ibfk_1` (`inventory_item_id`),
  KEY `invoice_items_invoice_idx` (`invoice_id`),
  CONSTRAINT `invoice_items_ibfk_1` FOREIGN KEY (`inventory_item_id`) REFERENCES `inventory_items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `invoice_items_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_items`
--

LOCK TABLES `invoice_items` WRITE;
/*!40000 ALTER TABLE `invoice_items` DISABLE KEYS */;
INSERT INTO `invoice_items` VALUES (21,8.36,2,1),(10,14.29,9,1),(7,10.19,11,1),(15,12.66,19,1),(18,9.41,2,2),(4,9.71,3,2),(13,14.12,4,2),(3,9.1,5,2),(14,14.92,6,2),(10,14.51,7,2),(20,6.22,8,2),(8,1.75,9,2),(9,4.11,10,2),(24,7.43,12,2),(5,3.22,13,2),(9,5.52,14,2),(18,10.12,15,2),(15,1.7,16,2),(2,11.6,17,2),(4,1.33,18,2),(18,0.82,19,2),(18,5.73,20,2),(6,3.46,5,3),(12,9.18,22,3),(24,2.51,23,3),(4,13.46,25,3),(14,6.2,26,3),(7,14.55,27,3),(6,6.13,28,3),(3,8.11,29,3),(16,5.25,9,4),(4,10.22,22,4),(7,4.25,23,4),(24,4.5,24,4),(5,10.38,25,4),(0,3.61,26,4),(5,12.59,27,4),(5,11.4,28,4),(18,5.16,29,4),(13,2.36,30,4),(4,8.61,22,5),(1,2.26,23,5),(23,5.3,24,5),(21,12.85,25,5),(25,12.66,26,5),(22,4.7,27,5),(17,5.89,29,5),(2,7.71,30,5),(14,13.94,2,6),(23,4.83,4,6),(17,12.22,5,6),(15,12.7,6,6),(14,9.82,8,6),(11,7.14,10,6),(10,7.85,11,6),(1,8.46,14,6),(6,13.31,15,6),(21,1.42,16,6),(6,3.15,17,6),(10,1.49,22,6),(18,10.24,23,6),(24,13.84,24,6),(13,2.91,26,6),(9,7.4,27,6),(12,8.76,29,6),(9,13.9,2,7),(4,12.3,4,7),(13,7.13,8,7),(14,6.7,9,7),(7,9.5,10,7),(8,6.56,11,7),(13,5.57,12,7),(10,9.41,13,7),(22,1.43,15,7),(15,1.9,17,7),(1,11.9,19,7),(14,13.24,22,7),(9,8.7,24,7),(20,9.25,25,7),(24,14.93,26,7),(22,11.56,29,7),(14,10.69,2,8),(13,11.68,5,8),(23,13.7,6,8),(22,9.74,14,8),(2,13.7,15,8),(24,9.83,17,8),(14,7.75,24,8),(15,5.62,25,8),(15,11.74,28,8),(7,9.9,29,8),(2,11.98,2,9),(19,10.94,4,9),(18,9.91,5,9),(15,12.83,6,9),(8,0.79,12,9),(4,9.29,13,9),(0,9.34,14,9),(17,8.74,17,9),(10,4.44,19,9),(22,7.38,22,9),(0,8.65,23,9),(10,9.2,26,9),(25,0.3,27,9),(10,1.75,28,9),(14,13.27,29,9),(3,8.33,30,9),(14,11.25,32,9),(6,3.54,3,10),(3,9.88,6,10),(23,11.21,7,10),(2,4.37,10,10),(0,11.91,12,10),(6,14.53,19,10),(9,8.19,20,10),(12,10.24,23,10),(16,12.59,24,10),(19,1.42,26,10),(17,6.7,28,10),(3,11.19,9,11),(2,12.23,19,12),(23,9.65,24,12),(18,0.1,15,13),(15,14.2,18,15),(12,8.96,29,15),(3,13.29,7,17),(4,12.56,26,17),(19,4.89,13,18),(8,2.43,16,19),(23,5.12,16,21),(23,2.13,14,22),(14,0.36,19,22),(6,0.77,25,22),(24,9.32,27,23),(1,12.41,3,25),(3,3.16,25,25),(2,2.84,6,26),(24,14.66,18,28),(11,14.22,20,28),(21,9.76,22,29),(15,2.66,2,30),(19,5.78,9,30),(24,7.22,32,30),(24,0.84,17,31),(8,8.85,19,31),(13,13.8,12,33),(1,13.6,16,33),(20,13.96,20,34),(9,13.7,24,34),(24,6.58,27,34),(22,11.85,16,35),(25,1.91,26,36),(2,1.89,29,36),(2,5.35,30,36),(14,6.76,26,38),(15,10.33,29,38),(9,11.91,30,38),(8,6.1,32,38),(3,11.38,15,39),(6,0.37,17,39),(16,0.34,30,40);
/*!40000 ALTER TABLE `invoice_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `invoice_items_with_description_and_category`
--

DROP TABLE IF EXISTS `invoice_items_with_description_and_category`;
/*!50001 DROP VIEW IF EXISTS `invoice_items_with_description_and_category`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `invoice_items_with_description_and_category` AS SELECT
                                                                        1 AS `description`,
                                                                        1 AS `category`,
                                                                        1 AS `quantity`,
                                                                        1 AS `unit_price`,
                                                                        1 AS `invoice_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`),
  CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (1,'1a41010','2010-06-21',3),(2,'f17253','2011-05-17',1),(3,'fae1010','2010-02-16',1),(4,'e886495','2011-08-24',2),(5,'76b966','2011-06-17',1),(6,'9632051','2011-06-21',1),(7,'a055875','2011-09-19',2),(8,'12c7269','2011-05-12',3),(9,'9402901','2010-04-14',2),(10,'0139484','2010-04-27',3),(11,'d732981','2010-06-28',1),(12,'129302','2011-03-10',2),(13,'aae4172','2010-03-25',3),(14,'3447285','2010-06-21',2),(15,'ba89260','2011-07-12',1),(16,'69d7901','2011-03-28',3),(17,'a343596','2011-06-28',1),(18,'dd02904','2011-05-19',2),(19,'c578408','2011-01-23',2),(20,'0cd4285','2011-02-11',3),(21,'00d383','2011-02-27',1),(22,'8425757','2011-01-15',2),(23,'d775678','2011-06-26',1),(24,'3a43736','2010-06-10',3),(25,'0b08439','2010-05-28',1),(26,'79d7280','2011-02-21',3),(27,'29c6003','2010-07-18',1),(28,'3d27142','2010-01-12',1),(29,'ba09633','2011-08-18',2),(30,'6f54657','2010-09-17',1),(31,'6b89311','2010-01-18',1),(32,'5224721','2011-05-25',2),(33,'9b86923','2011-07-26',1),(34,'d4f32','2010-05-10',1),(35,'9954710','2011-03-14',3),(36,'4225080','2011-06-20',3),(37,'ab56691','2011-05-10',3),(38,'40d1918','2010-02-13',3),(39,'26a2080','2011-06-16',2),(40,'a6f2218','2010-05-11',3),(41,'4b67387','2011-01-23',2),(42,'b601096','2010-01-28',2),(43,'7831923','2011-06-24',3),(44,'5bf6158','2011-08-17',1),(45,'17b9','2010-09-14',1),(46,'6ed5057','2011-05-10',3),(47,'3595230','2010-02-11',3),(48,'6789542','2011-06-17',3),(49,'c2e6429','2010-05-25',2),(50,'b0f982','2011-06-16',2),(72,'sample101','2018-05-01',1),(82,'sample2','2018-05-06',2),(92,'Bree&#039;s Viewing','2018-06-15',2);
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `invoices_full_info`
--

DROP TABLE IF EXISTS `invoices_full_info`;
/*!50001 DROP VIEW IF EXISTS `invoices_full_info`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `invoices_full_info` AS SELECT
                                               1 AS `id`,
                                               1 AS `number`,
                                               1 AS `date`,
                                               1 AS `supplier`,
                                               1 AS `category`,
                                               1 AS `total`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `invoices_with_total_and_category`
--

DROP TABLE IF EXISTS `invoices_with_total_and_category`;
/*!50001 DROP VIEW IF EXISTS `invoices_with_total_and_category`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `invoices_with_total_and_category` AS SELECT
                                                             1 AS `id`,
                                                             1 AS `number`,
                                                             1 AS `date`,
                                                             1 AS `supplier`,
                                                             1 AS `category`,
                                                             1 AS `total`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `plant_taxonomy`
--

DROP TABLE IF EXISTS `plant_taxonomy`;
/*!50001 DROP VIEW IF EXISTS `plant_taxonomy`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `plant_taxonomy` AS SELECT
                                           1 AS `plant_id`,
                                           1 AS `common_name`,
                                           1 AS `water_requirements`,
                                           1 AS `light_requirements`,
                                           1 AS `temperature_requirements`,
                                           1 AS `substrate_requirements`,
                                           1 AS `species`,
                                           1 AS `genus`,
                                           1 AS `family`,
                                           1 AS `order`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `plants`
--

DROP TABLE IF EXISTS `plants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plants` (
  `plant_id` int(11) NOT NULL AUTO_INCREMENT,
  `common_name` varchar(45) NOT NULL,
  `water_requirements` varchar(255) NOT NULL,
  `light_requirements` varchar(255) NOT NULL,
  `temperature_requirements` varchar(255) NOT NULL,
  `substrate_requirements` varchar(255) NOT NULL,
  `taxonomic_species_id` int(11) NOT NULL,
  PRIMARY KEY (`plant_id`),
  UNIQUE KEY `plant_id_UNIQUE` (`plant_id`),
  UNIQUE KEY `taxonomic_specis_id_UNIQUE` (`taxonomic_species_id`),
  CONSTRAINT `taxonomic_species_id` FOREIGN KEY (`taxonomic_species_id`) REFERENCES `taxonomic_species` (`taxonomic_species_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plants`
--

LOCK TABLES `plants` WRITE;
/*!40000 ALTER TABLE `plants` DISABLE KEYS */;
/*!40000 ALTER TABLE `plants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `sessions_id` int(11) NOT NULL AUTO_INCREMENT,
  `sessions_user_id` varchar(45) DEFAULT NULL,
  `token` varchar(32) NOT NULL,
  `serial` varchar(32) NOT NULL,
  `date` varchar(10) DEFAULT 'foo',
  PRIMARY KEY (`sessions_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (29,'1','ncsfj9f2278yk1hhufrsj92s1u19A','u9nrj7298syskuc2hA1fsffh1129j','foo');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (2,'Cares more than a Doctory'),(1,'No Pants, Just Pots'),(3,'Triple Threat Gardening');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomic_family`
--

DROP TABLE IF EXISTS `taxonomic_family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomic_family` (
  `taxonomic_family_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `taxonomic_order_id` int(11) NOT NULL,
  PRIMARY KEY (`taxonomic_family_id`),
  UNIQUE KEY `taxonomic_family_id_UNIQUE` (`taxonomic_family_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `taxonomic_order_id_idx` (`taxonomic_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomic_family`
--

LOCK TABLES `taxonomic_family` WRITE;
/*!40000 ALTER TABLE `taxonomic_family` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxonomic_family` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomic_genus`
--

DROP TABLE IF EXISTS `taxonomic_genus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomic_genus` (
  `taxonomic_genus_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `taxonomic_family_id` int(11) NOT NULL,
  PRIMARY KEY (`taxonomic_genus_id`),
  UNIQUE KEY `taxonomic_genus_id_UNIQUE` (`taxonomic_genus_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `taxonomic_family_id_idx` (`taxonomic_family_id`),
  CONSTRAINT `taxonomic_family_id` FOREIGN KEY (`taxonomic_family_id`) REFERENCES `taxonomic_family` (`taxonomic_family_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomic_genus`
--

LOCK TABLES `taxonomic_genus` WRITE;
/*!40000 ALTER TABLE `taxonomic_genus` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxonomic_genus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomic_order`
--

DROP TABLE IF EXISTS `taxonomic_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomic_order` (
  `taxonomic_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`taxonomic_order_id`),
  UNIQUE KEY `taxonomic_order_id_UNIQUE` (`taxonomic_order_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomic_order`
--

LOCK TABLES `taxonomic_order` WRITE;
/*!40000 ALTER TABLE `taxonomic_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxonomic_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxonomic_species`
--

DROP TABLE IF EXISTS `taxonomic_species`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxonomic_species` (
  `taxonomic_species_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `taxonomic_genus_id` int(11) NOT NULL,
  PRIMARY KEY (`taxonomic_species_id`),
  UNIQUE KEY `taxonomic_species_id_UNIQUE` (`taxonomic_species_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `taxonomic_genus_id_idx` (`taxonomic_genus_id`),
  CONSTRAINT `taxonomic_genus_id` FOREIGN KEY (`taxonomic_genus_id`) REFERENCES `taxonomic_genus` (`taxonomic_genus_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxonomic_species`
--

LOCK TABLES `taxonomic_species` WRITE;
/*!40000 ALTER TABLE `taxonomic_species` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxonomic_species` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `test`
--

DROP TABLE IF EXISTS `test`;
/*!50001 DROP VIEW IF EXISTS `test`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `test` AS SELECT
                                 1 AS `id`,
                                 1 AS `number`,
                                 1 AS `date`,
                                 1 AS `supplier`,
                                 1 AS `category`,
                                 1 AS `total`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `pass` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user','password');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mnc_keeper'
--

--
-- Dumping routines for database 'mnc_keeper'
--
/*!50003 DROP PROCEDURE IF EXISTS `check_invoice_items` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_invoice_items`(
  IN new_quantity          INT(100) UNSIGNED,
  IN new_unit_price        FLOAT,
  IN new_inventory_item_id INT(11) UNSIGNED,
  IN new_invoice_id        INT(11) UNSIGNED
)
  BEGIN
    SET @quantity = new_quantity;
    SET @unit_price = new_unit_price;
    SET @inventory_item_id = new_inventory_item_id;
    SET @invoice_id = new_invoice_id;
    IF (
         SELECT COUNT(*)
         FROM invoice_items
         WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id
       ) != 0
    THEN
      SET @old_quantiy = (SELECT quantity
                          FROM invoice_items
                          WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id);
      SET @old_unit_price = (SELECT unit_price
                             FROM invoice_items
                             WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id);
      IF (SELECT COUNT(*)
          FROM invoice_items
          WHERE invoice_id = @invoice_id
                AND inventory_item_id = @inventory_item_id
                AND unit_price = @old_unit_price
                AND quantity = @old_quantity) = 0
      THEN
        UPDATE invoice_items
        SET unit_price = @unit_price, quantity = @quantity
        WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id;
      END IF;
    ELSE
      INSERT INTO invoice_items
      SET unit_price      = @unit_price,
        quantity          = @quantity,
        inventory_item_id = @inventory_item_id,
        invoice_id        = @invoice_id;
    END IF;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `invoice_items_with_description_and_category`
--

/*!50001 DROP VIEW IF EXISTS `invoice_items_with_description_and_category`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
  /*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
  /*!50001 VIEW `invoice_items_with_description_and_category` AS (select `ve`.`description` AS `description`,`c`.`name` AS `category`,`vo`.`quantity` AS `quantity`,`vo`.`unit_price` AS `unit_price`,`vo`.`invoice_id` AS `invoice_id` from ((`invoice_items` `vo` join `inventory_items` `ve` on((`vo`.`inventory_item_id` = `ve`.`id`))) join `categories` `c` on((`ve`.`category_id` = `c`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `invoices_full_info`
--

/*!50001 DROP VIEW IF EXISTS `invoices_full_info`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
  /*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
  /*!50001 VIEW `invoices_full_info` AS (select `i`.`id` AS `id`,`i`.`number` AS `number`,`i`.`date` AS `date`,`s`.`name` AS `supplier`,(select `c`.`name` from ((`invoice_items` `inv` join `inventory_items` `v` on((`inv`.`inventory_item_id` = `v`.`id`))) join `categories` `c` on((`c`.`id` = `v`.`category_id`))) where (`inv`.`invoice_id` = `i`.`id`) group by `c`.`name` order by count(`c`.`name`) desc limit 1) AS `category`,(select round(sum((`invoice_items`.`quantity` * `invoice_items`.`unit_price`)),2) from `invoice_items` where (`invoice_items`.`invoice_id` = `i`.`id`)) AS `total` from (`invoices` `i` join `suppliers` `s` on((`i`.`supplier_id` = `s`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `invoices_with_total_and_category`
--

/*!50001 DROP VIEW IF EXISTS `invoices_with_total_and_category`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
  /*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
  /*!50001 VIEW `invoices_with_total_and_category` AS (select `invoices`.`id` AS `id`,`invoices`.`number` AS `number`,`invoices`.`date` AS `date`,`suppliers`.`name` AS `supplier`,(select `invoice_items_with_description_and_category`.`category` from `invoice_items_with_description_and_category` where (`invoice_items_with_description_and_category`.`invoice_id` = `invoices`.`id`) group by `invoice_items_with_description_and_category`.`category`) AS `category`,(select round(sum((`invoice_items_with_description_and_category`.`unit_price` * `invoice_items_with_description_and_category`.`quantity`)),2) from `invoice_items_with_description_and_category` where (`invoice_items_with_description_and_category`.`invoice_id` = `invoices`.`id`)) AS `total` from (`invoices` join `suppliers` on((`invoices`.`supplier_id` = `suppliers`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `plant_taxonomy`
--

/*!50001 DROP VIEW IF EXISTS `plant_taxonomy`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
  /*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
  /*!50001 VIEW `plant_taxonomy` AS (select `p`.`plant_id` AS `plant_id`,`p`.`common_name` AS `common_name`,`p`.`water_requirements` AS `water_requirements`,`p`.`light_requirements` AS `light_requirements`,`p`.`temperature_requirements` AS `temperature_requirements`,`p`.`substrate_requirements` AS `substrate_requirements`,`taxonomic_species`.`name` AS `species`,`taxonomic_genus`.`name` AS `genus`,`taxonomic_family`.`name` AS `family`,`taxonomic_order`.`name` AS `order` from ((((`plants` `p` join `taxonomic_species` on((`p`.`taxonomic_species_id` = `taxonomic_species`.`taxonomic_species_id`))) join `taxonomic_genus` on((`taxonomic_species`.`taxonomic_genus_id` = `taxonomic_genus`.`taxonomic_genus_id`))) join `taxonomic_family` on((`taxonomic_genus`.`taxonomic_family_id` = `taxonomic_family`.`taxonomic_family_id`))) join `taxonomic_order` on((`taxonomic_family`.`taxonomic_order_id` = `taxonomic_order`.`taxonomic_order_id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `test`
--

/*!50001 DROP VIEW IF EXISTS `test`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
  /*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
  /*!50001 VIEW `test` AS select `invoices`.`id` AS `id`,`invoices`.`number` AS `number`,`invoices`.`date` AS `date`,`suppliers`.`name` AS `supplier`,(select `invoice_items_with_description_and_category`.`category` from `invoice_items_with_description_and_category` where (`invoice_items_with_description_and_category`.`invoice_id` = `invoices`.`id`) group by `invoice_items_with_description_and_category`.`category`) AS `category`,(select round(sum((`invoice_items_with_description_and_category`.`unit_price` * `invoice_items_with_description_and_category`.`quantity`)),2) from `invoice_items_with_description_and_category` where (`invoice_items_with_description_and_category`.`invoice_id` = `invoices`.`id`)) AS `total` from (`invoices` join `suppliers` on((`invoices`.`supplier_id` = `suppliers`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
