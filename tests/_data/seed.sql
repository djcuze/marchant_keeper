-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: http://marchantandco.herokuapp/com    Database: mnc_keeper
-- ------------------------------------------------------
-- Server version	5.7.21
--
-- Table structure for table `categories`
--
DROP DATABASE IF EXISTS mnc_keeper;
CREATE DATABASE mnc_keeper;

CREATE TABLE categories (
  id     INT(11)      NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `name` (`name`)
);

--
-- Dumping data for table `categories`
--

INSERT INTO categories (id, name) VALUES (1, 'Botanical');
INSERT INTO categories (id, name) VALUES (3, 'Chemical');
INSERT INTO categories (id, name) VALUES (4, 'Hardware');
INSERT INTO categories (id, name) VALUES (2, 'Receptacle');

--
-- Table structure for table `inventory_items`
--

CREATE TABLE inventory_items (
  id          INT(11)      NOT NULL,
  description VARCHAR(255) NOT NULL,
  quantity    INT(100)     NOT NULL,
  image_path  VARCHAR(255) DEFAULT NULL,
  category_id INT(11)      NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY description (description),
  KEY category_idx (category_id),
  CONSTRAINT category_id FOREIGN KEY (category_id) REFERENCES categories (id)
);

--
-- Dumping data for table `inventory_items`
--

INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (2, 'Agave', 1, '/public/images/uploads/agave.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (3, 'Bougainvillea', 15, '/public/images/uploads/bouganvillea.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (4, 'French Marigold', 1, '/public/images/uploads/marigold.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (5, 'Fiddle Leaf', 13, '/public/images/uploads/fiddle_leaf.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (6, 'Impatiens', 21, '/public/images/uploads/impatiens.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (7, 'Azaleas', 21, '/public/images/uploads/azaleas.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (8, 'Aloe Vera', 14, '/public/images/uploads/aloe_vera.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (9, 'Dracaena', 6, '/public/images/uploads/dracaena.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (10, 'Plectranthus', 24, '/public/images/uploads/plectranthus.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (11, 'Philodendron', 15, '/public/images/uploads/philodendron.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (12, 'Christmas Cactus', 13, '/public/images/uploads/christmascactus.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (13, 'Ficus', 16, '/public/images/uploads/ficus.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (14, 'Chinese Evergreen', 19, '/public/images/uploads/chineseevergreen.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (15, 'Snake plant', 9, '/public/images/uploads/snake_image.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (16, 'Peperomia', 13, '/public/images/uploads/peperomia.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (17, 'Asparagus Fern', 1, '/public/images/uploads/asparagusfern.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (18, 'Anthurium', 13, '/public/images/uploads/anthurium.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (19, 'Dieffenbachia', 12, '/public/images/uploads/dieffenbachia.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (20, 'Jasmine', 3, '/public/images/uploads/jasmine.jpg', 1);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (22, 'Terracotta (small)', 24, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (23, 'Tall square', 16, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (24, 'Short square', 21, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (25, 'Circular white glazed pot', 25, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (26, 'Glass Terrarium', 12, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (27, 'Wooden bowl', 16, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (28, 'Large Concrete Pot', 4, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (29, 'Small wooden square', 20, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (30, 'Yellow cage', 8, 'http://placehold.it/300/300', 2);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (32, 'Packaging labels', 7, 'http://placehold.it/300/300', 4);
INSERT INTO inventory_items (id, description, quantity, image_path, category_id)
VALUES (33, 'asdf', 222, 'http://placehold.it/300/300', 2);


--
-- Table structure for table `suppliers`
--

CREATE TABLE suppliers (
  id     INT(11)      NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `name` (`name`)
);

--
-- Dumping data for table `suppliers`
--

INSERT INTO suppliers (id, name) VALUES (2, 'Cares more than a Doctory');
INSERT INTO suppliers (id, name) VALUES (1, 'No Pants, Just Pots');
INSERT INTO suppliers (id, name) VALUES (3, 'Triple Threat Gardening');

--
-- Table structure for table `invoices`
--
CREATE TABLE invoices (
  id          INT(11)      NOT NULL,
  `number`    VARCHAR(255) NOT NULL,
  `date`      DATE DEFAULT NULL,
  supplier_id INT(11)      NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (supplier_id) REFERENCES suppliers (id)
);

--
-- Dumping data for table `invoices`
--

INSERT INTO invoices (id, number, date, supplier_id) VALUES (1, '1a41010', '2010-06-21', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (2, 'f17253', '2011-05-17', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (3, 'fae1010', '2010-02-16', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (4, 'e886495', '2011-08-24', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (5, '76b966', '2011-06-17', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (6, '9632051', '2011-06-21', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (7, 'a055875', '2011-09-19', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (8, '12c7269', '2011-05-12', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (9, '9402901', '2010-04-14', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (10, '0139484', '2010-04-27', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (11, 'd732981', '2010-06-28', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (12, '129302', '2011-03-10', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (13, 'aae4172', '2010-03-25', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (14, '3447285', '2010-06-21', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (15, 'ba89260', '2011-07-12', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (16, '69d7901', '2011-03-28', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (17, 'a343596', '2011-06-28', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (18, 'dd02904', '2011-05-19', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (19, 'c578408', '2011-01-23', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (20, '0cd4285', '2011-02-11', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (21, '00d383', '2011-02-27', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (22, '8425757', '2011-01-15', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (23, 'd775678', '2011-06-26', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (24, '3a43736', '2010-06-10', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (25, '0b08439', '2010-05-28', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (26, '79d7280', '2011-02-21', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (27, '29c6003', '2010-07-18', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (28, '3d27142', '2010-01-12', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (29, 'ba09633', '2011-08-18', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (30, '6f54657', '2010-09-17', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (31, '6b89311', '2010-01-18', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (32, '5224721', '2011-05-25', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (33, '9b86923', '2011-07-26', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (34, 'd4f32', '2010-05-10', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (35, '9954710', '2011-03-14', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (36, '4225080', '2011-06-20', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (37, 'ab56691', '2011-05-10', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (38, '40d1918', '2010-02-13', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (39, '26a2080', '2011-06-16', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (40, 'a6f2218', '2010-05-11', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (41, '4b67387', '2011-01-23', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (42, 'b601096', '2010-01-28', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (43, '7831923', '2011-06-24', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (44, '5bf6158', '2011-08-17', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (45, '17b9', '2010-09-14', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (46, '6ed5057', '2011-05-10', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (47, '3595230', '2010-02-11', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (48, '6789542', '2011-06-17', 3);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (49, 'c2e6429', '2010-05-25', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (50, 'b0f982', '2011-06-16', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (72, 'sample101', '2018-05-01', 1);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (82, 'sample2', '2018-05-06', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (92, 'Bree&#039;s Viewing', '2018-06-15', 2);
INSERT INTO invoices (id, number, date, supplier_id) VALUES (93, 'Wubbalub', '2018-06-21', 1);

--
-- Table structure for table `invoice_items`
--

CREATE TABLE invoice_items (
  quantity          INT(100) NOT NULL,
  unit_price        FLOAT    NOT NULL,
  inventory_item_id INT(11)  NOT NULL,
  invoice_id        INT(11)  NOT NULL,
  PRIMARY KEY (invoice_id, inventory_item_id),
  FOREIGN KEY (inventory_item_id) REFERENCES inventory_items (id)
    ON DELETE CASCADE,
  FOREIGN KEY (invoice_id) REFERENCES invoices (id)
);

--
-- Dumping data for table `invoice_items`
--

INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (21, 8.36, 2, 1);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 14.29, 9, 1);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (7, 10.19, 11, 1);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 12.66, 19, 1);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 9.41, 2, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 9.71, 3, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 14.12, 4, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 9.1, 5, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 14.92, 6, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 14.51, 7, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (20, 6.22, 8, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (8, 1.75, 9, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 4.11, 10, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 7.43, 12, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (5, 3.22, 13, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 5.52, 14, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 10.12, 15, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 1.7, 16, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 11.6, 17, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 1.33, 18, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 0.82, 19, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 5.73, 20, 2);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 3.46, 5, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (12, 9.18, 22, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 2.51, 23, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 13.46, 25, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 6.2, 26, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (7, 14.55, 27, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 6.13, 28, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 8.11, 29, 3);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (16, 5.25, 9, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 10.22, 22, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (7, 4.25, 23, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 4.5, 24, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (5, 10.38, 25, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (0, 3.61, 26, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (5, 12.59, 27, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (5, 11.4, 28, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 5.16, 29, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 2.36, 30, 4);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 8.61, 22, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (1, 2.26, 23, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 5.3, 24, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (21, 12.85, 25, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (25, 12.66, 26, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (22, 4.7, 27, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (17, 5.89, 29, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 7.71, 30, 5);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 13.94, 2, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 4.83, 4, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (17, 12.22, 5, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 12.7, 6, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 9.82, 8, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (11, 7.14, 10, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 7.85, 11, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (1, 8.46, 14, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 13.31, 15, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (21, 1.42, 16, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 3.15, 17, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 1.49, 22, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 10.24, 23, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 13.84, 24, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 2.91, 26, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 7.4, 27, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (12, 8.76, 29, 6);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 13.9, 2, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 12.3, 4, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 7.13, 8, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 6.7, 9, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (7, 9.5, 10, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (8, 6.56, 11, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 5.57, 12, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 9.41, 13, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (22, 1.43, 15, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 1.9, 17, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (1, 11.9, 19, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 13.24, 22, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 8.7, 24, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (20, 9.25, 25, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 14.93, 26, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (22, 11.56, 29, 7);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 10.69, 2, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 11.68, 5, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 13.7, 6, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (22, 9.74, 14, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 13.7, 15, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 9.83, 17, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 7.75, 24, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 5.62, 25, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 11.74, 28, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (7, 9.9, 29, 8);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 11.98, 2, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (19, 10.94, 4, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 9.91, 5, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 12.83, 6, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (8, 0.79, 12, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 9.29, 13, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (0, 9.34, 14, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (17, 8.74, 17, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 4.44, 19, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (22, 7.38, 22, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (0, 8.65, 23, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 9.2, 26, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (25, 0.3, 27, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (10, 1.75, 28, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 13.27, 29, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 8.33, 30, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 11.25, 32, 9);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 3.54, 3, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 9.88, 6, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 11.21, 7, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 4.37, 10, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (0, 11.91, 12, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 14.53, 19, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 8.19, 20, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (12, 10.24, 23, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (16, 12.59, 24, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (19, 1.42, 26, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (17, 6.7, 28, 10);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 11.19, 9, 11);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 12.23, 19, 12);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 9.65, 24, 12);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (18, 0.1, 15, 13);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 14.2, 18, 15);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (12, 8.96, 29, 15);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 13.29, 7, 17);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (4, 12.56, 26, 17);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (19, 4.89, 13, 18);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (8, 2.43, 16, 19);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 5.12, 16, 21);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (23, 2.13, 14, 22);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 0.36, 19, 22);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 0.77, 25, 22);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 9.32, 27, 23);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (1, 12.41, 3, 25);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 3.16, 25, 25);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 2.84, 6, 26);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 14.66, 18, 28);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (11, 14.22, 20, 28);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (21, 9.76, 22, 29);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 2.66, 2, 30);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (19, 5.78, 9, 30);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 7.22, 32, 30);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 0.84, 17, 31);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (8, 8.85, 19, 31);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (13, 13.8, 12, 33);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (1, 13.6, 16, 33);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (20, 13.96, 20, 34);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 13.7, 24, 34);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (24, 6.58, 27, 34);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (22, 11.85, 16, 35);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (25, 1.91, 26, 36);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 1.89, 29, 36);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (2, 5.35, 30, 36);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (14, 6.76, 26, 38);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (15, 10.33, 29, 38);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (9, 11.91, 30, 38);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (8, 6.1, 32, 38);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (3, 11.38, 15, 39);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (6, 0.37, 17, 39);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (16, 0.34, 30, 40);
INSERT INTO invoice_items (quantity, unit_price, inventory_item_id, invoice_id) VALUES (99, 2, 33, 93);

--
-- Temporary view structure for view `invoice_items_with_description_and_category`
--

DROP TABLE IF EXISTS invoice_items_with_description_and_category;
/*!50001 DROP VIEW IF EXISTS invoice_items_with_description_and_category*/;
/*!50001 CREATE VIEW `invoice_items_with_description_and_category` AS
  SELECT
    1 AS description,
    1 AS category,
    1 AS quantity,
    1 AS unit_price,
    1 AS invoice_id */;

--
-- Temporary view structure for view `invoices_with_total_and_category`
--

DROP TABLE IF EXISTS invoices_with_total_and_category;
/*!50001 DROP VIEW IF EXISTS invoices_with_total_and_category*/;
/*!50001 CREATE VIEW `invoices_with_total_and_category` AS
  SELECT
    1 AS id,
    1 AS number,
    1 AS date,
    1 AS supplier,
    1 AS category,
    1 AS total */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
  sessions_id      INT(11)     NOT NULL,
  sessions_user_id VARCHAR(45) DEFAULT NULL,
  token            VARCHAR(32) NOT NULL,
  `serial`         VARCHAR(32) NOT NULL,
  `date`           VARCHAR(10) DEFAULT 'foo',
  PRIMARY KEY (sessions_id)
);

--
-- Dumping data for table `sessions`
--

INSERT INTO sessions (sessions_id, sessions_user_id, token, serial, date)
VALUES (34, '1', 'ukc28j279rnjfA99h1sshsff21uy1', 'f122ryh7huk1jus9sfnAc2s98jf91', 'foo');
--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id INT(11)     NOT NULL,
  `name`  VARCHAR(25) NOT NULL,
  pass    VARCHAR(45) NOT NULL,
  PRIMARY KEY (user_id),
  UNIQUE KEY `name` (`name`)
);

--
-- Dumping data for table `users`
--

INSERT INTO users (user_id, name, pass) VALUES (1, 'user', 'password');

DELIMITER ;
CREATE PROCEDURE check_invoice_items(
  IN new_quantity          INT(100) UNSIGNED,
  IN new_unit_price        FLOAT,
  IN new_inventory_item_id INT(11) UNSIGNED,
  IN new_invoice_id        INT(11) UNSIGNED
)
  BEGIN
    SET @quantity = new_quantity;
    SET @unit_price = new_unit_price;
    SET @inventory_item_id = new_inventory_item_id;
    SET @invoice_id = new_invoice_id;
    IF (
         SELECT COUNT(*)
         FROM invoice_items
         WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id
       ) != 0
    THEN
      SET @old_quantiy = (SELECT quantity
                          FROM invoice_items
                          WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id);
      SET @old_unit_price = (SELECT unit_price
                             FROM invoice_items
                             WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id);
      IF (SELECT COUNT(*)
          FROM invoice_items
          WHERE invoice_id = @invoice_id
                AND inventory_item_id = @inventory_item_id
                AND unit_price = @old_unit_price
                AND quantity = @old_quantity) = 0
      THEN
        UPDATE invoice_items
        SET unit_price = @unit_price, quantity = @quantity
        WHERE invoice_id = @invoice_id AND inventory_item_id = @inventory_item_id;
      END IF;
    ELSE
      INSERT INTO invoice_items
      SET unit_price      = @unit_price,
        quantity          = @quantity,
        inventory_item_id = @inventory_item_id,
        invoice_id        = @invoice_id;
    END IF;
  END;
;
DELIMITER ;
-- Dump completed
