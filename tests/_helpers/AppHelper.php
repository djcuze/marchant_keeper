<?php

class AppHelper extends \Codeception\Module
{
    static function log_user_in(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->see('Login');
        $I->fillField('username', 'user');
        $I->fillField('password', 'password');
        $I->click('submit');
        $I->waitForElementVisible('#notice');
        $I->see('Login Successful');
        $I->seeCurrentUrlEquals('/');
        return $I;
    }
}