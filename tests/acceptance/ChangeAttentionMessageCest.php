<?php

class ChangeAttentionMessageCest
{
    static function it_changes_message_when_no_images_need_uploading(AcceptanceTester $I)
    {
        $I = AppHelper::log_user_in($I);
        $I->seeElement('.navigation');

        // Asserts that feedback is delivered
        $I->see('There are');
        $I->see('items that need attention');
    }
}