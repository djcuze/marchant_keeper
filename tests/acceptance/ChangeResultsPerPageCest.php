<?php require "./tests/_helpers/AppHelper.php";

class ChangesResultsPerPageCest
{
    public function it_changes_results_per_page_on_invoices_index(AcceptanceTester $I)
    {
        $I = AppHelper::log_user_in($I);
        /* After The Redirect */
        $I->click('//a[@href="/invoices"]');
        $I->seeCurrentUrlEquals('/invoices');
        try {
            $I->waitForElementNotVisible('#notice', 4);
        } catch (Exception $e) {
            $I->executeJS("$('#notice').hide()");
        }
        $I->click('//button[@name="results_per_page_30"]');

        $I->seeNumberOfElements('//tr[@class="table__row"]', 30);
    }

    public function it_changes_results_per_page_on_inventory_index(AcceptanceTester $I)
    {
        $I = AppHelper::log_user_in($I);
        /* After The Redirect */
        $I->click('//a[@href="/inventory"]');
        $I->seeCurrentUrlEquals('/inventory');
        try {
            $I->waitForElementNotVisible('#notice', 4);
        } catch (Exception $e) {
            $I->executeJS("$('#notice').hide()");
        };
        $I->click('//button[@name="results_per_page_30"]');

        // Assumes there are only 29 records
        $I->seeNumberOfElements('//tr[@class="table__row"]', 29);
    }
}