<?php require_once "./tests/_helpers/AppHelper.php";

class InventoryIndexCest
{
    public function it_redirects_to_login_when_user_attempts_to_visit_inventory_page_when_not_logged_in(AcceptanceTester $I)
    {
        $I->amOnPage('/inventory');
        // Expects the application to have redirected user to the login page
        $I->seeCurrentUrlEquals('/login');
    }

    public function it_redirects_to_associated_inventory_item_on_click_of_table_row(AcceptanceTester $I)
    {
        $I = AppHelper::log_user_in($I);

        $I->click('//a[@href="/inventory"]');
        $I->seeCurrentUrlEquals('/inventory');

        $first_row_in_table = '//tr[@class="table__row"][position()=1]';
        $second_cell_in_row = '//td[position()=2]';

        $inventory_number = $I->grabAttributeFrom($first_row_in_table . $second_cell_in_row, 'innerHTML');
        $I->click('//tr[@class="table__row"][position()=1]');
        // Assumes the first record has an index of 2
        $I->seeCurrentUrlEquals('/inventory/2');
        $I->see($inventory_number);
    }
}