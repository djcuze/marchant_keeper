<?php require_once "./tests/_helpers/AppHelper.php";

use \Codeception\Util\Locator;

class InvoicesIndexCest
{
    public function it_redirects_to_login_when_user_attempts_to_visit_invoices_page_when_not_logged_in(AcceptanceTester $I)
    {
        $I->amOnPage('/invoices');
        // Expects the application to have redirected user to the login page
        $I->seeCurrentUrlEquals('/login');
    }

    public function it_redirects_to_associated_invoice_on_click_of_table_row(AcceptanceTester $I)
    {
        $I = AppHelper::log_user_in($I);

        $I->click('//a[@href="/invoices"]');
        $I->seeCurrentUrlEquals('/invoices');

        $invoice_number = $I->grabAttributeFrom('//tr[@class="table__row"][position()=1]//td[position()=1]', 'innerHTML');
        $I->click('//tr[@class="table__row"][position()=1]');
        // Assumes the first record has an index of 1
        $I->seeCurrentUrlEquals('/invoices/1');
        $I->see($invoice_number);
    }

}