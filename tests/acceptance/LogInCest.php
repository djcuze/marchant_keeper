<?php require_once "./tests/_helpers/AppHelper.php";

class LogInCest
{
    public function it_redirects_to_homepage_on_successful_login(AcceptanceTester $I)
    {
        $I = AppHelper::log_user_in($I);
        // Expects the application to have redirected user to the homepage
        $I->see('Login Successful');
        $I->seeCurrentUrlEquals('/');
    }

    public function it_provides_feedback_when_user_submits_incorrect_login_details(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField('username', 'wrongusername');
        $I->fillField('password', 'wrongpassword');
        $I->click('submit');
        $I->seeCurrentUrlEquals('/login');
        $I->see('Error: Username and/or password incorrect');
    }

    public function it_provides_feedback_when_user_submits_a_blank_form(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->click('submit');
        $I->executeJS("$('#notice').show()");
        $I->waitForElementVisible('#notice', 30);
        $I->see('Please enter a username and password');
    }
}
