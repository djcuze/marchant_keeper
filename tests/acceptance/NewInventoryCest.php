<?php require_once "./tests/_helpers/AppHelper.php";

class NewInventoryCest
{
    public function it_provides_feedback_when_user_submits_empty_form(AcceptanceTester $I)
    {

        $I = AppHelper::log_user_in($I);
        $I->click('//a[@href="/inventory/new"]');
        $I->see('New Inventory');
        $I->click('//input[@type="submit"]');

        // Expects Feedback alerting User of empty fields
        $I->see('Error: Please fill out all fields');
    }

    public function it_redirects_after_successful_creation_and_adds_a_default_image_to_created_inventory_item_if_one_isnt_provided(AcceptanceTester $I)
    {
        // Parameters to Pass
        $description = 'Foobarz';
        $quantity = 9;

        $I = AppHelper::log_user_in($I);

        $I->amOnPage('/inventory/new');

        $I->fillField('//input[@title="description"]', $description);
        $I->fillField('//input[@title="quantity"]', $quantity);

        // Due to custom select:
        $I->executeJS('$("select#category_id").show()');
        $selection = $I->grabAttributeFrom('//select[@id="category_id"]/option[position()=1]', 'innerHTML');

        $I->selectOption('//select[@id="category_id"]', $selection);
        $I->click('//input[@type="submit"]');
        $I->wait(10);
        $I->seeCurrentUrlEquals('/inventory/33');

        $I->seeInDatabase('inventory_items', [
            'description' => $description,
            'quantity' => $quantity,
            'image_path' => "http://placehold.it/300/300"
        ]);
    }
}
