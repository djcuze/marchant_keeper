<?php require_once "./tests/_helpers/AppHelper.php";

class NewInvoiceCest
{
    public function it_provides_feedback_when_user_submits_empty_form(AcceptanceTester $I)
    {

        $I = AppHelper::log_user_in($I);
        $I->click('//a[@href="/invoices/new"]');
        $I->see('New Invoice');
        $I->click('//input[@type="submit"]');
        $I->waitForElementVisible('#notice', 10);
        // Expects Feedback alerting User of empty fields
        $I->see('Error: Please fill out all fields');
    }

    public function it_provides_feedback_when_user_submits_invoice_without_any_invoice_items(AcceptanceTester $I)
    {
        // Parameters to Pass
        $invoice_number = 'XHA10TEST';
        $invoice_date = '1999-09-09';

        $I = AppHelper::log_user_in($I);

        $I->click('//a[@href="/invoices/new"]');
        $I->see('New Invoice');


        $I->fillField('//input[@title="number"]', $invoice_number);
        $I->fillField('//input[@title="date"]', $invoice_date);

        // Due to custom select:
        $I->executeJS('$("select#supplier_id").show()');
        $selection = $I->grabAttributeFrom('//select[@id="supplier_id"]/option[position()=1]', 'innerHTML');

        $I->selectOption('//select[@id="supplier_id"]', $selection);
        $I->click('//input[@type="submit"]');

        $I->see('Error: Please add at least one item to the invoice');
    }

    public function it_provides_feedback_when_user_submits_valid_form(AcceptanceTester $I)
    {
        // Parameters to Pass
        $invoice_number = 'XHA10TEST';
        $invoice_date = '1999-09-09';

        $I = AppHelper::log_user_in($I);

        $I->click('//a[@href="/invoices/new"]');
        $I->see('New Invoice');

        $I->fillField('//input[@title="number"]', $invoice_number);
        $I->fillField('//input[@title="date"]', $invoice_date);
        // Due to custom select:
        $I->executeJS('$("select#supplier_id").show()');
        $selection = $I->grabAttributeFrom('//select[@id="supplier_id"]/option[position()=1]', 'innerHTML');

        $I->selectOption('//select[@id="supplier_id"]', $selection);
        // Attempt early submit
//        $I->see('Error: Please add at least one item to the invoice');

        // Adds invoice items
        $I->fillField('//input[@id="description_0"]', 'A description');
        $I->fillField('//input[@id="quantity_0"]', 9);
        $I->fillField('//input[@id="unit_price_0"]', 2.77);

        $I->executeJS('$("select#category_select_0").show()');
        $selection = $I->grabAttributeFrom('//select[@id="category_select_0"]/option[position()=1]', 'innerHTML');
        $I->selectOption('//select[@id="category_select_0"]', $selection);

        $I->click('//input[@type="submit"]');
        $I->see('Invoice created successfully');
        $I->see('A description');
        $I->see($invoice_number);
    }
}
