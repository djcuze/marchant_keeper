#!/usr/bin/env bash
# Categories
    # POST
    curl -i -X POST -H "Content-Type:application/json" http://localhost:63342/app/controllers/categories.php -d '{"name":"test3"}';
    # GET ALL
    curl -i -X GET -H "Content-Type:application/json" http://localhost:63342/app/controllers/categories.php;
    # GET ONE
    curl -i -X GET -H "Content-Type:application/json" http://localhost:63342/app/controllers/categories.php?id=1;
    # PATCH
    curl -i -X PATCH -H "Content-Type:application/json" http://localhost:63342/app/controllers/categories.php -d '{"id":1, "name":"foobarz"}';
    # DELETE
    curl -i -X DELETE http://localhost:63342/app/controllers/categories.php -d '{"id": 999 }';

# Invoices
    # POST
    curl -i -X POST -H "Content-Type:application/json" http://localhost:63342/app/controllers/invoices.php -d '{"name":"test3"}';
    # GET ALL
    curl -i -X GET -H "Content-Type:application/json" http://localhost:63342/app/controllers/invoices.php;
    # GET ONE
    curl -i -X GET -H "Content-Type:application/json" http://localhost:63342/app/controllers/invoices.php?id=90999;
    # PATCH
    curl -i -X PATCH -H "Content-Type:application/json" http://localhost:63342/app/controllers/invoices.php -d '{"id":1, "name":"foobarz"}';
    # DELETE
