<?php

use \Codeception\Util\HttpCode;

class invoiceControlCest
{
    /** @var \FunctionalTester */
    public function it_returns_with_valid_json_on_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('/app/controllers/invoices.php');
        $I->seeResponseMatchesJsonType([
            "type" => "string",
            "id" => "integer",
            'attributes' => [
                'number' => 'string',
                'total' => 'string',
                'supplier' => 'string'
            ]]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function it_returns_with_404_on_unsuccessful_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('/app/controllers/invoices.php', [
            'id' => 90999
        ]);
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->dontSeeResponseCodeIs(HttpCode::OK);
    }

    public function it_returns_a_single_json_object_when_id_is_passed_to_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('/app/controllers/invoices.php', [
            // Assumes an invoice with an ID of 2 exists
            'id' => 2
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseMatchesJsonType([
            "type" => "string",
            "id" => "integer",
            'attributes' => [
                'number' => 'string',
                'total' => 'string',
                'supplier' => 'string',
                'category' => 'string'
            ]]);
    }

    public function it_returns_with_valid_json_on_a_successful_POST_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/app/controllers/invoices.php', [
            'id' => 1,
            'number' => 'XHA111',
            'date' => '1999-09-09',
            'supplier_id' => 1
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseMatchesJsonType([
            "type" => "string",
            "id" => "integer",
            'attributes' => [
                'number' => 'string',
                'total' => 'string',
                'supplier' => 'string'
            ]]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function it_returns_with_valid_json_on_a_successful_PATCH_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPATCH('/app/controllers/invoices.php', [
            'id' => 72,
            'number' => 'PPP333',
            'date' => '2099-09-09',
            'supplier_id' => 2
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseMatchesJsonType([
            "type" => "string",
            "id" => "integer",
            'attributes' => [
                'number' => 'string',
                'total' => 'string',
                'supplier' => 'string'
            ]]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }
}