<?php

class InvoiceFunctionalityCest
{
    public function _before(FunctionalTester $I)
    {
        // Invoice Params
        $this->invoice_number = 'TOASTEDSANGAZ';
        $this->invoice_supplier = 1;
        $this->invoice_date = '1999-09-09';

        // Invoice Item 1 Params
        // No matching Inventory Item
        $this->item_1_description = 'Sunflower';
        $this->item_1_category = 1;
        $this->item_1_quantity = 1;
        $this->item_1_unit_price = floatval(5.10);

        // Invoice Item 2 Params
        // No matching Inventory Item
        $this->item_2_description = 'Peace Lily';
        $this->item_2_category = 1;
        $this->item_2_quantity = 4;
        $this->item_2_unit_price = 3.08;

        // Invoice Item 3 Params
        // Matching Inventory Item
        $this->item_3_description = 'Agave';
        $this->item_3_category = 1;
        $this->item_3_quantity = 9;
        $this->item_3_unit_price = (float)2.55;

        $this->params = ['number' => $this->invoice_number,
            'supplier_id' => $this->invoice_supplier,
            'date' => $this->invoice_date];
    }

    public function it_successfully_adds_multiple_entries_to_the_database_in_one_transaction(FunctionalTester $I)
    {
        // Existing Inventory Item current quantity
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('/app/controllers/inventory.php', ['id' => 2]);
        $response = $I->grabResponse();
        $existing_inventory_item = json_decode($response);
        try {
            $existing_inventory_item_quantity = $existing_inventory_item->attributes->quantity;
            $I->haveHttpHeader('Content-Type', 'application/json');

            $I->sendPOST('/app/controllers/invoices.php', $this->params);

            $created_invoice = json_decode($I->grabResponse());

            // Invoice Item 1
            $I->sendPOST('/app/controllers/invoice_items.php', array(
                'category_id' => $this->item_1_category,
                'quantity' => $this->item_1_quantity,
                'unit_price' => $this->item_1_unit_price,
                'description' => $this->item_1_description,
                'invoice_id' => $created_invoice->id
            ));

            // Invoice Item 2
            $I->sendPOST('/app/controllers/invoice_items.php', array(
                'category_id' => $this->item_2_category,
                'quantity' => $this->item_2_quantity,
                'unit_price' => $this->item_2_unit_price,
                'description' => $this->item_2_description,
                'invoice_id' => $created_invoice->id
            ));

            // Invoice Item 3
            $I->sendPOST('/app/controllers/invoice_items.php', array(
                'category_id' => $this->item_3_category,
                'quantity' => $this->item_3_quantity,
                'unit_price' => $this->item_3_unit_price,
                'description' => $this->item_3_description,
                'invoice_id' => $created_invoice->id
            ));

        } catch (PDOException $e) {
            print_r($e->getMessage());
        }

        // Asserts an INSERT has been performed on the inventory_items table
        $I->seeInDatabase('inventory_items', [
            'description' => $this->item_1_description,
            'quantity' => $this->item_1_quantity,
            'category_id' => $this->item_1_category
        ]);
        $I->seeInDatabase('inventory_items', [
            'description' => $this->item_2_description,
            'quantity' => $this->item_2_quantity,
            'category_id' => $this->item_2_category
        ]);

        $I->seeInDatabase('inventory_items', [
            'description' => $this->item_3_description,
            'quantity' => $this->item_3_quantity + $existing_inventory_item_quantity,
            'category_id' => $this->item_3_category
        ]);


        $I->seeInDatabase('invoices', [
            'number' => $this->invoice_number,
            'id' => $created_invoice->id
        ]);
        // Asserts an INSERT has been performed on the invoice_items table
        $I->seeInDatabase('invoice_items', [
            'quantity' => $this->item_1_quantity,
            'invoice_id' => $created_invoice->id
        ]);
        $I->seeInDatabase('invoice_items', [
            'quantity' => $this->item_2_quantity,
            'invoice_id' => $created_invoice->id
        ]);
        $I->seeInDatabase('invoice_items', [
            'quantity' => $this->item_3_quantity,
            'invoice_id' => $created_invoice->id
        ]);
    }
}