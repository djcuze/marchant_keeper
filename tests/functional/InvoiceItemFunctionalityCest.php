<?php

class InvoiceItemFunctionalityCest
{
    public function it_adjusts_the_existing_inventory_item_quantity_when_invoice_item_is_added(FunctionalTester $I)
    {
        $quantity = 9;
        $unit_price = 1.48;
        // Assumes an Invoice with the id of 92 exists
        $invoice_id = 82;
        $inventory_item_id = 2;
        $description = 'Agave';

        // Ensures the existence of the original item
        $I->seeInDatabase('inventory_items', [
            'id' => $inventory_item_id,
            'quantity' => 1
        ]);

        // Sends params
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/app/controllers/invoice_items.php', array(
            'quantity' => $quantity,
            'description' => $description,
            'unit_price' => $unit_price,
            'invoice_id' => $invoice_id,
            'inventory_item_id' => $inventory_item_id
        ));
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'quantity' => 'integer',
            'unit_price' => 'float',
            'invoice_id' => 'integer',
            'inventory_item_id' => 'integer'
        ]);
        // Ensures that the invoice item itself has been added to the database
        $I->seeInDatabase('invoice_items', [
            'inventory_item_id' => $inventory_item_id,
            'invoice_id' => $invoice_id
        ]);

        // And finally, checks the quantity has changed on the inventory item
        $I->seeInDatabase('inventory_items', [
            'id' => $inventory_item_id,
            'quantity' => 10
        ]);
    }

    public function it_creates_an_inventory_item_if_no_existing_record_matches_the_description_parameter_submitted(FunctionalTester $I)
    {
        // Params to send
        $description = 'Peace Lily';
        $quantity = 1;
        $unit_price = 14;
        $invoice_id = 92;
        $category_id = 1;

        // Ensures No item exists with given name
        $I->dontSeeInDatabase('inventory_items', [
            'description' => $description,
        ]);

//        // Sends params
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/app/controllers/invoice_items.php', array(
            'description' => $description,
            'quantity' => $quantity,
            'category' => 1,
            'unit_price' => $unit_price,
            'invoice_id' => $invoice_id,
            'category_id' => $category_id
        ));

        // Checks the quantity has changed and the new item exists
        $I->seeInDatabase('inventory_items', [
            'description' => $description,
            'quantity' => $quantity
        ]);

        // And finally, ensures that the invoice item itself has been added to the database
        $I->seeInDatabase('invoice_items', [
            'quantity' => $quantity,
            'unit_price' => $unit_price,
            'invoice_id' => $invoice_id
        ]);
    }
}