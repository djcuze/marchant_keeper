<?php

use \Codeception\Util\HttpCode;

class categoryControlCest
{
    /** @var \FunctionalTester */
    public function it_returns_with_valid_json_on_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendAjaxRequest('GET', '/app/controllers/categories.php');
        $I->seeResponseMatchesJsonType([
            'attributes' => ['name' => 'string'],
            'id' => 'integer',
            'type' => 'string'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function it_returns_a_single_json_object_when_id_is_passed_to_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendAjaxRequest('GET', '/app/controllers/categories.php', [
            'id' => 1
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseMatchesJsonType([
            'attributes' => ['name' => 'string'],
            'id' => 'integer',
            'type' => 'string'
        ]);
    }

    public function it_returns_with_valid_json_on_a_successful_POST_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/app/controllers/categories.php', [
            'name' => 'A test Name'
        ]);
        $I->seeResponseCodeIs(201);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseMatchesJsonType([
            'attributes' => ['name' => 'string'],
            'id' => 'integer',
            'type' => 'string'
        ]);
    }

    public function it_returns_with_valid_json_on_a_successful_PATCH_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPATCH('/app/controllers/categories.php', [
            'id' => 1,
            'name' => 'A test Name'
        ]);
        $I->seeResponseCodeIs(200);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseMatchesJsonType([
            'attributes' => ['name' => 'string'],
            'id' => 'integer',
            'type' => 'string'
        ]);
    }

    public function it_returns_with_200_on_a_successful_DELETE_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE('/app/controllers/categories.php', [
            'id' => 1
        ]);
        $I->seeResponseCodeIs(200);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }
}