<?php

use \Codeception\Util\HttpCode;

class inventoryControlCest
{

    public function _before(FunctionalTester $I)
    {
        $this->json_expectation = [
            'type' => 'string',
            'id' => 'integer',
            'attributes' => [
                'description' => 'string',
                'category' => 'integer',
                'image_path' => 'string'
            ]
        ];

        $this->patch_params = [
            'id' => 2,
            'description' => 'Test Lilies',
            'quantity' => 8,
            'category_id' => 1,
            'image_path' => 'http://placehold.it/500/500'
        ];

        $this->post_params = [
            'description' => 'Test Lilies',
            'quantity' => 11,
            'category_id' => 2,
            'image_path' => 'http://placehold.it/500/500'
        ];
    }

    /** @var \FunctionalTester */
    public function it_returns_with_valid_json_on_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendAjaxRequest('GET', '/app/controllers/inventory.php');
        $I->seeResponseMatchesJsonType($this->json_expectation);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function it_returns_a_single_json_object_when_id_is_passed_to_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendAjaxRequest('GET', '/app/controllers/inventory.php', [
            'id' => 1
        ]);
        $I->seeResponseMatchesJsonType($this->json_expectation);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function it_returns_with_404_on_unsuccessful_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('/app/controllers/inventory.php', [
            'id' => 90999
        ]);
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->dontSeeResponseCodeIs(HttpCode::OK);
    }

    public function it_returns_with_valid_json_on_a_successful_POST_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/app/controllers/inventory.php', $this->post_params);
        $I->seeResponseMatchesJsonType($this->json_expectation);
        $I->seeResponseCodeIs(201);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function it_returns_with_valid_json_on_a_successful_PATCH_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPATCH('/app/controllers/inventory.php', $this->patch_params);
        $I->seeResponseMatchesJsonType($this->json_expectation);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->dontSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }
}