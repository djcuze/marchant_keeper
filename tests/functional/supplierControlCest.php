<?php

class supplierControlCest
{
    /** @var \FunctionalTester */
    public function it_returns_with_valid_json_on_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendAjaxRequest('GET', '/app/controllers/suppliers.php');
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string'
        ]);
    }

    public function it_returns_a_single_json_object_when_id_is_passed_to_GET(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendAjaxRequest('GET', '/app/controllers/suppliers.php', [
            'id' => 1
        ]);
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string'
        ]);
    }

    public function it_returns_with_valid_json_on_a_successful_POST_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/app/controllers/suppliers.php', [
            'name' => 'A test Name'
        ]);
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string'
        ]);
    }

    public function it_returns_with_valid_json_on_a_successful_PATCH_request(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPATCH('/app/controllers/suppliers.php', [
            'id' => 1,
            'name' => 'A test Name'
        ]);
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string'
        ]);
    }
}