<?php

require __DIR__ . '/../../app/models/category.php';

class CategoryTest extends \PHPUnit\Framework\TestCase
{
    protected $category;
    private $conn;
    private $params;

    public function setUp()
    {
        $dbClass = new Database();
        $this->conn = $dbClass->getConnection();

        $obj = new stdClass();
        $obj->name = 'A test category';
        $this->params = json_encode($obj, JSON_NUMERIC_CHECK);
        $this->category = new \App\Models\Category($this->conn, $this->params);
    }

    /** @test */
    public function it_takes_params_when_constructed()
    {
        $this->assertEquals('A test category', $this->category->name);
    }

    /** @test */
    public function it_updates_a_record_given_correct_params()
    {
        // Assumes an invoice with the id of 1 exists
        $stmt = $this->category->update(1);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_creates_a_record_given_correct_params()
    {
        $this->category->name = 'Foodbar';
        $stmt = $this->category->create();
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    // Assumes an invoice with the id of 1 exists
    public function it_reads_a_single_record() {
        $stmt = $this->category->read(1);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }
}
