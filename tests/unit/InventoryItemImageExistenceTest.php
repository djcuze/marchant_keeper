<?php

class InventoryItemImageExistenceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $image_check;
    protected $conn;

    protected function setUp()
    {
        require_once './app/lib/fetch_imageless_inventory_items.php';
        $db = new Database();
        $db->getConnection();
        $this->image_check = new ImageCheck($db);
    }

    /** @test */
    public function it_returns_false_if_no_images_need_uploading()
    {
        $this->assertFalse($this->image_check->requiresUpload(0));
    }

    public function it_returns_true_if_image_needs_uploading()
    {
        $db = new Database();
        $conn = $db->getConnection();
        $no_of_results = $this->image_check->fetchData($conn);
        $this->assertTrue($this->image_check->requiresUpload($no_of_results));
        $this->image_check->requiresUpload(sizeof($no_of_results));
        $this->assertEquals(sizeof($no_of_results), sizeof($no_of_results));
    }

}