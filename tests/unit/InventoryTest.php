<?php

require __DIR__ . '/../../app/models/inventory.php';

class InventoryTest extends \PHPUnit\Framework\TestCase
{
    protected $inventory_item;
    private $conn;
    private $params;

    public function setUp()
    {
        $dbClass = new Database();
        $this->conn = $dbClass->getConnection();

        $obj = new stdClass();
        $obj->description = "Peace Lily";
        $obj->quantity = 10;
        $obj->category_id = 1;

        $this->params = json_encode($obj, JSON_NUMERIC_CHECK);

        $this->inventory_item = new \App\Models\Inventory($this->conn, $this->params);
    }

    /** @test */
    public function it_takes_params_when_constructed()
    {
        $this->assertEquals('Peace Lily', $this->inventory_item->description);
        $this->assertEquals(10, $this->inventory_item->quantity);
        $this->assertEquals(1, $this->inventory_item->category_id);
        $this->assertEquals('http://placehold.it/300/300', $this->inventory_item->image_path);
    }

    /** @test */
    public function it_assigns_a_default_image_if_one_not_supplied()
    {
        $this->inventory_item->create();
        $this->assertEquals('http://placehold.it/300/300', $this->inventory_item->image_path);
    }

    /** @test */
    public function it_creates_a_record_given_correct_params()
    {
        // Works around description unique integrity violation
        $this->inventory_item->description = 'Something Else';
        $stmt = $this->inventory_item->create();
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    // Assumes an invoice with the id of 1 exists
    public function it_reads_a_single_record()
    {
        $stmt = $this->inventory_item->read(2);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_updates_a_record_given_correct_params()
    {
        // Works around description unique integrity violation
        $this->inventory_item->description = 'Banana Lily';
        $this->inventory_item->image_path = './public/images/uploads/1.jpg';
        $stmt = $this->inventory_item->update(2);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_updates_its_quantity_when_given_an_id_and_a_new_quantity()
    {
        // Works around integrity violation
        $this->inventory_item->description = 'A different description';
        $this->inventory_item->quantity = 10;
        $this->inventory_item->create();

        $id = $this->conn->lastInsertId();
        $adjustment = 7;

        $this->inventory_item->updateQuantity($id, $adjustment);
        $this->assertEquals(17, $this->inventory_item->quantity);
    }

    /** @test */
    public function it_deletes_a_record()
    {
        $stmt = $this->inventory_item->delete(2);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }
}
