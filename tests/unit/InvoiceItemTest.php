<?php

require_once __DIR__ . '/../../app/models/invoice_item.php';

class InvoiceItemTest extends \PHPUnit\Framework\TestCase
{
    protected $invoice_item;
    private $conn;
    private $params;
    private $invoice_params;
    private $invoice_id;

    public function setUp()
    {
        $dbClass = new Database();
        $this->conn = $dbClass->getConnection();

        // Creates an Invoice For Integrity
        $obj = new stdClass();
        $obj->number = "XHA1111";
        $obj->date = '1999-09-09';
        $obj->supplier_id = 1;
        $this->invoice_params = json_encode($obj, JSON_NUMERIC_CHECK);
        $invoice = new \App\Models\Invoice($this->conn, $this->invoice_params);
        $stmt = $invoice->create();

        $this->invoice_id = $this->conn->lastInsertId();

        $obj = new stdClass();
        $obj->quantity = 25;
        $obj->unit_price = 10.99;
        $obj->description = 'Antelopes';

        $obj->invoice_id = $this->conn->lastInsertId();
        $obj->inventory_item_id = 3;

        $this->params = json_encode($obj, JSON_NUMERIC_CHECK);
        $this->invoice_item = new \App\Models\InvoiceItem($this->conn, $this->params);
    }

    /** @test */
    public function it_takes_params_when_constructed()
    {
        $this->assertEquals(25, $this->invoice_item->quantity);
        $this->assertEquals(10.99, $this->invoice_item->unit_price);
        $this->assertEquals($this->invoice_id, $this->invoice_item->invoice_id);
        $this->assertEquals(3, $this->invoice_item->inventory_item_id);
    }

    /** @test */
    public function it_creates_a_record_given_correct_params()
    {
        codecept_debug($this->invoice_item);
        $stmt = $this->invoice_item->create();
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_reads_all_records_for_an_invoice()
    {
        // Assumes an invoice with the id of 94 exists
        $stmt = $this->invoice_item->readAllForInvoice(94);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_updates_a_record_given_correct_params()
    {
        $this->invoice_item->quantity = 10;
        $this->invoice_item->unit_price = 1.66;

        // Assumes an invoice with the id of 94 exists
        $this->invoice_item->invoice_id = 94;

        $stmt = $this->invoice_item->update();
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_deletes_a_record()
    {
        $inventory_item_id = $this->invoice_item->inventory_item_id;
        $invoice_id = $this->invoice_item->invoice_id;
        $this->invoice_item->delete($invoice_id, $inventory_item_id);
    }
}
