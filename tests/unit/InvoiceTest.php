<?php

require_once __DIR__ . '/../../app/models/invoice.php';

class InvoiceTest extends \PHPUnit\Framework\TestCase
{
    protected $invoice;
    private $conn;
    private $params;

    public function setUp()
    {
        $dbClass = new Database();
        $this->conn = $dbClass->getConnection();

        $obj = new stdClass();
        $obj->number = "XHA1111";
        $obj->date = '1999-09-09';
        $obj->supplier_id = 1;

        $this->params = json_encode($obj, JSON_NUMERIC_CHECK);

        $this->invoice = new \App\Models\Invoice($this->conn, $this->params);
    }

    /** @test */
    public function it_takes_params_when_constructed()
    {
        $this->assertEquals('XHA1111', $this->invoice->number);
        $this->assertEquals('1999-09-09', $this->invoice->date);
        $this->assertEquals(1, $this->invoice->supplier_id);
    }

    /** @test */
    public function it_updates_a_record_given_correct_params()
    {
        // Assumes an invoice with the id of 72 exists
        $stmt = $this->invoice->update(72);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_creates_a_record_given_correct_params()
    {
        // Works around number unique integrity violation
        $this->invoice->number = 'Something Else';
        $stmt = $this->invoice->create();
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    // Assumes an invoice with the id of 72 exists
    public function it_reads_a_single_record() {
        $stmt = $this->invoice->read(72);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }
}
