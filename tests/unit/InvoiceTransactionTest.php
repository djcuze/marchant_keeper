<?php
//
//class InvoiceTransactionTest extends \Codeception\Test\Unit
//{
//    /** @test */
//    public function it_gets_and_sets_an_invoice_object()
//    {
//        $dbClass = new Database();
//        $db = $dbClass->getConnection();
//
//        $transaction = new InvoiceTransaction($db);
//        $invoice = new Invoice($db);
//
//        // Binds Invoice params
//        $invoice->number = 'XHA100TEST';
//        $invoice->supplier = 1;
//        $invoice->date = '1999-09-09';
//
//        $transaction->setInvoice($invoice);
//
//        $this->assertTrue($transaction->invoice === $invoice);
//    }
//
//    /** @test */
//    public function it_gets_and_sets_multiple_invoice_item_objects()
//    {
//        $dbClass = new Database();
//        $db = $dbClass->getConnection();
//
//        $transaction = new InvoiceTransaction($db);
//        $invoice_item_1 = new InvoiceItem($db);
//        $invoice_item_2 = new InvoiceItem($db);
//
//        // Binds Invoice params
//        $invoice_item_1->quantity = 11;
//        $invoice_item_1->unit_price = 1;
//        $invoice_item_1->description = 'Example Plant';
//        $invoice_item_2->quantity = 28;
//        $invoice_item_2->unit_price = 7;
//        $invoice_item_2->description = 'Monstera';
//
//        $transaction->addInvoiceItem($invoice_item_1);
//        $transaction->addInvoiceItem($invoice_item_2);
//
//        $this->assertContains($invoice_item_1, $transaction->invoice_items);
//        $this->assertContains($invoice_item_2, $transaction->invoice_items);
//    }
//
//    /** @test */
//    public function it_returns_true_when_a_match_in_the_inventory_table_is_found()
//    {
//        $dbClass = new Database();
//        $db = $dbClass->getConnection();
//
//        // Assumes an entry exists for 'Monstera'
//        $description = 'Monstera';
//
//        // Could be empty. Observe 'potential'
//        $potential_match = CheckInvoiceItemsAgainstExistingInventoryItems::queryDatabase($description, $db);
//        $this->assertNotEmpty($potential_match);
//    }
//
//    /** @test */
//    public function it_returns_true_if_invoice_is_inserted()
//    {
//        $dbClass = new Database();
//        $db = $dbClass->getConnection();
//        $invoice = new Invoice($db);
//        $subject = new InvoiceTransaction($db);
//        $invoice_item_1 = new InvoiceItem($db);
//        $invoice_item_2 = new InvoiceItem($db);
//
//        // Binds Invoice params
//        $invoice_item_1->quantity = 11;
//        $invoice_item_1->unit_price = 1;
//        $invoice_item_1->description = 'Example Plant';
//        $invoice_item_1->category = 1;
//        $invoice_item_2->quantity = 28;
//        $invoice_item_2->unit_price = 7;
//        $invoice_item_2->description = 'Monstera';
//        $invoice_item_2->category = 1;
//
//        // Binds Invoice params
//        $invoice->number = 'XHA100TEST';
//        $invoice->supplier = 1;
//        $invoice->date = '1999-09-09';
//
//        $subject->setInvoice($invoice);
//        $subject->addInvoiceItem($invoice_item_1);
//        $subject->addInvoiceItem($invoice_item_2);
//        $this->assertTrue($subject->begin());
//    }
//
////    Private Function:
////
//    //    /** @test */
//    //    public function it_adds_to_the_inventory_item_quantity()
//    //    {
//    //        $dbClass = new Database();
//    //        $db = $dbClass->getConnection();
//    //
//    //        $subject = new InvoiceTransaction($db);
//    //        $result = $subject->updateInventoryItemQuantity(1, 5);
//    //        $this->assertTrue($result);
//    //    }
//    //
//    //    /** @test */
//    //    public function it_returns_false_on_unsuccessful_quantity_update() {
//    //        $dbClass = new Database();
//    //        $db = $dbClass->getConnection();
//    //
//    //        $subject = new InvoiceTransaction($db);
//    //        // Assumes there is no Inventory Item with an id of 6
//    //        $result = $subject->updateInventoryItemQuantity(999999, 1);
//    //        $this->assertFalse($result);
//    //    }
//}