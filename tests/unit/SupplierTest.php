<?php

require __DIR__ . '/../../app/models/supplier.php';

class SupplierTest extends \PHPUnit\Framework\TestCase
{
    protected $supplier;
    private $conn;
    private $params;

    public function setUp()
    {
        $dbClass = new Database();
        $this->conn = $dbClass->getConnection();

        $obj = new stdClass();
        $obj->name = 'A test supplier';
        $this->params = json_encode($obj, JSON_NUMERIC_CHECK);
        $this->supplier = new \App\Models\Supplier($this->conn, $this->params);
    }

    /** @test */
    public function it_takes_params_when_constructed()
    {
        $this->assertEquals('A test supplier', $this->supplier->name);
    }

    /** @test */
    public function it_updates_a_record_given_correct_params()
    {
        // Assumes an invoice with the id of 1 exists
        $stmt = $this->supplier->update(1);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    public function it_creates_a_record_given_correct_params()
    {
        $this->supplier->name = 'Foodbar';
        $stmt = $this->supplier->create();
        $this->assertGreaterThan(0, $stmt->rowCount());
    }

    /** @test */
    // Assumes an invoice with the id of 1 exists
    public function it_reads_a_single_record() {
        $stmt = $this->supplier->read(1);
        $this->assertGreaterThan(0, $stmt->rowCount());
    }
}
