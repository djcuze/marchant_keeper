<?php
require_once __DIR__ . '/../../app/lib/sanitize.php';

class sanitizeTest extends \Codeception\Test\Unit
{
    /** @test */
    public function it_sanitizes_special_characters()
    {
        $value = sanitize("&");
        $this->assertEquals('&amp;', $value);

        $value = sanitize('"');
        $this->assertEquals('&quot;', $value);

        $value = sanitize("'");
        $this->assertEquals('&#039;', $value);
    }

}