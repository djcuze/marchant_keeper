var debug = process.env.NODE_ENV !== 'production';
var webpack = require('webpack');

module.exports = {
    mode: 'development',
    context: __dirname,
    devtool: debug ? 'inline-sourcemap' : null,
    entry: './app/assets/components/app.js',
    output: {
        path: __dirname + '/public',
        filename: 'bundle.js',
        publicPath: '/', // if you don't put the "/" here, you get this error:
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimizeUglifyJsPlugin({mangle: false, sourcemap: false})
    ],
};
